/*
 Mikrokontroler: Atmega16
 Autor: ja
 */
#define BAUD 4800        //��dana pr�dko�� transmisji
#define FOSC 1000000     // Clock Speed
#define MYUBRR FOSC/16/BAUD-1 //12
//#include <stddef.h>
#include <avr\io.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/setbaud.h>

void USART_Init(unsigned int ubrr) {
	/* Set baud rate */
	UBRRH = (unsigned char) (ubrr >> 8);
	UBRRL = (unsigned char) ubrr;
	/* Enable receiver and transmitter */
	UCSRB = (1 << RXEN) | (1 << TXEN);
	/* Set frame format: 8data, 2stop bit */
	UCSRC = (1 << URSEL) | (1 << USBS) | (3 << UCSZ0);
}

void USART_Transmit(unsigned char data) {
	/* Wait for empty transmit buffer */
	while (!(UCSRA & (1 << UDRE)))
		;
	/* Put data into buffer, sends the data */
	UDR = data;
}

unsigned char USART_Receive(void) {
	/* Wait for data to be received */
	while (!(UCSRA & (1 << RXC)))
		;
	/* Get and return received data from buffer */
	return UDR ;
}

int main(void) {
	USART_Init( MYUBRR);
	DDRB |= _BV(PB1);
	unsigned char a = 0;
	while (1) {
		a = USART_Receive();
		USART_Transmit(++a);
		PORTB ^= _BV(PB1);
		_delay_ms(500);
		PORTB ^= _BV(PB1);
	}
}
