///*
///*
//
//Program realizuj�cy obliczanie i wys�anie przez RS-232 wyniku funkcji
//kwadratowej y = 0.3187x^2 + 2x - 7 na podstawie x odebranego
//wcze�niej tak�e za pomoc� RS-232.
//
//Szczeg�y: http://mikrokontrolery.blogspot.com/2011/03/rs-232-atmega8-komputer-terminal.html
//
//Mikrokontroler: Atmega16
//Autor: Dondu
//Data: 2012.11.30
//
//*/
//
////#include <stddef.h>
//#include <avr\io.h>
//#include <stdio.h>
//#include <avr/interrupt.h>
//#include <util/delay.h>
//
////zmienne dot. odbioru danych
//volatile unsigned char odb_x;       //odebrana liczba X
//volatile unsigned char odb_flaga =0;//flaga informuj�ca main o odebraniu liczby
//
////bufor znak�w ze wzorem funkcji, kt�ry wysy�amy po starcie programu
//volatile unsigned int usart_bufor_ind;           //indeks bufora nadawania
//char usart_bufor[30] = "y = 0.3187x^2 + 2x - 7"; //bufor nadawania
//
//void usart_inicjuj(void)
//{
// //definiowanie parametr�w transmisji za pomoc� makr zawartych w pliku
// //nag��wkowym setbaud.h. Je�eli wybierzesz pr�dko��, kt�ra nie b�dzie
// //mo�liwa do realizacji otrzymasz ostrze�enie:
// //#warning "Baud rate achieved is higher than allowed"
//
// #define BAUD 4800        //tutaj podaj ��dan� pr�dko�� transmisji
// #include <util/setbaud.h> //linkowanie tego pliku musi by�
//                           //po zdefiniowaniu BAUD
//
// //ustaw obliczone przez makro warto�ci
// UBRRH = UBRRH_VALUE;
// UBRRL = UBRRL_VALUE;
// #if USE_2X
//   UCSRA |=  (1<<U2X);
// #else
//   UCSRA &= ~(1<<U2X);
// #endif
//
//
// //Ustawiamy pozosta�e parametry modu� USART
// //U W A G A !!!
// //W ATmega8, aby zapisa� do rejestru UCSRC nale�y ustawia� bit URSEL
// //zobacz tak�e: http://mikrokontrolery.blogspot.com/2011/04/avr-czyhajace-pulapki.html#avr_pulapka_2
// UCSRC = (1<<URSEL) | (1<<UCSZ1) | (1<<UCSZ0);  //bit�w danych: 8
//                                                //bity stopu:  1
//                                                //parzysto��:  brak
// //w��cz nadajnik i odbiornik oraz ich przerwania odbiornika
// //przerwania nadajnika w��czamy w funkcji wyslij_wynik()
// UCSRB = (1<<TXEN) | (1<<RXEN) | (1<<RXCIE);
//}
//
//
////--------------------------------------------------------------
//
//
//ISR(USART_RXC_vect)
//{
// //przerwanie generowane po odebraniu bajtu
// odb_x = UDR;   //zapami�taj odebran� liczb�
// odb_flaga = 1; //ustaw flag� odbioru liczby dla main()
//}
//
//
////--------------------------------------------------------------
//
//ISR(USART_UDRE_vect){
//
// //przerwanie generowane, gdy bufor nadawania jest ju� pusty,
// //odpowiedzialne za wys�anie wszystkich znak�w z tablicy usart_bufor[]
//
// //sprawdzamy, czy bajt do wys�ania jest znakiem ko�ca tekstu, czyli zerem
// if(usart_bufor[usart_bufor_ind]!= 0){
//
//  //za�aduj znak do rejestru wysy�ki i ustaw indeks na nast�pny znak
//  UDR = usart_bufor[usart_bufor_ind++];
//
// }else{
//
//  //osi�gni�to koniec napisu w tablicy usart_bufor[]
//  UCSRB &= ~(1<<UDRIE); //wy��cz przerwania pustego bufora nadawania
// }
//}
//
//
////--------------------------------------------------------------
//
//
//void wyslij_wynik(void){
//
// //funkcja rozpoczyna wysy�anie, wysy�aj�c pierwszy znak znajduj�cy si�
// //w tablicy usart_bufor[]. Pozosta�e wy�le funkcja przerwania,
// //kt�ra zostanie wywo�ana automatycznie po wys�aniu ka�dego znaku.
//
// //dodaj do tekstu wyniku znaki ko�ca linii (CR+LF), by na
// //ekranie terminala wyniki pojawia�y si� w nowych liniach
// unsigned char z;
// for(z=0; z<30; z++){
//  if(usart_bufor[z]==0){   //czy to koniec takstu w tablicy
//   //tak znaleziono koniec tekstu, dodajemy znak CR
//   usart_bufor[z]   = 13;  //znak powrotu karetki CR (Carrige Return)
//   usart_bufor[z+1]  = 10; //znak nowej linii LF (Line Feed)
//   usart_bufor[z+2]  = 0;  //znak ko�ca ci�gu tekstu w tablicy
//   break;
//  }
// }
//
//
// //Zaczekaj, a� bufor nadawania b�dzie pusty
// while (!(UCSRA & (1<<UDRE)));
//
// //bufor jest pusty mo�na wys�a�
//
// //nast�pny znak do wysy�ki to znak nr 1
// usart_bufor_ind = 0;
//
// //w��cz przerwania pustego bufora UDR, co rozpocznie transmisj�
// //aktualnej zawarto�ci bufora
// UCSRB |= (1<<UDRIE);
//
//}
//
////--------------------------------------------------------------
//
//
//int main(void)
//{
// usart_inicjuj(); //inicjuj modu� USART (RS-232)
// sei();           //w��cz przerwania globalne
//
// wyslij_wynik();  //na pocz�tku wysy�amy text wzoru, kt�ry po
//                  //resecie jest w tablicy usart_bufor[]
//
// while(1){
//
//  //tutaj Tw�j program ....
//
//  //czekamy na informacj� o odebraniu danej nie blokuj�c mikrokontrolera
//  if(odb_flaga){
//   odb_flaga = 0; //zga� flag�
//   //wykonaj obliczenia i wynik przekszta�� na znaki �aduj�c do
//   //bufora usart_bufor[]
//   sprintf(usart_bufor, "%f", 0.3187 * odb_x * odb_x + 2 * odb_x - 7);
//   wyslij_wynik();  //rozpocznij wysy�anie wyniku przez RS-232
//  }
//  //w tym czasie mo�na wykonywa� dowolny program
//  //umie�� go tutaj lub przed if() powy�ej
// }
//}
