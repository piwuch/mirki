/*
 * twi.h
 *
 *  Created on: 21-07-2014
 *      Author: Ja
 */

#include <avr/io.h>

#define ACK 1
#define NOACK 0

typedef unsigned char uc;

// procedura transmisji sygna�u START
void twistart(void);
// procedura transmisji sygna�u STOP
void twistop(void);
// procedura transmisji bajtu danych
void twiwrite(uc data);
//procedura odczytu bajtu danych
uc twiread(uc ack);
//procedura transmisji ci�gu danych
void twiwrite_buf(uc SLA, uc adr, uc len, uc *buf);
//procedura odczytu ci�gu danych
void twiread_buf(uc SLA, uc adr, uc len, uc *buf);
