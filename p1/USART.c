/*
 * USART.c
 *
 *  Created on: 21-07-2014
 *      Author: Ja
 */

#include "USART.h"

void USART_Init(unsigned int ubrr) {
	UBRR0H = (unsigned char) (ubrr >> 8);
	UBRR0L = (unsigned char) ubrr;
	UCSR0B = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0);
	UCSR0C = (3 << UCSZ00);//(1 << URSEL) |
}

void USART_Transmit(char data) {
	while (!(UCSR0A & (1 << UDRE0)))
		;
	UDR0 = data;
}

void USART_Transmit_Text(char * text) {
	while (*text) {
		USART_Transmit(*text++);
	}
}

unsigned char USART_Receive(void) {
	/* Wait for data to be received */
	while (!(UCSR0A & (1 << RXC0)))
		;
	/* Get and return received data from buffer */
	return UDR0 ;
}

