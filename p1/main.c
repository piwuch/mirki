/*
 * main.c
 *	projekt, praktyki 2014,
 *	dac+lcd+usart
 *  Created on: 31-07-2014
 *      Author: Ja
 */
#define FOSC 16000000
#define BAUD 9600
#define MYUBRR FOSC/16/BAUD-1 //103
#define ADR 0b10010000
//keys
#define KEY1 _BV(PC3)
#define KEY2 _BV(PC2)
#define KEY3 _BV(PC1)
#define KEY4 _BV(PC0)
//out pins
#define ENA_VDD_PLL_DLL _BV(PD2)
#define ENA_VDD_DIG _BV(PE2)
//DAC declarations
#define DAC_ADR 0b10101000
#define VOUT_A 0b00000000
#define VOUT_C 0b00000010
#define VOUT_D 0b00000011
#define VOUT_E 0b00000100
#define VOUT_G 0b00000110
#define VOUT_ALL 0b00001111
#define SETUP_INTER_REF 0b10000000
#define WRITE_DAC_CHANNEL 0b00110000
//current monitors
#define CURRENT_MONITOR_0 0x80
#define CURRENT_MONITOR_1 0x82
#define VBUS_REGISTER 0x02
#define CURRENT_REGISTER 0x04
#define CALLIBRATION_REGISTER 0x05
//text statements
#define MENU_LVL_0 0
#define WRONG_CMD 1
#define NEW_LINE 2
#define HIGH_OR_LOW 3
//menu indicators
#define MENU_MAIN 0
#define MENU_ENA_VDD_PLL_DLL 1
#define MENU_ENA_VDD_DIG 2
#define MENU_WRITE_TO_DAC 3
#define MENU_NUMBER_TO_DAC 6 // w razie dodawania mozna przesunac tutaj
#define MENU_READ_CURRENT 4
#define MENU_READ_VBUS 5

//#include <stddef.h>
#include <avr\io.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <util/delay.h>
//#include <util/twi.h>
#include <util/setbaud.h>
#include <avr/wdt.h>

#include "HD44780.h"
#include "twi.h"
#include "USART.h"

// function declarations, descriptions on bottom
void I2C_SetBusSpeed(uint16_t speed);
void usart_write_number(int number);
void statement(unsigned char command);

volatile unsigned char usart_flag = 0;
volatile unsigned char timer_flag = 0;
volatile char UWbuf[10];
volatile char URbuf[10];
volatile unsigned char Rbuf_ind = 0;
unsigned char LCD_color = 0;

ISR(USART0_RX_vect) {
	URbuf[Rbuf_ind] = UDR0;
	UDR0 = URbuf[Rbuf_ind++];
	usart_flag = 1; //ustaw flag� odbioru liczby dla main()
	PORTC &= ~(7 << PC4);
	PORTC ^= (LCD_color++ << PC4);
}

ISR(TIMER1_COMPA_vect) {
	timer_flag = 1;
}

int main(void) {
	//zmienne
	unsigned char key_lock = 0, key_lock2 = 0;
	unsigned char x = 0, menu_ind = 0, i = 0, number_from_usart = 0;
	unsigned char twobyte_table[] = { 0, 1 };
	unsigned char write_adr = WRITE_DAC_CHANNEL;
	long liczba = 0, multiplier = 1000;
//	porty i piny
	DDRC |= (7 << PC4);
	DDRD |= ENA_VDD_PLL_DLL;
	DDRE |= ENA_VDD_DIG;
//	DDRF |= 0xFF; // to bedzie ale nie dla mnie
	PORTC |= KEY1 | KEY2 | KEY3 | KEY4; //dwa keye nie istnieja na plytce
//	wyswietlacz LCD
	LCD_Initalize();
	LCD_Clear();
	LCD_WriteText("lubie placki");
//	USART
	USART_Init(MYUBRR);
	statement(MENU_LVL_0);
//	I2C lub TWI
	I2C_SetBusSpeed(100);
//	watchdog
	wdt_reset();
	wdt_enable(WDTO_2S);
//	przygotowanie do pracy
	twiwrite_buf(DAC_ADR, SETUP_INTER_REF, 2, twobyte_table);
	twiwrite_buf(DAC_ADR, WRITE_DAC_CHANNEL + VOUT_D, 2, twobyte_table);
	twobyte_table[0] = 1;
	twobyte_table[1] = 0;
	twiwrite_buf(CURRENT_MONITOR_0, CALLIBRATION_REGISTER, 2, twobyte_table);
	twiwrite_buf(CURRENT_MONITOR_1, CALLIBRATION_REGISTER, 2, twobyte_table);
	twobyte_table[0] = 0;
	sei();
	_delay_ms(10);
	while (1) {
		wdt_reset();
		if (!key_lock && !(PINC & KEY1)) {
			key_lock = 1;
			PORTC &= ~(7 << PC4);
			PORTC ^= (LCD_color++ << PC4);
		} else if (key_lock && (PINC & KEY1)) {
			key_lock++;
		}
		if (!key_lock2 && !(PINC & KEY4)) {
			key_lock2 = 1;
			LCD_Clear();
			LCD_WriteData(x++);
			LCD_WriteData(x++);
			LCD_WriteData(x++);
			LCD_WriteData(x++);
			LCD_WriteData(x++);
		} else if (key_lock2 && (PINC & KEY4)) {
			key_lock2++;
		}
//		USART obsluga
		if (usart_flag) {
			usart_flag = 0;
			number_from_usart = URbuf[Rbuf_ind - 1] - '0';
			switch (menu_ind) {
			case MENU_MAIN: {
				switch (URbuf[Rbuf_ind - 1]) {
				case '1':
					menu_ind = MENU_ENA_VDD_DIG;
					statement(HIGH_OR_LOW);
					break;
				case '2':
					menu_ind = MENU_ENA_VDD_PLL_DLL;
					statement(HIGH_OR_LOW);
					break;
				case '3':
					menu_ind = MENU_WRITE_TO_DAC;
					USART_Transmit_Text("\n\r1 - ADJ ICP DLL"
							"\n\r2 - ADJ I DLL"
							"\n\r3 - ADJ VDD"
							"\n\r4 - ADJ VCO"
							"\n\r5 - ADJ ICP PLL");
					break;
				case '4':
					menu_ind = MENU_READ_CURRENT;
					statement(HIGH_OR_LOW);
					break;
				case '5':
					menu_ind = MENU_READ_VBUS;
					statement(HIGH_OR_LOW);
					break;
				default:
					statement(WRONG_CMD);
					break;
				}
				break;
				case MENU_ENA_VDD_DIG:
				{
					if (!number_from_usart) {
						USART_Transmit_Text("\n\rENA VDD DIG is now LOW");
						PORTE &= ~ENA_VDD_DIG;
					} else {
						USART_Transmit_Text("\n\rENA VDD DIG is now HIGH");
						PORTE |= ENA_VDD_DIG;
					}
					menu_ind = MENU_MAIN;
					statement(MENU_LVL_0);
				}
				break;
				case MENU_ENA_VDD_PLL_DLL:
				{
					if (!number_from_usart) {
						USART_Transmit_Text("\n\rENA VDD PLL DLL is now LOW");
						PORTD &= ~ENA_VDD_PLL_DLL;
					} else {
						USART_Transmit_Text("\n\rENA VDD PLL DLL is now HIGH");
						PORTD |= ENA_VDD_PLL_DLL;
					}
					menu_ind = MENU_MAIN;
					statement(MENU_LVL_0);
				}
				break;
				case 3:
				{
					menu_ind = MENU_NUMBER_TO_DAC;
					switch (URbuf[Rbuf_ind - 1]) {
					case '1':
						write_adr += VOUT_G;
						break;
					case '2':
						write_adr += VOUT_E;
						break;
					case '3':
						write_adr += VOUT_D;
						break;
					case '4':
						write_adr += VOUT_C;
						break;
					case '5':
						write_adr += VOUT_A;
						break;
					default:
						statement(WRONG_CMD);
						menu_ind = MENU_MAIN;
						statement(MENU_LVL_0);
						break;
					}
					if (menu_ind == MENU_NUMBER_TO_DAC) {
						USART_Transmit_Text(
								"\n\rwrite number from [0000,4095] : ");
					}
				}
				break;
				case MENU_NUMBER_TO_DAC:
				{
					if (multiplier != 1) {
						liczba += (number_from_usart) * multiplier;
						multiplier /= 10;
					} else {
						liczba += (number_from_usart);
						if (liczba > 4095) {
							statement(WRONG_CMD);
						} else {
							usart_write_number(liczba);
//						zostawiam zakomentowany preskaler dla VDD PLL DLL
//						liczba = (((1486-liczba)*418)+50)/100; // preskaler
							twobyte_table[0] = (liczba >> 4);
							twobyte_table[1] = (liczba << 4);
							twiwrite_buf(DAC_ADR, write_adr, 2, twobyte_table);
						}
//						test code
//						usart_write_number(liczba);
//						usart_write_number(twobyte_table[0]);
//						usart_write_number(twobyte_table[1]);

//					sprzatanie po sobie
						i = 0;
						menu_ind = 0;
						liczba = 0;
						multiplier = 1000;
						write_adr = WRITE_DAC_CHANNEL;
						twobyte_table[0] = 0;
						twobyte_table[1] = 0;
						statement(MENU_LVL_0);
					}
				}
				break;
				case MENU_READ_CURRENT:
				{
					twobyte_table[0] = 2;
					switch (URbuf[Rbuf_ind - 1]) {
					case '0':
						twiread_buf(CURRENT_MONITOR_0, CURRENT_REGISTER, 2,
								twobyte_table);
						break;
					case '1':
						twiread_buf(CURRENT_MONITOR_1, CURRENT_REGISTER, 2,
								twobyte_table);
						break;
					default:
						statement(WRONG_CMD);
						break;
					}
					statement(NEW_LINE);
					usart_write_number(
							twobyte_table[0] * 256 + twobyte_table[1]);
					statement(NEW_LINE);
					menu_ind = MENU_MAIN;
					statement(MENU_LVL_0);
				}
				break;
				case MENU_READ_VBUS:
				{
					twobyte_table[0] = 2;
					switch (URbuf[Rbuf_ind - 1]) {
					case '0':
						twiread_buf(CURRENT_MONITOR_0, VBUS_REGISTER, 2,
								twobyte_table);
						break;
					case '1':
						twiread_buf(CURRENT_MONITOR_1, VBUS_REGISTER, 2,
								twobyte_table);
						break;
					default:
						statement(WRONG_CMD);
						break;
					}
					statement(NEW_LINE);
					usart_write_number(
							twobyte_table[0] * 256 + twobyte_table[1]);
					statement(NEW_LINE);
					menu_ind = MENU_MAIN;
					statement(MENU_LVL_0);
				}
				break;
				default:
				statement(WRONG_CMD);
				break;
			}
				if (Rbuf_ind > 7) {
					while (Rbuf_ind)
						URbuf[Rbuf_ind--] = 0;
					URbuf[0] = 0;
				}
			}
		}
	}
}
void I2C_SetBusSpeed(uint16_t speed) {
	speed = (F_CPU / speed / 100 - 16) / 2; //speed=TWBR�4^TWPS
	uint8_t prescaler = 0;
	while (speed > 255) //Oblicz warto�� preskalera
	{
		prescaler++;
		speed = speed / 4;
	};
	TWSR = (TWSR & (_BV(TWPS1) | _BV(TWPS0))) | prescaler;
	TWBR = speed;
}
//metoda pomocnicza
void usart_write_number(int number) {
	USART_Transmit_Text("\n\r");
	int mult = 1000;
	while (mult != 1) {
		USART_Transmit('0' + number / mult);
		number %= mult;
		mult /= 10;
	}
	USART_Transmit('0' + number);
}
//wyswietlanie komunikatow
void statement(unsigned char command) {
	switch (command) {
	case 0:
		statement(NEW_LINE);
		USART_Transmit_Text(" 1 - change ENA VDD DIG"
				"\n\r 2 - change ENA VDD PLL DLL"
				"\n\r 3 - Write to DAC"
				"\n\r 4 - Read current from INA226"
				"\n\r 5 - Read Vbus from INA226");
		statement(NEW_LINE);
		break;
	case 1:
		USART_Transmit_Text("wrong command");
		statement(NEW_LINE);
		break;
	case 2:
		USART_Transmit_Text("\n\r");
		break;
	case 3:
		statement(NEW_LINE);
		USART_Transmit_Text("choose 0 or 1");
		statement(NEW_LINE);
		break;
	default:
		USART_Transmit_Text("cos poszlo nie tak");
		break;
	}
}
