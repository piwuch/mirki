/*
 * define.h
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 21-02-2015
 *   	    Author: Karol Witkowski
 */

#ifndef DEFINE_H_
#define DEFINE_H_

#define SET(reg,val)	reg |= (val)
#define CLR(reg,val)	reg &=~(val)
#define TOG(reg,val)	reg ^= (val)

#define ROTATE_RIGHT(x) x=(x>>1)|(x<<7)
#define ROTATE_LEFT(x) x=(x<<1)|(x>>7)

// Makra upraszczaj�ce dost�p do port�w
// *** PORT
#define PORT(x) XPORT(x)
#define XPORT(x) (PORT##x)
// *** PIN
#define PIN(x) XPIN(x)
#define XPIN(x) (PIN##x)
// *** DDR
#define DDR(x) XDDR(x)
#define XDDR(x) (DDR##x)

#define PI		3.14159265359

#endif /* DEFINE_H_ */
