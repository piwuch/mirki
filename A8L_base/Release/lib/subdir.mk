################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lib/I2C.c \
../lib/TIMER.c \
../lib/external_interrupts.c \
../lib/functions.c \
../lib/hd44780.c \
../lib/shift_register.c 

OBJS += \
./lib/I2C.o \
./lib/TIMER.o \
./lib/external_interrupts.o \
./lib/functions.o \
./lib/hd44780.o \
./lib/shift_register.o 

C_DEPS += \
./lib/I2C.d \
./lib/TIMER.d \
./lib/external_interrupts.d \
./lib/functions.d \
./lib/hd44780.d \
./lib/shift_register.d 


# Each subdirectory must supply rules for building sources it contributes
lib/%.o: ../lib/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega8 -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


