/*
 * test_main.c
 *
 *  Created on: 14-11-2014
 *      Author: Ja
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "C:\Users\Ja\Dropbox\workspace\Zestaw bibliotek\makra.h"
#include "lib/external_interrupts.h"
#include "lib/functions.h"
#include "lib/hd44780.h"
#include "lib/I2C.h"
#include "lib/shift_register.h"
#include "lib/TIMER.h"

#define BIP _BV(PB4)
#define BLINK PORTB ^= BIP

#define RTC_ADRESS 0b10100010

#define DISPLAY_0(number) if(number<10) lcd_char('0')

uint8_t cnt = 0;
volatile struct {
	uint8_t countdown :1;
	uint8_t cnt_step :1;
	uint8_t active_alarm :1;
	uint8_t timer1 :1;
	uint8_t timer0 :1;
	uint8_t int1 :1;
	uint8_t int0 :1;
} flags;

// Interrupts -------------------------------------------------
ISR(INT0_vect){
	BLINK;
	flags.int0 = 1;
}
ISR(TIMER1_COMPA_vect) {
//	BLINK;
	if (cnt > 0) {
		cnt = 0;
		flags.timer1 = 1;
	} else
		cnt++;

}
// Main -------------------------------------------------------
int main(void) {
// Variables
	uint8_t i, a=0;
	uint8_t deg[] = { 0b00110, 0b01001, 0b01001, 0b00110, 0b00000, 0b00000,
			0b00000, 0b00000 };
	uint8_t number = 2;
	uint8_t *num = &number;
// Ports
	interrupts_init();
	sr_init();
	DDRB |= BIP;
// I2C
	I2C_set_bitrate(100);
	number = 0b10000011;
	I2C_write_buf(RTC_ADRESS, 0x0D,1, num);
	number = 0x14;
	I2C_write_buf(RTC_ADRESS, 0x02,1,num);
// Timers
	TIMER1_Init();
// ADC
// LCD
	lcd_init();
	lcd_defchar(0x80, deg);
	lcd_cls();
	lcd_str("  10:08:14  18");
	lcd_char(0);
	lcd_char('C');
	lcd_str("  10:08:14  18");
	lcd_char(0);
	lcd_char('C');
	lcd_locate(1, 0);
	lcd_str(" 28.03.2015     Alarm:07:10");
	for (i = 0; i < 10; i++) {
		BLINK;
		_delay_ms(100);
	}
	flags.cnt_step = 0;
	sei();
// Loop ---------------------------------------------------------
	for (;;) {
		if (flags.int0) {
			flags.int0 = 0;

			I2C_read_buf(RTC_ADRESS, 0x04, 1, num);
			number &= ~0b11000000;
			number = BCD_to_dec(number);
			lcd_locate(0, 2);
			DISPLAY_0(number);
			lcd_int(number);

			I2C_read_buf(RTC_ADRESS, 0x03, 1, num);
			number &= ~0b10000000;
			number = BCD_to_dec(number);
			lcd_locate(0, 5);
			DISPLAY_0(number);
			lcd_int(number);

			I2C_read_buf(RTC_ADRESS, 0x02, 1, num);
			number &= ~0b10000000;
			number = BCD_to_dec(number);
			lcd_locate(0, 8);
			DISPLAY_0(number);
			lcd_int(number);
			if(a++%2==0){
				number = (a/2)<<5;
//				sr_reset();
				sr_write(3,num);
			}
		}
	}
}
