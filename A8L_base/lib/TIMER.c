/*
 * Control TIMER
 * File : TIMER.c
 * Microcontroller : Atmel AVR
 * Compilator : avr-gcc
 * Author : Karol Witkowski
 * Date 24.11.2014.
 */

#include "TIMER.h"

void TIMER1_Init() {
	TIMSK |= _BV(OCIE1A) |_BV(TOIE1);
	TCCR1B |= _BV(CS12) | _BV(WGM12);
	OCR1A = 0x7a12; //31250;
}
