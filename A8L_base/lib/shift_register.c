/*
 * shift_register.c
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 03-02-2015
 *   	    Author: Karol Witkowski
 */

#include "shift_register.h"

#include <avr/io.h>

// Signal control macros
#define SET_DATA SET(PORT(SR_DATA_PORT), _BV(SR_DATA))
#define SET_CLK SET(PORT(SR_CLK_PORT), _BV(SR_CLK))
#define CLR_DATA CLR(PORT(SR_DATA_PORT), _BV(SR_DATA))
#define CLR_CLK CLR(PORT(SR_CLK_PORT), _BV(SR_CLK))
#if SR_USE_RESET
#define SET_RESET CLR(PORT(SR_RESET_PORT), _BV(SR_RESET));
#define CLR_RESET SET(PORT(SR_RESET_PORT), _BV(SR_RESET));
#endif /* SR_USE_RESET */

void sr_init() {
	DDR(SR_CLK_PORT) |= _BV(SR_CLK);
	DDR(SR_DATA_PORT) |= _BV(SR_DATA);
#if SR_USE_RESET
	DDR(SR_RESET_PORT) |= _BV(SR_RESET);
	CLR_RESET;
#endif /* SR_USE_RESET */
//	SET_CLK;
//	SET_DATA;
//#if SR_USE_RESET
//	SET_RESET
//	;
//#endif /* SR_USE_RESET */
//	_delay_ms(15);
//	CLR_DATA;
//	CLR_CLK;
//#if SR_USE_RESET
//	CLR_RESET
//	;
//#endif /* SR_USE_RESET */
}

void sr_write(uint8_t len, uint8_t *data) {
	register uint8_t l = 0;
	register int8_t j = 0;
	for (;;) {
		for (j = 7; j >= 0; j--) {
			if (*data & _BV(j)) {
				SET_DATA;
			}
			SET_CLK;
			CLR_CLK;
			CLR_DATA;
			if (++l == len)
				break;

		}
		if (l == len)
			break;

		data++;
	}
}
#if SR_USE_RESET
void sr_reset() {
	SET_RESET;
	CLR_RESET;
}
#endif /* SR_USE_RESET */
