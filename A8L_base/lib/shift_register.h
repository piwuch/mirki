/*
 * shift_register.h
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 03-02-2015
 *   	    Author: Karol Witkowski
 */

// Sterowanie rejestrem przesuwnym o dowolnej d�ugosci za pomoc� 2(3) wyprowadze�.
// Posiadaj�c rejestry o osmiu wyjsciach nale�y po��cyc je ze sob� kaskadowo:
// ostatnie wyjscie rejestru po��czone z wejsciem kolejnego
// wejscia zegarowe wszystkich rejestr�w zwarte
// W ten spos�b z N rejestr�w otrzymac mo�na rejestr o 7N+1 wyjsciach.
#ifndef SHIFT_REGISTER_H_
#define SHIFT_REGISTER_H_

#include <avr/io.h>
#include "C:\Users\Ja\Dropbox\workspace\Zestaw bibliotek\makra.h"
// Konfiguracja kompilacji
#define SR_USE_RESET 1
// Definicje wyprowadze�
#define SR_DATA_PORT  C
#define SR_DATA		  3
#define SR_CLK_PORT	  C
#define SR_CLK		  1
#if SR_USE_RESET
#define SR_RESET_PORT C
#define SR_RESET	  2
#endif /* SR_USE_RESET */
// Deklaracje funkcji
void sr_init();
// wysy�anie rozpoczynane od MSB pierwszego bajtu przez len bit�w
void sr_write(uint8_t len, uint8_t *data);
void sr_reset();
#endif /* SHIFT_REGISTER_H_ */
