/*
 * term.c
 * termometr + I2C + przerwanie
 *  Created on: 18-07-2014
 *      Author: Ja
 */

#define BAUD 4800        //��dana pr�dko�� transmisji
#define FOSC 1000000     // Clock Speed
#define MYUBRR FOSC/16/BAUD-1 //12
#define TA 0b10010000
//#include <stddef.h>
#include <avr\io.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <util/delay.h>
//#include <util/setbaud.h>
#include <util/twi.h>
#include "HD44780.h"
#include "twi.h"

void USART_Init(unsigned int ubrr) {
	UBRRH = (unsigned char) (ubrr >> 8);
	UBRRL = (unsigned char) ubrr;
	UCSRB = (1 << RXEN) | (1 << TXEN);
	UCSRC = (1 << URSEL) | (1 << USBS) | (3 << UCSZ0);
}

void USART_Transmit(unsigned char data) {
	while (!(UCSRA & (1 << UDRE)))
		;
	UDR = data;
}

void I2C_SetBusSpeed(uint16_t speed) {
	speed = (F_CPU / speed / 100 - 16) / 2; //speed=TWBR�4^TWPS
	uint8_t prescaler = 0;
	while (speed > 255) //Oblicz warto�� preskalera
	{
		prescaler++;
		speed = speed / 4;
	};
	TWSR = (TWSR & (_BV(TWPS1) | _BV(TWPS0))) | prescaler;
	TWBR = speed;
}

void temperature_to_text(char a1, char a2, char *text) {
	if (a1 > 127) {
		a1 -= 128;
		*text++ = '-';
	}
	*text++ = '0' + a1 / 10;
	*text++ = '0' + a1 % 10;
	*text++ = '.';
	*text++ = a2 ? '5' : '0';
	*text++ = ' ';
	*text++ = 239;
	*text++ = 'C';

}

ISR(INT0_vect) {
	PORTB ^= _BV(PB2);
}

int main(void) {
	USART_Init( MYUBRR);
	DDRB |= _BV(PB1);
	DDRB |= _BV(PB2);
	MCUCR |= (1 << ISC00);
	GICR |= (1 << INT0);
	sei();
	LCD_Initalize();
	TWCR = _BV(TWEA) | _BV(TWEN); //i2c init
	I2C_SetBusSpeed(100);
	volatile unsigned char a[] = { 0, 0 };
	unsigned char temperature[10];
	uc tOS[] = { 34, 0 };
	uc tHYS[] = { 33, 0 };
	{
		int i;
		for (i = 0; i < 10; i++) {
			temperature[i] = 0;
		}
	}
	twiwrite_buf(TA, 2, 2, tHYS);
	twiwrite_buf(TA, 3, 2, tOS);

	while (1) {
		PORTB ^= _BV(PB1);0
		_delay_ms(500);
		PORTB ^= _BV(PB1);
		_delay_ms(500);
		twiread_buf(TA, 0, 2, a);
		temperature_to_text(a[0], a[1], temperature);
		LCD_Clear();
		LCD_WriteText(temperature);
	}
}
