/*
 * Control ADC
 * File : ADC.h
 * Microcontroller : Atmel AVR
 * Compilator : avr-gcc
 * Author : Karol Witkowski
 * Date 24.11.2014.
 */
#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_
#include <avr/io.h>
/*
 * Deklaracje funkcji
 */

uint8_t BCD_to_dec(uint8_t number);
char* number_to_string(uint32_t number, char str[]);
void EEPROM_write(uint16_t uiAddress, uint8_t ucData);
uint8_t EEPROM_read(uint16_t uiAddress);
#endif
