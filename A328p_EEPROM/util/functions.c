/*
 * Przydatne funkcje
 * File : functions.c
 * Microcontroller : Atmel AVR
 * Compilator : avr-gcc
 * Author : Karol Witkowski
 * Created on : 24.11.2014.
 */
#include "functions.h"

uint8_t BCD_to_dec(uint8_t number) {
	uint8_t temp = (0b00001111 & number) + (10 * (number >> 4));
	return temp;
}

char* number_to_string(uint32_t number, char str[]){
    char const digit[] = "0123456789";
    char* p = str;
    if(number<10){
        *p++ = '0';
    }
    int shifter = number;
    do{ //Move to where representation ends
        ++p;
        shifter = shifter/10;
    }while(shifter);
    *p = '\0';
    do{ //Move back, inserting digits as u go
        *--p = digit[number%10];
        number = number/10;
    }while(number);
    return str;
}

void EEPROM_write(uint16_t uiAddress, uint8_t ucData)
{
/* Wait for completion of previous write */
while(EECR & (1<<EEPE))
;
/* Set up address and Data Registers */
EEAR = uiAddress;
EEDR = ucData;
/* Write logical one to EEMPE */
EECR |= (1<<EEMPE);
/* Start eeprom write by setting EEPE */
EECR |= (1<<EEPE);
}

uint8_t EEPROM_read(uint16_t uiAddress)
{
/* Wait for completion of previous write */
while(EECR & (1<<EEPE))
;
/* Set up address register */
EEAR = uiAddress;
/* Start eeprom read by writing EERE */
EECR |= (1<<EERE);
/* Return data from Data Register */
return EEDR;
}
