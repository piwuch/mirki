/*
 * test_main.c
 *
 *  Created on: 14-11-2014
 *      Author: Ja
 */

#include <avr\io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "util/HD44780.h"
#include "util/functions.h"

#define BIP _BV(PD3)
#define BLINK PORTD ^= BIP
// flag bits:
//7 COUNTDOWN
//6
//5
//4
//3 TIMER1
//2 TIMER0
//1 INT1
//0 INT0
extern volatile uint8_t flag = 0;
#define COUNTDOWN _BV(7)
#define CNT_STEP _BV(6)
#define ACTIVE_ALARM _BV(5)

#define FLAG_TIMER1 _BV(3)
#define FLAG_TIMER0 _BV(2)
#define FLAG_INT1 _BV(1)
#define FLAG_INT0 _BV(0)
#define SET_FLAG(which_flag) flag |= (which_flag)
#define CLEAR_FLAG(which_flag) flag &= ~(which_flag)
#define CHECK_FLAG(which_flag) (flag & (which_flag))

volatile unsigned char a = 0;
// Interrupts -------------------------------------------------

// Main -------------------------------------------------------
int main(void) {
	DDRD |= BIP | PD2;
	PORTD |= (1 << PORTD2);

	SREG |= _BV(7);
	sei();
	uint8_t number = 2;
	LCD_Initalize();
	uint16_t adres = 0;


	for(adres = 0;adres<15;)
	LCD_WriteData(EEPROM_read(adres++));

	for (number = 0; number < 6; number++) {
		BLINK;
		_delay_ms(100);
	}
	_delay_ms(5);
// Loop ---------------------------------------------------------
	for (;;) {

	}
}

