/*
 * Control TIMER
 * File : TIMER.c
 * Microcontroller : Atmel AVR
 * Compilator : avr-gcc
 * Author : Karol Witkowski
 * Date 24.11.2014.
 */

#include "TIMER.h"

void TIMER_Init() {
#if USE_TIMER0
#if TIMER0_OVERFLOW_INTERRUPT_ENABLE
	TIMER_INTERRUPT_MASK_REGISTER |= _BV(TOIE0);
#endif /* TIMER0_OVERFLOW_INTERRUPT_ENABLE */
	TIMER0_COUNTER_CONTROL_REGISTER |= TIMER0_CLOCK_SOURCE;
#endif /* USE_TIMER0 */
#if USE_TIMER1
	TIMSK |= _BV(OCIE1A) |_BV(TOIE1);
	TCCR1B |= _BV(CS12) | _BV(WGM12);
	OCR1A = 0x7a12; //31250;
#endif /* USE_TIMER1 */
}
