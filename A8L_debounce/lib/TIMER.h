/*
 * Control ADC
 * File : ADC.h
 * Microcontroller : Atmel AVR
 * Compilator : avr-gcc
 * Author : Karol Witkowski
 * Date 24.11.2014.
 */

#include <avr/io.h>

///*
// * Konfiguracja sygna��w
// */


#define USE_TIMER0 1

#define TIMER_INTERRUPT_MASK_REGISTER TIMSK
#define TIMER0_OVERFLOW_INTERRUPT_ENABLE 1

#define TIMER0_COUNTER_CONTROL_REGISTER TCCR0
// �r�d�o zegara TIMER0
// 0 - Brak, timer zatrzymany
// 1 - clk / 1
// 2 - clk / 8
// 3 - clk / 64
// 4 - clk / 256
// 5 - clk / 1024
// 6 - negedge on T0 pin
// 7 - posedge on T0 pin
#define TIMER0_CLOCK_SOURCE 4

#define USE_TIMER1 0
///*
// * Deklaracje funkcji
// */

void TIMER_Init();
