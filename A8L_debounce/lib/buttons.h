/*
 * buttons.h
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 21-02-2015
 *   	    Author: Karol Witkowski
 */

#ifndef BUTTONS_H_
#define BUTTONS_H_
#include "../define.h"

#define NUMBER_OF_BUTTONS 1

#define K1PORT D
#define K1PIN  3
#define K2PORT C
#define K2PIN  4
#define K3PORT C
#define K3PIN  4
#define K4PORT C
#define K4PIN  4
#define K5PORT C
#define K5PIN  4

volatile extern uint16_t KeyTimer;

void key_init();

void SuperDebounce(uint8_t * key_state, uint8_t key_number, uint16_t rep_time,
		uint16_t rep_wait, void (*push_proc)(void), void (*rep_proc)(void));

#endif /* BUTTONS_H_ */
