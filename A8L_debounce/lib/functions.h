/*
 * File : functions.h
 * Microcontroller : Atmel AVR
 * Compilator : avr-gcc
 * Author : Karol Witkowski
 * Date 24.11.2014.
 */
#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_
#include <avr/io.h>

// Deklaracje funkcji ---------------------------------------------
uint8_t BCD_to_dec(uint8_t bcd);uint8_t dec_to_BCD(uint8_t dec);
#endif
