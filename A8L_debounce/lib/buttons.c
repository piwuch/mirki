/*
 * buttons.c
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 21-02-2015
 *   	    Author: Karol Witkowski
 */

#include <avr/io.h>
#include "buttons.h"
volatile uint16_t KeyTimer;

void key_init() {
	PORT(K1PORT) |= _BV(K1PIN);
#if NUMBER_OF_BUTTONS > 1
	PORT(K2PORT) |= _BV(K2PIN);
#endif
#if NUMBER_OF_BUTTONS > 2
	PORT(K3PORT) |= _BV(K3PIN);
#endif
#if NUMBER_OF_BUTTONS > 3
	PORT(K4PORT) |= _BV(K4PIN);
#endif
#if NUMBER_OF_BUTTONS > 4
	PORT(K5PORT) |= _BV(K5PIN);
#endif
}

void SuperDebounce(uint8_t * key_state, uint8_t key_number, uint16_t rep_time,
		uint16_t rep_wait, void (*push_proc)(void), void (*rep_proc)(void)) {

	enum {
		idle, debounce, go_rep, wait_rep, rep
	};
	uint8_t key_press = 0;
	switch(key_number){
	case 1:
		key_press = !(PIN(K1PORT) & _BV(K1PIN));
		break;
#if NUMBER_OF_BUTTONS > 1
	case 2:
			uint8_t key_press = !(PIN(K2PORT) & _BV(K2PIN));
			break;
#endif
#if NUMBER_OF_BUTTONS > 2
	case 3:
			uint8_t key_press = !(PIN(K3PORT) & _BV(K3PIN));
			break;
#endif
#if NUMBER_OF_BUTTONS > 3
	case 4:
			uint8_t key_press = !(PIN(K4PORT) & _BV(K4PIN));
			break;
#endif
#if NUMBER_OF_BUTTONS > 4
	case 5:
			uint8_t key_press = !(PIN(K5PORT) & _BV(K5PIN));
			break;
#endif
	}

	if (key_press && !*key_state) {
		*key_state = debounce;
		KeyTimer = 15;
	} else if (*key_state) {

		if (key_press && debounce == *key_state && !KeyTimer) {
			*key_state = 2;
			KeyTimer = 5;
		} else if (!key_press && *key_state > 1 && *key_state < 4) {
			if (push_proc)
				push_proc(); /* KEY_UP */
			*key_state = idle;
		} else if (key_press && go_rep == *key_state && !KeyTimer) {
			*key_state = wait_rep;
			KeyTimer = rep_wait;
		} else if (key_press && wait_rep == *key_state && !KeyTimer) {
			*key_state = rep;
		} else if (key_press && rep == *key_state && !KeyTimer) {
			KeyTimer = rep_time;
			if (rep_proc)
				rep_proc(); /* KEY_REP */
		}
	}
	if (*key_state >= 3 && !key_press)
		*key_state = idle;
}
