/*
 * test_main.c
 *
 *  Created on: 14-11-2014
 *      Author: Ja
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "C:\Users\Ja\Dropbox\workspace\Zestaw bibliotek\makra.h"
#include "lib/buttons.h"
#include "lib/external_interrupts.h"
#include "lib/functions.h"
#include "lib/hd44780.h"
#include "lib/I2C.h"
#include "lib/TIMER.h"

#define BIP _BV(PC3)
#define BLINK PORTC ^= BIP

#define RTC_ADRESS 0b10100010

#define DISPLAY_0(number) if(number<10) lcd_char('0')
#define CURSOR_LCD lcd_locate(y,x)

uint8_t cnt = 0, x, y;
volatile struct {
	uint8_t countdown :1;
	uint8_t cnt_step :1;
	uint8_t active_alarm :1;
	uint8_t timer1 :1;
	uint8_t timer0 :1;
	uint8_t int1 :1;
	uint8_t int0 :1;
} flags;

// Interrupts -------------------------------------------------
ISR(INT0_vect) {
	flags.int0 = 1;
}
ISR(TIMER0_OVF_vect) {
	uint16_t n;
	n = KeyTimer;
	if (n)
		KeyTimer = --n;
}
//ISR(TIMER1_COMPA_vect) {
//	if (cnt > 0) {
//		cnt = 0;
//		flags.timer1 = 1;
//	} else
//		cnt++;
//}
// Declarations
void k1push();
void k1repeat();

// Main -------------------------------------------------------
int main(void) {
// Variables
	uint8_t i;
//	uint8_t zmienna = 0;
	uint8_t k1state;
	uint8_t deg[] = { 0b00110, 0b01001, 0b01001, 0b00110, 0b00000, 0b00000,
			0b00000, 0b00000 };
	uint8_t number = 2;
// Ports
	interrupts_init();
	DDRC |= BIP;
//	BLINK;
//	key_init();
// LCD
	lcd_init();
	lcd_defchar(0x80, deg);
	lcd_cls();
	lcd_str("starting...");
// I2C
	I2C_set_bitrate(100);
	number = 0b10000011;
	I2C_write_buf(RTC_ADRESS, 0x0D, 1, &number);
	number = 0x14;
	I2C_write_buf(RTC_ADRESS, 0x02, 1, &number);
// Timers
	TIMER_Init();

// ADC
// Starting loop
	for (i = 0; i < 10; i++) {
		BLINK;
		_delay_ms(100);
	}
	flags.cnt_step = 0;
	sei();
	lcd_cls();
// Loop ---------------------------------------------------------
	for (;;) {
		SuperDebounce(&k1state, 1, 15, 100, k1push, k1repeat);
	}
}

void k1push() {
	lcd_cls();
	lcd_str("key 1: ");
	y = 0;
	x = 7;
	CURSOR_LCD;
	lcd_int(1);
	cnt = 1;
	BLINK;

}
void k1repeat() {
	CURSOR_LCD;
	lcd_int(++cnt);
}
