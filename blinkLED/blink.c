/*
 /*
 * blink.c
 *
 *  Created on: 16-09-2014
 *      Author: Ja
 */

#define FOSC 96000000
#include <avr\io.h>
#include <stdio.h>
#include <util/delay.h>

#define LED (1<<PB4)
#define T _BV(PB3)
#define KEY1 (1<<PB2)

uint8_t key_lock;

int main(void) {

	PORTB |= KEY1;  // podci�gamy linie klawiszy do VCC
	DDRB |= LED | T;   // pin LED jako WYj�cie
	GIMSK |= _BV(PCIE);
	PCMSK |= _BV(PCINT2);

	_delay_ms(10);

	while (1) {

		if (GIFR && PCIF) {
			GIFR &= ~PCIF;
			// reakcja na PUSH_UP (zwolnienie przycisku)
			PORTB ^= T;
		}

		_delay_ms(1000);
		PORTB ^= LED;
		_delay_ms(28);
		PORTB ^= LED;
	}

}
