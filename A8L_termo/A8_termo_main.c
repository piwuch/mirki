/*
 * test_main.c
 *
 *  Created on: 14-11-2014
 *      Author: Ja
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "C:\Users\Ja\Dropbox\workspace\Zestaw bibliotek\makra.h"
#include "lib/1Wire/ds18x20.h"
#include "lib/external_interrupts.h"
#include "lib/functions.h"
#include "lib/hd44780.h"
#include "lib/I2C.h"

#define BIP _BV(PC3)
#define BLINK PORTC ^= BIP

#define RTC_ADRESS 0b10100010

#define DISPLAY_0(number) if(number<10) lcd_char('0')

uint8_t cnt = 0;
volatile uint8_t sekundy; /* licznik sekund 0-59 */

uint8_t subzero, cel, cel_fract_bits;
volatile struct {
	uint8_t countdown :1;
	uint8_t cnt_step :1;
	uint8_t active_alarm :1;
	uint8_t timer1 :1;
	uint8_t timer0 :1;
	uint8_t int1 :1;
	uint8_t int0 :1;
} flags;

// Interrupts -------------------------------------------------
ISR(INT0_vect) {
	flags.int0 = 1;
	sekundy++;
}
// Main -------------------------------------------------------
void display_temp();
int main(void) {
// Variables
	uint8_t i;
	uint8_t czujniki_cnt;
	uint8_t deg[] = { 0b00110, 0b01001, 0b01001, 0b00110, 0b00000, 0b00000,
			0b00000, 0b00000 };
	uint8_t number = 2;
	uint8_t *num = &number;
// Ports
	interrupts_init();
	DDRC |= BIP;
// I2C
	I2C_set_bitrate(100);
	number = 0b10000011;
	I2C_write_buf(RTC_ADRESS, 0x0D, 1, num);
	number = 0x14;
	I2C_write_buf(RTC_ADRESS, 0x02, 1, num);
// LCD
	lcd_init();
	lcd_defchar(0x80, deg);
	lcd_cls();
	for (i = 0; i < 7; i++) {
		BLINK;
		_delay_ms(100);
	}
	flags.cnt_step = 0;
// TERMO DB
	czujniki_cnt = search_sensors();
	DS18X20_start_meas(DS18X20_POWER_PARASITE, NULL );
	_delay_ms(750);

	sei();
// Loop ---------------------------------------------------------
	for (;;) {
		if (flags.int0) {
			flags.int0 = 0;
			BLINK;
			if (0 == (sekundy % 2))
				DS18X20_start_meas(DS18X20_POWER_EXTERN, NULL );
			if (1 == (sekundy % 2)) {
				if (DS18X20_OK
						== DS18X20_read_meas(gSensorIDs[0], &subzero, &cel,
								&cel_fract_bits)) {
					lcd_cls();
					display_temp(0);
				} else {
					lcd_locate(1, 0);
					lcd_str(" error ");
				}
			}
		}
	}
}
void display_temp() {
	if (subzero)
		lcd_str("-");
	else
		lcd_str(" ");
	lcd_int(cel);
	lcd_str(".");
	lcd_int(cel_fract_bits);
	lcd_char(0);
	lcd_char('C');
}
