################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lib/1Wire/crc8.c \
../lib/1Wire/ds18x20.c \
../lib/1Wire/onewire.c 

OBJS += \
./lib/1Wire/crc8.o \
./lib/1Wire/ds18x20.o \
./lib/1Wire/onewire.o 

C_DEPS += \
./lib/1Wire/crc8.d \
./lib/1Wire/ds18x20.d \
./lib/1Wire/onewire.d 


# Each subdirectory must supply rules for building sources it contributes
lib/1Wire/%.o: ../lib/1Wire/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega8 -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


