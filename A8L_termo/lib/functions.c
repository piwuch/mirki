/*
 * Przydatne funkcje
 * File : functions.c
 * Microcontroller : Atmel AVR
 * Compilator : avr-gcc
 * Author : Karol Witkowski
 * Created on : 24.11.2014.
 */
#include "functions.h"

// konwersja liczby dziesiętnej na BCD
uint8_t dec_to_BCD(uint8_t dec) {
return ((dec / 10)<<4) | (dec % 10);
}

// konwersja liczby BCD na dziesiętną
uint8_t BCD_to_dec(uint8_t bcd) {
    return ((((bcd) >> 4) & 0x0F) * 10) + ((bcd) & 0x0F);
}
