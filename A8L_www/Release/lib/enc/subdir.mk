################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lib/enc/enc28j60.c \
../lib/enc/ip_arp_udp_tcp.c 

OBJS += \
./lib/enc/enc28j60.o \
./lib/enc/ip_arp_udp_tcp.o 

C_DEPS += \
./lib/enc/enc28j60.d \
./lib/enc/ip_arp_udp_tcp.d 


# Each subdirectory must supply rules for building sources it contributes
lib/enc/%.o: ../lib/enc/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega8 -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


