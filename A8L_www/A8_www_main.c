/*
 * test_main.c
 *
 *  Created on: 14-11-2014
 *      Author: Ja
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <string.h>
#include <util/delay.h>

#include "C:\Users\Ja\Dropbox\workspace\Zestaw bibliotek\makra.h"
#include "lib/external_interrupts.h"
#include "lib/functions.h"
#include "lib/hd44780.h"
#include "lib/I2C.h"

#include "lib/enc/ip_arp_udp_tcp.h"
#include "lib/enc/enc28j60.h"
#include "lib/enc/net.h"

#define BIP _BV(PC3)
#define BLINK PORTC ^= BIP

#define RTC_ADRESS 0b10100010

#define DISPLAY_0(number) if(number<10) lcd_char('0')

uint8_t cnt = 0;
uint8_t subzero, cel, cel_fract_bits;
volatile struct {
	uint8_t countdown :1;
	uint8_t cnt_step :1;
	uint8_t active_alarm :1;
	uint8_t timer1 :1;
	uint8_t timer0 :1;
	uint8_t int1 :1;
	uint8_t int0 :1;
} flags;
// ETHERNET --------------------------------------------------

// ustalamy adres MAC
static uint8_t mymac[6] = { 0x54, 0x55, 0x58, 0x10, 0x00, 0x29 };
// ustalamy adres IP urz�dzenia
static uint8_t myip[4] = { 192, 168, 0, 110 };

// server listen port for www
#define MYWWWPORT 80

#define BUFFER_SIZE 450
static uint8_t buf[BUFFER_SIZE + 1];

uint16_t http200ok(void) {
	return (fill_tcp_data_p(buf, 0,
			PSTR("HTTP/1.0 200 OK\r\nContent-Type: text/html\r\nPragma: no-cache\r\n\r\n")));
}

// prepare the webpage by writing the data to the tcp send buffer
uint16_t print_webpage(uint8_t *buf) {
	uint16_t plen;
	plen = http200ok();
	plen = fill_tcp_data_p(buf, plen, PSTR("<pre>"));
	plen = fill_tcp_data_p(buf, plen,
			PSTR("<font color='green' size='6'><b>Witaj !</b>\n</font>"));
	plen =
			fill_tcp_data_p(buf, plen,
					PSTR("<font color='blue'><i>tw�j serwer www dzia�a znakomicie</i>\n\n</font>"));
	plen = fill_tcp_data_p(buf, plen,
			PSTR("<hr><img src=http://www.atnel.pl/atnel_mini.jpg>"));
	plen = fill_tcp_data_p(buf, plen,
			PSTR("<a href=http://www.atnel.pl><br>www.atnel.pl</a>"));
	plen = fill_tcp_data_p(buf, plen, PSTR("</pre>\n"));
	return (plen);
}

// Interrupts -------------------------------------------------
ISR(INT0_vect) {
	flags.int0 = 1;
}
// Main -------------------------------------------------------
int main(void) {
// Variables
	uint8_t i;
	uint8_t deg[] = { 0b00110, 0b01001, 0b01001, 0b00110, 0b00000, 0b00000,
			0b00000, 0b00000 };
	uint8_t number = 2;
	uint8_t *num = &number;
// Ports
	interrupts_init();
	DDRC |= BIP;
// I2C
	I2C_set_bitrate(100);
	number = 0b10000011;
	I2C_write_buf(RTC_ADRESS, 0x0D, 1, num);
	number = 0x14;
	I2C_write_buf(RTC_ADRESS, 0x02, 1, num);
// ETHERNET
	uint16_t dat_p;
// initialize the hardware driver for the enc28j60
	enc28j60Init(mymac);
	enc28j60PhyWrite(PHLCON, 0x476);
// init the ethernet/ip layer:
	init_ip_arp_udp_tcp(mymac, myip, MYWWWPORT);
// LCD
	lcd_init();
	lcd_defchar(0x80, deg);
	lcd_cls();
	lcd_str("starting...");
	for (i = 0; i < 7; i++) {
		BLINK;
		_delay_ms(100);
	}
	flags.cnt_step = 0;

	sei();
// Loop ---------------------------------------------------------
	for (;;) {
		if (flags.int0) {
			flags.int0 = 0;
			BLINK;

		}
// read packet, handle ping and wait for a tcp packet:
		dat_p = packetloop_icmp_tcp(buf,
				enc28j60PacketReceive(BUFFER_SIZE, buf));

/*
 * dat_p will be unequal to zero if there is a valid
 * http get
 * */
		if (dat_p == 0) {
			// no http request
			continue;
		}
// tcp port 80 begin
		if (strncmp("GET ", (char *) &(buf[dat_p]), 4) != 0) {
// head, post and other methods:
			dat_p = http200ok();
			dat_p = fill_tcp_data_p(buf, dat_p, PSTR("<h1>200 OK</h1>"));
			goto SENDTCP;
		}
// just one web page in the "root directory" of the web server
		if (strncmp("/ ", (char *) &(buf[dat_p + 4]), 2) == 0) {
			dat_p = print_webpage(buf);
			goto SENDTCP;
		} else {
			dat_p =
					fill_tcp_data_p(buf, 0,
							PSTR("HTTP/1.0 401 Unauthorized\r\nContent-Type: text/html\r\n\r\n<h1>401 Unauthorized</h1>"));
			goto SENDTCP;
		}
		SENDTCP: www_server_reply(buf, dat_p); // send web page data
// tcp port 80 end
	}
}
