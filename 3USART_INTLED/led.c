/*
 * led.c
 *	led na przerwaniach
 *  Created on: 15-07-2014
 *      Author: Ja
 */
#define BAUD 4800        //��dana pr�dko�� transmisji
#define FOSC 1000000     // Clock Speed
#define MYUBRR FOSC/16/BAUD-1 //12
#include <avr\io.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/setbaud.h>

volatile unsigned char a;       //odebrana liczba X
volatile unsigned char odb_flaga = 0; //flaga informuj�ca main o odebraniu liczby

void USART_Init(unsigned int ubrr) {
	/* Set baud rate */
	UBRRH = (unsigned char) (ubrr >> 8);
	UBRRL = (unsigned char) ubrr;
	/* Enable receiver and 'transmitter' */
	UCSRB = (1 << RXEN)
			| (1 << RXCIE) /*| (1 << TXEN)*/;
	/* Set frame format: 8data, 2stop bit */
	UCSRC = (1 << URSEL) | (1 << USBS) | (3 << UCSZ0);
}

ISR(USART_RXC_vect) {
	//przerwanie generowane po odebraniu bajtu
	a = UDR;   //zapami�taj odebran� liczb�
	odb_flaga = 1; //ustaw flag� odbioru liczby dla main()
}
int main(void) {
	USART_Init( MYUBRR);//inicjalizacja USART
	sei(); //przerwania on
	DDRB |= _BV(PB1) | _BV(PB2);
	while (1) {
		if (odb_flaga) {
			odb_flaga = 0; //zga� flag�
			if (a == 'h') {
				PORTB |= _BV(PB1);//led on
			} else if (a == 'j') {
				PORTB &= ~_BV(PB1);// led off
			} else {
				PORTB |= _BV(PB2);// BIP
				_delay_ms(50);
				PORTB &= ~_BV(PB2);
				_delay_ms(50);
				PORTB |= _BV(PB2);// BIP
				_delay_ms(50);
				PORTB &= ~_BV(PB2);
			}
		}

	}
}
