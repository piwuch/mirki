/*
 * test_main.c
 *
 *  Created on: 14-11-2014
 *      Author: Ja
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "util/HD44780.h"
#include "util/functions.h"
#include "util/twi.h"

#define SET_TIME 0
#define GODZINA 2236
#define HH (((GODZINA/100)%10)) + (((GODZINA/1000)%10)<<4)
#define MM GODZINA%10 + (((GODZINA/10)%10)<<4)

#define BIP _BV(PC1)
#define BLINK PORTC ^= BIP
#define RTC 0b10100010
volatile struct {
	uint8_t countdown :1;
	uint8_t cnt_step :1;
	uint8_t active_alarm :1;
	uint8_t timer1 :1;
	uint8_t timer0 :1;
	uint8_t int1 :1;
	uint8_t int0 :1;
} flags;

char str[16];
//volatile unsigned char a = 0;
// Interrupts -------------------------------------------------
ISR(TIMER1_COMPA_vect) {
	flags.timer1 = 1;
}

ISR(INT0_vect) {
	flags.int0 = 1;
}
//void blink_loop(uint8_t blinks, uint16_t delay);
// Main -------------------------------------------------------
int main(void) {
	DDRC |= BIP;
	DDRD |= PD2;
	PORTD |= (1 << PORTD2);
	EICRA |= _BV(ISC00);
	EIMSK |= _BV(INT0);

	TIMSK1 |= _BV(OCIE1A);
	TCCR1B |= _BV(CS12) | _BV(WGM12);
	OCR1A = 0x7a12; //31250;
	SREG |= _BV(7);
	sei();
	uint8_t number = 2;
	uint8_t *num = &number;
	uint8_t seconds = 0;
	uint16_t alarm = 0;
	LCD_Initalize();
	LCD_WriteCommand(HD44780_DISPLAY_ONOFF | HD44780_DISPLAY_ON);
//	blink_loop(4,100);
	TWI_SetBusSpeed(100);
//	blink_loop(4,500);
	number = 0b10000011;
	TWI_write_buf(RTC, 0x0D, 1, num);
	if (SET_TIME) {
		number = HH;
		TWI_write_buf(RTC, 0x04, 1, num);
		number = MM;
		TWI_write_buf(RTC, 0x03, 1, num);
	}
	_delay_ms(5);
// Loop ---------------------------------------------------------
	for (;;) {
		if (flags.int0) {
			flags.int0 = 0;
			if (flags.active_alarm)
				flags.active_alarm = 0;
			else
				flags.countdown = 1;
//			alarm = 27000;
			alarm = 1560;
			alarm = 15;
		}
		if (flags.timer1) {
			flags.timer1 = 0;
			LCD_Clear();
			TWI_read_buf(RTC, 0x04, 1, num);
			number &= ~0b11000000;
			LCD_WriteText(BCD_to_BIN_STR(number, str));
			LCD_GoTo(36, 0);
			LCD_WriteText(BCD_to_BIN_STR(number, str));
			LCD_GoTo(4, 0);
			number = BCD_to_dec(number);
//
			LCD_WriteText(number_to_string(number, str)); //__112222_3334444
			LCD_WriteData(':');
			TWI_read_buf(RTC, 0x03, 1, num);
			number &= ~0b10000000;
			LCD_GoTo(12, 0);
			LCD_WriteText(BCD_to_BIN_STR(number, str));
			LCD_GoTo(8, 1);
			LCD_WriteText(BCD_to_BIN_STR(number, str));
			LCD_GoTo(8, 1);
			LCD_WriteText("    ");
			LCD_GoTo(7, 0);
			number = BCD_to_dec(number);
			LCD_WriteText(number_to_string(number, str));
			LCD_WriteData(':');
			TWI_read_buf(RTC, 0x02, 1, num);
			number &= ~0b10000000;
			number = BCD_to_dec(number);
			LCD_WriteText(number_to_string(number, str));
			if (seconds == number)
				flags.cnt_step = 0;
			else {
				seconds = number;
				flags.cnt_step = 1;
			}
			if (flags.countdown) {
				LCD_GoTo(0, 1);
				number = alarm / 3600;
				LCD_WriteText(number_to_string(number, str));
				LCD_WriteData(':');
				number = alarm / 60 - number * 60;
				LCD_WriteText(number_to_string(number, str));
				LCD_WriteData(':');
				number = alarm % 60;
				LCD_WriteText(number_to_string(number, str));
				if (flags.cnt_step)
					alarm--;
				if (alarm == 0) {
					flags.countdown = 0;
					flags.active_alarm = 1;
				}
			}
			if (flags.active_alarm) {
				BLINK;
				_delay_ms(30);
				BLINK;
				_delay_ms(50);
				BLINK;
				_delay_ms(30);
				BLINK;

			}
		}
	}
}

//void blink_loop(uint8_t blinks, uint16_t delay){
//	int i = 0;
//	for(;i<2*blinks;i++){
//		BLINK;
//		_delay_ms(delay);
//	}
//}
