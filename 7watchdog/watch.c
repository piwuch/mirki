/*
 * watch.c
 *
 *  Created on: 30-07-2014
 *      Author: Ja
 */

//#include <stddef.h>
#include <avr\io.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/wdt.h>
//#include <stdint.h>

volatile unsigned char i = 0;

ISR(INT0_vect) {
	i = 1;
}

int main(void) {
	DDRB |= _BV(PB1) | _BV(PB2);
	MCUCR |= (1 << ISC00);
	GICR |= (1 << INT0);

	WDTCR |= _BV(WDE) | _BV(WDP0) | _BV(WDP1) | _BV(WDP2);

	sei();
	while (1) {
		PORTB ^= _BV(PB1);
		_delay_ms(30);
		wdt_reset();
		if (i) {
			while (1) {
				PORTB ^= _BV(PB2);
				_delay_ms(30);
			}
		}
	}
} //main
