/*
 * Przydatne funkcje
 * File : functions.c
 * Microcontroller : Atmel AVR
 * Compilator : avr-gcc
 * Author : Karol Witkowski
 * Created on : 24.11.2014.
 */
#include "functions.h"

uint8_t BCD_to_dec(uint8_t number) {
	uint8_t temp = 00001111 & number;
	temp += 10 * (number > 4);
	return temp;
}

char* number_to_string(uint32_t number, char str[]){
    char const digit[] = "0123456789";
    char* p = str;
    if(number<0){
        *p++ = '-';
        number *= -1;
    }
    int shifter = number;
    do{ //Move to where representation ends
        ++p;
        shifter = shifter/10;
    }while(shifter);
    *p = '\0';
    do{ //Move back, inserting digits as u go
        *--p = digit[number%10];
        number = number/10;
    }while(number);
    return str;
}
