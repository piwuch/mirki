/*
 * Control ADC
 * File : ADC.c
 * Microcontroller : Atmel AVR
 * Compilator : avr-gcc
 * Author : Karol Witkowski
 * Date 24.11.2014.
 */

#include "adc.h"

void TIMER1_Init() {
	TIMSK1 |= _BV(OCIE1A);
	TCCR1B |= _BV(CS12) | _BV(WGM12);
	OCR1A = 0x7a12; //31250;
}
