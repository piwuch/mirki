/*
 * Control ADC
 * File : ADC.h
 * Microcontroller : Atmel AVR
 * Compilator : avr-gcc
 * Author : Karol Witkowski
 * Date 24.11.2014.
 */

#include <avr/io.h>

/*
 * Konfiguracja sygna��w
 */
#define ADC_AUTO_TRIGGER_ENABLE		0
#define ADC_INTERRUPT_ENABLE		0

//Voltage reference selection:
//0: AREF
//1: AVCC
//3: Internal 1.1V (AT328)
#define ADC_VOLTAGE_REFERENCE 		3

//Auto trigger source
//0: Free Running mode
//1: Analog Comparator
//2: External Interrupt Request 0
//3: Timer/Counter0 Compare Match A
//4: Timer/Counter0 Overflow
//5: Timer/Counter1 Compare Match B
//6: Timer/Counter1 Overflow
//7: Timer/Counter1 Capture Event
#define ADC_AUTO_TRIGGER_SOURCE		0

/*
 * Deklaracje funkcji
 */

void ADC_Init(uint8_t prescaler);
void ADC_MUX_Select(uint8_t adc_channel);
uint16_t ADC_read();
uint16_t ADC_read_channel(uint8_t adc_channel);

