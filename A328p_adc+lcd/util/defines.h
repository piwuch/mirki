/*
 * defines.h
 *
 *  Created on: 23-12-2014
 *      Author: Ja
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#define PORT(port) SPORT(port)
#define SPORT(port) (PORT##port)

#define DDR(port) SDDR(port)
#define SDDR(port) (DDR##port)

#define PIN(port) SPIN(port)
#define SPIN(port) (PIN##port)

#define P(port, pin) SECONDP(port,pin)
#define SECONDP(port, pin) (P##port##pin)

#define SET_AS_OUT(port, pin) DDR(port) |= _BV(P(port, pin))
#define SET_AS_IN(port, pin) DDR(port) &= ~_BV(P(port, pin))

#define GET_STATE(port, pin) (PIN(port)&P(port,pin))

#define SET(port, pin) PORT(port) |= _BV(P(port, pin))
#define CLR(port, pin) PORT(port) &=~_BV(P(port, pin))
#define TOG(port, pin) PORT(port) ^= _BV(P(port, pin))

#endif /* DEFINES_H_ */
