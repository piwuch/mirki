/*
 * test_main.c
 *
 *  Created on: 14-11-2014
 *      Author: Ja
 */
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "util/defines.h"
#include "util/HD44780.h"
#include "util/functions.h"
#include "util/ADC.h"
#include "util/TIMER.h"

#define LED 1
#define LEDP C
#define BLINK PORTC ^= LED

volatile unsigned char a;       //odebrana liczba a
volatile struct {
	uint8_t countdown :1;
	uint8_t cnt_step :1;
	uint8_t active_alarm :1;
	uint8_t timer1 :1;
	uint8_t timer0 :1;
	uint8_t int1 :1;
	uint8_t int0 :1;
} flags;
// Interrupts -------------------------------------------------
ISR(TIMER1_COMPA_vect) {
	flags.timer1 = 1;

}
void blink_loop(uint8_t blinks, uint16_t delay);
int main(void) {
	LCD_Initalize();
	LCD_WriteText("starting...");
	TIMER1_Init();
	SET_AS_OUT(LEDP,LED);
	blink_loop(3,100);
	ADC_Init(64);
	ADC_MUX_Select(0);

	char str[15];
	uint16_t number;
	sei();
	_delay_ms(50);

	for (;;) {

		if (flags.timer1) {
			flags.timer1 = 0;
			LCD_Clear();
			number = ADC_read();
			LCD_WriteText(number_to_string(number, str));
		}
	}
}

void blink_loop(uint8_t blinks, uint16_t delay){
	int i = 0;
	for(;i<2*blinks;i++){
		TOG(LEDP,LED);
		_delay_ms(delay);
	}
}
