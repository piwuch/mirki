################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../util/ADC.c \
../util/HD44780.c \
../util/TIMER.c \
../util/functions.c 

OBJS += \
./util/ADC.o \
./util/HD44780.o \
./util/TIMER.o \
./util/functions.o 

C_DEPS += \
./util/ADC.d \
./util/HD44780.d \
./util/TIMER.d \
./util/functions.d 


# Each subdirectory must supply rules for building sources it contributes
util/%.o: ../util/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


