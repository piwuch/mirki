/*
 * main.c
 *
 *  Created on: 16-09-2014
 *      Author: Ja
 */

#define FOSC 96000000
#include <avr\io.h>
#include <avr\sleep.h>
#include <avr\power.h>
#include <avr\interrupt.h>
#include <stdio.h>
#include <util/delay.h>

#define LED _BV(PB3)
//#define T   _BV(PB3)

void slp();
//#define SENSOR_LETTER _BV(PB1)
#define SENSOR_OPEN _BV(PB0)
ISR(PCINT0_vect) {
//	PORTB ^= T;
//	slp();
//	_delay_ms(100);
}
ISR(INT0_vect) {
	PORTB &= ~LED;
	slp();
//	_delay_ms(100);
}
uint8_t key_lock = 0;

int main(void) {

	PORTB |=
//			SENSOR_LETTER
//
			SENSOR_OPEN;
	DDRB |= LED ;
	GIMSK |= _BV(INT0) | _BV(PCIE);
	MCUCR |= _BV(ISC00) | _BV(SM1);
	SREG |= _BV(7);
	PCMSK |= _BV(PCINT0);
//	eclipse doesn't recognise
//	PRR |= _BV(PRTIM0) |_BV(PRADC);
	(*(volatile uint8_t *) ((0x25) + 0x20)) |= _BV(1) | _BV(0);
	_delay_ms(5);
	int i;
	for (i = 0; i < 10; i++) {
		PORTB ^= LED;
		_delay_ms(100);
	}
	_delay_ms(5);
	sei();
//	GIFR &= ~PCIF;
	_delay_ms(5);
	while (1) {
//		if (PORTB && T) {
////			GIFR &= ~PCIF;
////			PORTB ^= T;
//			slp();
//		}

		_delay_ms(1000);
		PORTB ^= LED;
		_delay_ms(28);
		PORTB ^= LED;
//		slp();

	}
}

void slp() {
	cli();
	sleep_enable()
	;
	sei();
	sleep_cpu()
	;
	sleep_disable()
	;
}
