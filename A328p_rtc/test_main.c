/*
 * test_main.c
 *
 *  Created on: 14-11-2014
 *      Author: Ja
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "util/HD44780.h"
//#include "util/UART.h"
#include "util/functions.h"
//#include "util/ADC.h"
#include "util/twi.h"

#define LED _BV(PD3)
#define BLINK PORTD ^= LED
#define ADCIN PC5   //definicja ADCIN (wej�cie ADC)
#define RTC 0b10100010
volatile unsigned char a;       //odebrana liczba a
volatile uint8_t timer_flag = 0;
volatile uint8_t int0_flag = 0;


ISR(TIMER1_COMPA_vect) {
	timer_flag++;
}

ISR(INT0_vect){
	int0_flag++;
	BLINK;
}

int main(void) {
	DDRD |= LED | PD2;
	PORTD |= (1 << PORTD2);
	EICRA |= _BV(ISC00);
	EIMSK |= _BV(INT0);

	TIMSK1 |= _BV(OCIE1A);
	TCCR1B |= _BV(CS12) | _BV(WGM12);
	OCR1A = 0x7a12; //31250;
	SREG |= _BV(7);
	sei();
	char str[15];
	uint8_t number = 2;
	uint8_t *num = &number;
	LCD_Initalize();

	for(number = 0;number<10;number++){
		BLINK;
		_delay_ms(300);
	}
	number = 2;
	TWI_SetBusSpeed(100);
//	TWI_write_buf(RTC, 0x01, 1, num);
	number = 0b10000011;
	TWI_write_buf(RTC, 0x0D, 1, num);
	_delay_ms(5);
	for (;;) {
//		if(int0_flag){
//			int0_flag = 0;
////			number++;
//		}
		if (timer_flag) {
			timer_flag = 0;
			LCD_Clear();
			TWI_read_buf(RTC,0x02,1,num);
//			number &= ~0b10000000;
//			number = 00001111 & number + (0);
			number = BCD_to_dec(number);
			LCD_WriteText(number_to_string(number, str));
		}
	}
}

