/*
 * TWI_.h
 *
 *  Created on: 21-07-2014
 *      Author: Ja
 */

#include <avr/io.h>
#include <util/twi.h>
#define ACK 1
#define NOACK 0

typedef uint8_t uc;

// procedura transmisji sygna�u START
void TWI_start(void);
// procedura transmisji sygna�u STOP
void TWI_stop(void);
// procedura transmisji bajtu danych
void TWI_write(uc data);
//procedura odczytu bajtu danych
uc TWI_read(uc ack);
//procedura transmisji ci�gu danych
void TWI_write_buf(uc SLA, uc adr, uc len, uc *buf);
//procedura odczytu ci�gu danych
void TWI_read_buf(uc SLA, uc adr, uc len, uc *buf);
void TWI_SetBusSpeed(uint16_t speed);
