/*
 * external_interrupts.h
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 03-02-2015
 *   	    Author: Karol Witkowski
 */

#ifndef EXTERNAL_INTERRUPTS_H_
#define EXTERNAL_INTERRUPTS_H_

#include <avr/io.h>

#define INTERRUPT_0_ENABLE 1
#define INTERRUPT_1_ENABLE 0

// INTERRUPT SENSE CONTROL
// Generate interrupt on:
// 0: LOW state
// 1: Logical change
// 2: Falling edge
// 3: Rising edge
#define INT0_SENSE_CONTROL 3
#define INT1_SENSE_CONTROL 0

// Function declarations
void interrupts_init();
#endif /* EXTERNAL_INTERRUPTS_H_ */
