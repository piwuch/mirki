/*
 * external_interrupts.c
 * 
 *	Microcontroller: Atmel AVR
 *  	  Compiler: AVR-gcc
 * 		Created on: 03-02-2015
 *   	    Author: Karol Witkowski
 */

#include <avr/io.h>
#include "external_interrupts.h"

void interrupts_init(){
#if	INTERRUPT_0_ENABLE == 1
	MCUCR |= INT0_SENSE_CONTROL;
	GICR |= _BV(6);
#endif

#if INTERRUPT_1_ENABLE == 1
	MCUCR |= (INT1_SENSE_CONTROL<<2);
	GICR |= _BV(7);

#endif
}

