/*
 * test_main.c
 *
 *  Created on: 14-11-2014
 *      Author: Ja
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

//#include "C:\Users\Ja\Dropbox\workspace\Zestaw bibliotek\makra.h"
#include "lib/external_interrupts.h"
#include "lib/hd44780.h"
#include "lib/I2C.h"

#define BIP _BV(PB4)
#define BLINK PORTB ^= BIP
#define BEEP  PORTC ^= _BV(PC0)

#define RTC_ADRESS 0b10100010

#define DISPLAY_0(number) if(number<10) lcd_char('0')

uint8_t cnt = 0;
volatile struct {
	uint8_t countdown :1;
	uint8_t cnt_step :1;
	uint8_t active_alarm :1;
	uint8_t timer1 :1;
	uint8_t timer0 :1;
	uint8_t int1 :1;
	uint8_t int0 :1;
} flags;

// Interrupts -------------------------------------------------
ISR(INT0_vect) {
	BLINK;
	flags.int0 = 1;
}
// Main -------------------------------------------------------
int main(void) {
// LCD
	lcd_init();
	lcd_cls();
	lcd_str("loading... ");

// Variables
	uint8_t i, min = 10, sec = 0;
	int8_t dots = -1;
	uint8_t number = 0b10110000;
	uint8_t *num = &number;
// Ports
	interrupts_init();
	DDRB |= BIP;
	DDRC |= _BV(PC0);
	BEEP;
	// I2C
	I2C_set_bitrate(100);
	number = 0b10000011;
	I2C_write_buf(RTC_ADRESS, 0x0D, 1, num);
	number = 0x14;
	I2C_write_buf(RTC_ADRESS, 0x02, 1, num);

	BEEP;
	_delay_ms(20);
	BEEP;
	lcd_cls();
	lcd_locate(1, 11);
	lcd_str("10:00");
	sei();
// Loop ---------------------------------------------------------
	for (;;) {
		if (flags.int0) {
			flags.int0 = 0;
			if (sec % 20 == 0)
				dots++;

			if (min == 0 && sec == 0) {
				lcd_cls();
				lcd_str("     koniec");
			} else {
				if (sec == 0) {
					sec = 59;
					min--;
				}
				sec--;
				lcd_cls();
				for (i = dots; i > 0; i--) {
					lcd_char(0xFF);
				}
				lcd_locate(1, 0);
				for (i = dots; i > 14; i--) {
					lcd_char(0xFF);
				}
				if (min > 4)
					lcd_locate(1, 11);
				else
					lcd_locate(0, 0);
				DISPLAY_0(min);
				lcd_int(min);
				lcd_char(':');
				DISPLAY_0(sec);
				lcd_int(sec);
				if (min == 2 && sec == 0) {
					BEEP;
					_delay_ms(30);
					BEEP;
					_delay_ms(50);
					BEEP;
					_delay_ms(30);
					BEEP;
				}
				if (min == 1 && sec == 0) {
					BEEP;
					_delay_ms(30);
					BEEP;
				}
			}
		}
	}
}
