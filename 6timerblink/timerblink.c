/*
 * timerblink.c
 *
 *  Created on: 25-07-2014
 *      Author: Ja
 */

#include <avr\io.h>
#include <avr/interrupt.h>

ISR(TIMER1_COMPA_vect) {
	PORTB ^= _BV(PB1);
}

int main(){
	DDRB |= _BV(PB1);
	TIMSK |= _BV(OCIE1A);
	TCCR1B |= _BV(CS10) | _BV(CS12) | _BV(WGM12);
	OCR1A = 0x0bb8;


	sei();
	for(;;){

	}
}

