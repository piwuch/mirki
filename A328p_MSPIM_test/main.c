/*
 * test_main.c
 *
 *  Created on: 14-11-2014
 *      Author: Ja
 */

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "util/HD44780.h"
#include "util/functions.h"
#include "util/MSPIM.h"

#define BIP _BV(PD3)
#define BLINK PORTD ^= BIP
#define SS _BV(PC0)
#define SS1 PORTC |= SS;
#define SS0 PORTC &= ~SS

// flag bits:
//7 COUNTDOWN
//6
//5
//4
//3 TIMER1
//2 TIMER0
//1 INT1
//0 INT0
volatile uint8_t flag = 0;
#define COUNTDOWN _BV(7)
#define CNT_STEP _BV(6)
#define ACTIVE_ALARM _BV(5)

#define FLAG_TIMER1 _BV(3)
#define FLAG_TIMER0 _BV(2)
#define FLAG_INT1 _BV(1)
#define FLAG_INT0 _BV(0)
#define SET_FLAG(which_flag) flag |= (which_flag)
#define CLEAR_FLAG(which_flag) flag &= ~(which_flag)
#define CHECK_FLAG(which_flag) (flag & (which_flag))

volatile unsigned char a = 0;
// Interrupts -------------------------------------------------
ISR(TIMER1_COMPA_vect) {
	SET_FLAG(FLAG_TIMER1);
}

ISR(INT0_vect) {
	SET_FLAG(FLAG_INT0);
}

// Main -------------------------------------------------------
int main(void) {
	DDRD |= BIP | PD2;
	DDRC |= SS;

	PORTD |= (1 << PORTD2);
	SS1
	;
	EICRA |= _BV(ISC00);
	EIMSK |= _BV(INT0);

	TIMSK1 |= _BV(OCIE1A);
	TCCR1B |= _BV(CS12) | _BV(WGM12);
//	OCR1A = 0x7a12; //31250;
	OCR1A = 10000; //31250;
	sei();
	char str[15];
	uint8_t tab[5] = { 0, 0, 0, 0, 0};
	uint8_t number = 2;
	uint16_t axis = 0;
	LCD_Initalize();
	MSPIM_Init(9600);
	for (number = 0; number < 6; number++) {
		BLINK;
		_delay_ms(100);
	}
	_delay_ms(5);
// Loop ---------------------------------------------------------
	for (;;) {
		if (CHECK_FLAG(FLAG_TIMER1)) {
			CLEAR_FLAG(FLAG_TIMER1);
//			BLINK;
//			_delay_ms(300);
//			BLINK;
//			_delay_ms(300);

			LCD_Clear();
			SS0;
			for (number = 0; number < 5; number++)
				tab[number] = MSPIM_Transmit(number);
			SS1
			;
//			BLINK;
//			_delay_ms(300);
//			BLINK;
//			_delay_ms(300);
			number = 0;
			axis = tab[0]+(tab[1]<<8);
			LCD_WriteText(number_to_string(axis, str));
			LCD_WriteText(" - Xaxis");
			LCD_GoTo(0, 1);

			axis = tab[2]+(tab[3]<<8);
			LCD_WriteText(number_to_string(axis, str));
			LCD_WriteText(" - Yaxis ");

			if(tab[4]&1)
				LCD_WriteData('1');
			if(tab[4]&2)
				LCD_WriteData('2');
			if(tab[4]&4)
				LCD_WriteData('3');
		}
	}
}

