/***************************************************************************
 *   Copyright (C) 2008 by Damian Kmiecik                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *                              CRC8 FILE                                  *
 *                                                                         *
 ***************************************************************************/
//Headers
#include <inttypes.h>			//INTTYPES
//Definitions

//Calculate CRC8
uint8_t crc8(uint8_t* data, uint8_t size) {
	uint8_t crc8 = 0;
	for(uint8_t i = 0; i < size; i++) {
		uint8_t byte;
		uint8_t bit;
		byte = data[i];
		for(uint8_t n = 8; n > 0; n--) {
			bit = (crc8 ^ byte) & 0x01;
			if(bit == 1) {
				crc8 ^= 0x18;
			}
			crc8 >>= 1;
			crc8 &= 0x7F;
			if(bit == 1) {
				crc8 |= 0x80;
			}
			byte >>= 1;
		}
	}
	return crc8;
}
