/***************************************************************************
 *   Copyright (C) 2009 by Damian Kmiecik                                  *
 *   d0zoenator@gmail.com						   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

/**
 * Part of my the Little IP Stack
 * Checksum
 *
 * @author 	Damian Kmiecik
 * @processor 	AVR
 * @package 	checksum
 * @compiler 	avr-gcc
 */

#ifndef _CHECKSUM_H
#define	_CHECKSUM_H

#ifdef	__cplusplus
extern "C" {
#endif

/**
 * Includes
 */
#include <inttypes.h>

/**
 * Compute checksum
 * @param uint8_t* pointer
 * @param uint16_t result16
 * @param uint32_t result32
 * @return uint16_t
 */
extern uint16_t checksum( uint8_t* pointer, uint16_t result16, uint32_t result32 );

#ifdef	__cplusplus
}
#endif

#endif	/* _CHECKSUM_H */
