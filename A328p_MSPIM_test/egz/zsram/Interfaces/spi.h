/***************************************************************************
 *   Copyright (C) 2009 by Damian Kmiecik                                  *
 *   d0zoenator@gmail.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

/**
 * SPI software/hardware implementation
 *
 * @author 	Damian Kmiecik
 * @processor 	AVR
 * @package 	spi
 * @compiler 	avr-gcc
 */

#ifndef _SPI_H
#define	_SPI_H

#ifdef	__cplusplus
extern "C"
{
#endif

/**
 * Includes
 */
#include <inttypes.h>

/**
 * Config I/O
 */
#define SPI_PORT        PORTB
#define SPI_DDR         DDRB
#define SPI_PIN         PINB
#define SPI_MOSI        PB5
#define SPI_MISO        PB6
#define SPI_SCK         PB7

/**
 * Config functions
 * Comment/uncomment to enable/disable function
 */
#define SPI_READ
#define SPI_SOFTWARE

/**
 * SPI initialize
 * Must be run before any usage of spi functions
 */
extern void spiInit(void);

/**
 * Write byte to SPI
 * @param uint8_t val
 */
extern void spiWrite(const uint8_t val);

/**
 * Read byte from SPI
 * @return uint8_t
 */
#ifdef SPI_READ
extern uint8_t spiRead(void);
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* _SPI_H */
