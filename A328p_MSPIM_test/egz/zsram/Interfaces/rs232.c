/*
 * uart.c
 *
 *  Created on: 2009-12-27
 *      Author: damian
 */

#include "rs232.h"
#include <avr/io.h>
#include <avr/pgmspace.h>

#define BAUDRATE	((F_CPU / (8L * UART_BAUDRATE)) - 1L)

void uart_init(void)
{
	UCSRA = _BV(U2X);
	UBRRH = (uint8_t)(BAUDRATE >> 8);
	UBRRL = (uint8_t)(BAUDRATE);
#ifdef UART_RECEIVE
	UCRB = _BV(TXEN) | _BV(RXEN);
#else
	UCRB = _BV(TXEN);
#endif
}

void uart_putc(const uint8_t val)
{
	UDR = val;
	loop_until_bit_is_set(UCRA, UDRE);
}

#ifdef UART_SEND_STR
void uart_puts(const char* text)
{
	while (*text)
	{
		uart_putc(*text);
		text++;
	}
}
#endif

#ifdef UART_PGM
#ifdef UART_SEND_STR
void uart_puts_p(const char* text)
{
	while (pgm_read_byte(text))
	{
		uart_putc(pgm_read_byte(text));
		text++;
	}
}
#endif
#endif

#ifdef UART_RECEIVE
uint8_t uart_getc(void)
{
	loop_until_bit_is_set(UCRA, RXC);
	return UDR;
}
#endif
