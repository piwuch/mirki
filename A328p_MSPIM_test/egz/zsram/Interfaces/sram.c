/***************************************************************************
 *   Copyright (C) 2009 by Damian Kmiecik                                  *
 *   d0zoenator@gmail.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

/**
 * External RAM latch addressed
 *
 * @author 	Damian Kmiecik <d0zoenator@gmail.com>
 * @processor 	AVR
 * @package 	sram
 * @compiler 	avr-gcc
 */

#include "sram.h"
#include <util/delay.h>

uint16_t sramAddr = 0;

#define SRAM_WE_HIGH		SRAM_STR_PORT |= _BV(SRAM_WE_PIN)
#define SRAM_WE_LOW			SRAM_STR_PORT &= ~_BV(SRAM_WE_PIN)
#define SRAM_LE_HIGH		SRAM_STR_PORT |= _BV(SRAM_LE_PIN)
#define SRAM_LE_LOW			SRAM_STR_PORT &= ~_BV(SRAM_LE_PIN)

/**
 * Initialize
 */
void sramInit(void) {
	SRAM_DATA_DRR = 0x00;
	SRAM_DATA_PORT = 0x00;
	SRAM_ADDR_DDR = 0xFF;
	SRAM_STR_DDR |= _BV(SRAM_WE_PIN) | _BV(SRAM_LE_PIN);
	SRAM_WE_HIGH;
	SRAM_LE_LOW;
	for (uint16_t i = 0; i < SRAM_SIZE; i++)
		sramWriteByte(i, 0x00);
}

/**
 * Set address
 * @param uint16_t addr
 */
inline void sramSetAddr(const uint16_t addr) {
	if (addr != sramAddr) {
		SRAM_ADDR_PORT = ((addr >> 6) & 0xFF);
		if (addr & _BV(8))
			SRAM_ADDR_PORT |= _BV(0);
		else
			SRAM_ADDR_PORT &= ~_BV(0);
		SRAM_LE_HIGH;
		SRAM_LE_LOW;
		SRAM_ADDR_PORT = (addr & 0xFF);
		sramAddr = addr;
	}
}

/**
 * Write byte
 * @param uint16_t addr
 */
void sramWriteByte(const uint16_t addr, const uint8_t data) {
	sramSetAddr(addr);
	SRAM_WE_LOW;
	SRAM_DATA_DRR = 0xFF;
	SRAM_DATA_PORT = data;
	SRAM_WE_HIGH;
	SRAM_DATA_DRR = 0x00;
}

/**
 * Read byte
 * @param uint16_t addr
 */
uint8_t sramReadByte(const uint16_t addr) {
	sramSetAddr(addr);
	return SRAM_DATA_PINx;
}

/**
 * Write
 */
void sramWrite(uint16_t addr, uint16_t len, const uint8_t *data) {
	while (len--) {
		sramWriteByte(addr++, *data++);
	}
}

/**
 * Read
 */
void sramRead(uint16_t addr, uint16_t len, uint8_t *data) {
	while (len--) {
		*data = sramReadByte(addr++);
		data++;
	}
}
