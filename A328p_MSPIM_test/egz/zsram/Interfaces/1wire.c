/***************************************************************************
 *   Copyright (C) 2008 by Damian Kmiecik                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *                             1WIRE FILE                                  *
 *                                                                         *
 ***************************************************************************/
//Headers
#include <avr/io.h>			//I/O
#include <inttypes.h>			//INTTYPES
#include <util/delay.h>			//DELAYS
#include <avr/interrupt.h>		//INT
#include "1wire.h"			//1wire
//Definitions
#define OW_LOW	OW_DDR |= _BV(OW_PIN)
#define OW_HI	OW_DDR &= ~_BV(OW_PIN)
#define OW	OW_PINP & _BV(OW_PIN)
//Init 1wire
void ow_init(void) {
	OW_HI;
}
//Reset 1wire
uint8_t ow_reset(void) {
	OW_LOW;
	_delay_us(480);
	OW_HI;
	_delay_us(50);
	if(OW)
		return 1;
	_delay_us(480);
	return 0;
}
//Write byte
void ow_write(uint8_t val) {
	for(uint8_t i = 0; i < 8; i++) {
		cli();
		OW_LOW;
		_delay_us(2);
		if(val & 0x01)
			OW_HI;
		_delay_us(75);
		OW_HI;
		sei();
		val >>= 1;
		_delay_us(2);
	}
	_delay_us(10);
}
//Read byte
uint8_t ow_read(void) {
	uint8_t val = 0;
	for (uint8_t i = 0; i < 8; i++) {
		cli();
		OW_LOW;
		_delay_us(2);
		OW_HI;
		_delay_us(15);
		if(OW)
			val |= 0x01 << i;
		_delay_us(60);
		sei();
	}
	return val;
}
