/***************************************************************************
 *   Copyright (C) 2008 by Damian Kmiecik                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *                          1WIRE HEADER                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _1WIRE_H_
#define _1WIRE_H_
//Headers
#include <inttypes.h>			//INTTYPES
//Definitions
#define OW_PIN		PD2
#define OW_PORT		PORTD
#define OW_DDR		DDRD
#define OW_PINP		PIND
//Declarations
void ow_init(void);
uint8_t ow_reset(void);
void ow_write(uint8_t val);
uint8_t ow_read(void);
 //
#endif
 
