/***************************************************************************
 *   Copyright (C) 2009 by Damian Kmiecik                                  *
 *   d0zoenator@gmail.com						   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

/**
 * External RAM latch addressed
 *
 * @author 	Damian Kmiecik <d0zoenator@gmail.com>
 * @processor 	AVR
 * @package 	sram
 * @compiler 	avr-gcc
 */

#ifndef _SRAM_H
#define	_SRAM_H

#ifdef	__cplusplus
extern "C" {
#endif

/**
 * Includes
 */
#include <inttypes.h>
#include <avr/io.h>

/**
 * I/O
 */
#define SRAM_DATA_PORT		PORTA
#define SRAM_DATA_PINx		PINA
#define SRAM_DATA_DRR		DDRA
#define SRAM_WE_PIN			PD7
#define SRAM_LE_PIN			PD6
#define SRAM_STR_PORT		PORTD
#define SRAM_STR_DDR		DDRD
#define SRAM_ADDR_PORT		PORTC
#define SRAM_ADDR_DDR		DDRC

/**
 * Definintions
 */
#define SRAM_SIZE		0x7FFF

/**
 * Initialize
 */
extern void sramInit(void);

/**
 * Write byte
 * @param uint16_t addr
 */
extern void sramWriteByte(const uint16_t addr, const uint8_t data);

/**
 * Read byte
 * @param uint16_t addr
 */
extern uint8_t sramReadByte(const uint16_t addr);

/**
 * Read
 */
extern void sramWrite(uint16_t addr, uint16_t len, const uint8_t *data);

/**
 * Read
 */
extern void sramRead(uint16_t addr, uint16_t len, uint8_t *data);

#ifdef	__cplusplus
}
#endif

#endif	/* _SRAM_H */
