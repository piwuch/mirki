/*
 * uart.h
 *
 *  Created on: 2009-12-27
 *      Author: damian
 */

#ifndef UART_H_
#define UART_H_

#include <inttypes.h>

#define UART_BAUDRATE	115200L
//#define UART_RECEIVE
//#define UART_SEND_STR
//#define UART_PGM

#if defined (__AVR_ATmega16__)
#define UCRA	UCSRA
#define UCRB	UCSRB
#define UCRC	UCSRC
#endif
#if defined (__AVR_ATmega32__)
#define UCRA	UCSRA
#define UCRB	UCSRB
#define UCRC	UCSRC
#endif

extern void uart_init(void);
extern void uart_putc(const uint8_t val);
#ifdef UART_SEND_STR
extern void uart_puts(const char*);
#ifdef UART_PGM
extern void uart_puts_p(const char* text);
#endif
#endif
#ifdef UART_RECEIVE
extern uint8_t uart_getc(void);
#endif

#endif /* UART_H_ */
