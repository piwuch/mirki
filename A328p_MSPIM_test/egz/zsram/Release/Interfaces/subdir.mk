################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Interfaces/1wire.c \
../Interfaces/rs232.c \
../Interfaces/spi.c \
../Interfaces/sram.c 

S_UPPER_SRCS += \
../Interfaces/i2cmaster.S 

OBJS += \
./Interfaces/1wire.o \
./Interfaces/i2cmaster.o \
./Interfaces/rs232.o \
./Interfaces/spi.o \
./Interfaces/sram.o 

C_DEPS += \
./Interfaces/1wire.d \
./Interfaces/rs232.d \
./Interfaces/spi.d \
./Interfaces/sram.d 

S_UPPER_DEPS += \
./Interfaces/i2cmaster.d 


# Each subdirectory must supply rules for building sources it contributes
Interfaces/%.o: ../Interfaces/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -DDEBUG -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega32 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

Interfaces/%.o: ../Interfaces/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Assembler'
	avr-gcc -x assembler-with-cpp -gstabs -mmcu=atmega32 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


