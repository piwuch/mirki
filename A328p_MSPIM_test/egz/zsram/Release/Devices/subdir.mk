################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Devices/clock.c \
../Devices/ds18b20.c \
../Devices/eeprom.c \
../Devices/netdevice.c \
../Devices/partition.c \
../Devices/stdout.c \
../Devices/timer.c 

OBJS += \
./Devices/clock.o \
./Devices/ds18b20.o \
./Devices/eeprom.o \
./Devices/netdevice.o \
./Devices/partition.o \
./Devices/stdout.o \
./Devices/timer.o 

C_DEPS += \
./Devices/clock.d \
./Devices/ds18b20.d \
./Devices/eeprom.d \
./Devices/netdevice.d \
./Devices/partition.d \
./Devices/stdout.d \
./Devices/timer.d 


# Each subdirectory must supply rules for building sources it contributes
Devices/%.o: ../Devices/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -DDEBUG -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega32 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


