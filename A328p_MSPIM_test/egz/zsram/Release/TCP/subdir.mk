################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../TCP/arp_icmp_ip.c \
../TCP/ethernet.c \
../TCP/tcp_udp.c 

OBJS += \
./TCP/arp_icmp_ip.o \
./TCP/ethernet.o \
./TCP/tcp_udp.o 

C_DEPS += \
./TCP/arp_icmp_ip.d \
./TCP/ethernet.d \
./TCP/tcp_udp.d 


# Each subdirectory must supply rules for building sources it contributes
TCP/%.o: ../TCP/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -DDEBUG -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega32 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


