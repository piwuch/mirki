################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Lib/base64.c \
../Lib/byteordering.c \
../Lib/checksum.c \
../Lib/crc8.c 

OBJS += \
./Lib/base64.o \
./Lib/byteordering.o \
./Lib/checksum.o \
./Lib/crc8.o 

C_DEPS += \
./Lib/base64.d \
./Lib/byteordering.d \
./Lib/checksum.d \
./Lib/crc8.d 


# Each subdirectory must supply rules for building sources it contributes
Lib/%.o: ../Lib/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -DDEBUG -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega32 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


