/*
 * httpd.c
 *
 *  Created on: 2010-03-03
 *      Author: damian
 */

#include <avr/io.h>
#include <string.h>
#include <stdlib.h>
#include <inttypes.h>
#include "../Interfaces/sram.h"
#include "../Devices/stdout.h"
#include "../Devices/timer.h"
#include "../TCP/ethernet.h"
#include "../FAT/fat.h"
#include "../Devices/partition.h"
#include "../Drivers/sd_raw.h"
#include "../Configuration/conf.h"
#include "../Interfaces/rs232.h"
#include "../Lib/base64.h"
#include "../Devices/ds18b20.h"
#include "httpd.h"

HTTP_TABLE httpTableItem;
struct fat_file_struct* fd;

/**
 * Headers
 */
#define HEADER_MAIN_LEN			sizeof(headerMain)-1
prog_uint8_t headerMain[] = { "HTTP/0.9 200 OK\r\n"
	"Server: Damian Kmiecik's AVR WebServer\r\n"
	"Content-Length: " };
#define HEADER_NOT_FOUND_LEN	sizeof(headerNotFound)-1
prog_uint8_t headerNotFound[] = { "HTTP/0.9 404 Not Found\r\n"
	"Server: Damian Kmiecik's AVR WebServer\r\n"
	"Content-Type: text/html\r\n"
	"Content-Length: 18\r\n\r\n"
	"<h1>NOT FOUND</h1>" };
#define HEADER_AUTH_NEED_LEN	sizeof(headerNeedAuth)-1
prog_uint8_t headerNeedAuth[] = { "HTTP/0.9 401 Authorization Required\r\n"
	"Server: Damian Kmiecik's AVR WebServer\r\n"
	"WWW-Authenticate: Basic realm=\"Secure Area\"\r\n"
	"Content-Length: 0\r\n\r\n" };
#define HEADER_CONTENT_LEN		sizeof(headerContent)-1
prog_uint8_t headerContent[] = { "\r\nContent-Type: " };
#define HEADER_MIME_HTML_LEN	sizeof(headerMimeHtml)-1
prog_uint8_t headerMimeHtml[] = { "text/html\r\n\r\n" };
#define HEADER_MIME_CSS_LEN		sizeof(headerMimeCss)-1
prog_uint8_t headerMimeCss[] = { "text/css\r\n\r\n" };
#define HEADER_MIME_JPG_LEN		sizeof(headerMimeJpg)-1
prog_uint8_t headerMimeJpg[] = { "image/jpeg\r\n\r\n" };
#define HEADER_MIME_GIF_LEN		sizeof(headerMimeGif)-1
prog_uint8_t headerMimeGif[] = { "image/gif\r\n\r\n" };
#define HEADER_MIME_PLAIN_LEN	sizeof(headerMimePlain)-1
prog_uint8_t headerMimePlain[] = { "text/plain\r\n\r\n" };

/**
 * Initialize
 */
void httpdInit(const uint16_t port) {
	// Add HTTPD app to port
	TCPappAdd(port, httpd);
}

/**
 * Write Header not authenticated
 */
void needAuthHeaderWrite(void) {
	memcpy_P(&ethBuffer[TCP_DATA], headerNeedAuth, HEADER_AUTH_NEED_LEN);
	tcpTableItem.dataLen = HEADER_AUTH_NEED_LEN;
}

/**
 * Write NOT FOUND not authenticated
 */
void notFoundHeaderWrite(void) {
	memcpy_P(&ethBuffer[TCP_DATA], headerNotFound, HEADER_NOT_FOUND_LEN);
	tcpTableItem.dataLen = HEADER_NOT_FOUND_LEN;
}

/**
 * Write ok header
 */
void okHeaderWrite(void) {
	memcpy_P(&ethBuffer[TCP_DATA], headerMain, HEADER_MAIN_LEN);
	tcpTableItem.dataLen = HEADER_MAIN_LEN;
}

/**
 * Get requested filename
 */
void getFilename(uint8_t *cmdPtr) {
	uint8_t i = 0;
	while (*cmdPtr != ' ') {
		httpTableItem.fileName[i++] = *cmdPtr++;
	}
	httpTableItem.fileName[i] = '\0';
	// Check if root
	if (!strlen(&httpTableItem.fileName[0]))
		strcpy_P(&httpTableItem.fileName[0], PSTR("index.html"));
}

/**
 * Check auth
 */
void checkAuth(const uint8_t tcpIndex) {
	// Chek auth
	uint8_t *authPtr =
			strcasestr_P(&ethBuffer[TCP_DATA], PSTR("Authorization"));
	if (authPtr) {
		// Check pass
		//authPtr += sizeof("Authorization: Basic ");
		// TODO: CHECK PASSWORD
		if (1) {
#ifdef DEBUG
			printf_P(PSTR("HTTPD %u AUTH\n"), tcpIndex);
#endif
			httpTableItem.auth = 1;
		}
	}
}

/**
 * Get post Var
 */
void getPostVars(const uint8_t tcpIndex) {
	//PB0
	uint8_t *posPtr = strstr_P(&ethBuffer[TCP_DATA], PSTR("buttonPB0"));
	if (!posPtr) {
		PORTB |= _BV(PB0);
		DDRB |= _BV(PB0);
#ifdef DEBUG
		printf_P(PSTR("HTTPD %u POST PB0=1\n"), tcpIndex);
#endif
	} else {
		PORTB &= ~_BV(PB0);
		DDRB |= _BV(PB0);
#ifdef DEBUG
		printf_P(PSTR("HTTPD %u POST PB0=0\n"), tcpIndex);
#endif
	}
	//PB1
	posPtr = strstr_P(&ethBuffer[TCP_DATA], PSTR("buttonPB1"));
	if (posPtr) {
		PORTB |= _BV(PB1);
		DDRB |= _BV(PB1);
#ifdef DEBUG
		printf_P(PSTR("HTTPD %u POST PB1=1\n"), tcpIndex);
#endif
	} else {
		PORTB &= ~_BV(PB1);
		DDRB |= _BV(PB1);
#ifdef DEBUG
		printf_P(PSTR("HTTPD %u POST PB1=0\n"), tcpIndex);
#endif
	}
}

/**
 * Replace document vars
 */
void documentVars(const uint8_t tcpIndex) {
	uint8_t buffer[4];
	// Temperature
	uint8_t *posPtr = strcasestr_P(&ethBuffer[TCP_DATA], PSTR("{{TEMP}}"));
	if (posPtr) {
#ifdef DEBUG
		printf_P(PSTR("HTTPD %u VAR TEMP\n"), tcpIndex);
#endif
		itoa(temperature, &buffer[0], 10);
		memcpy(posPtr, buffer, strlen(buffer));
		memcpy_P(posPtr + strlen(buffer), PSTR("        "), 8 - strlen(buffer));
	}
	// PortD
	posPtr = strcasestr_P(&ethBuffer[TCP_DATA], PSTR("{{PRTD}}"));
	if (posPtr) {
#ifdef DEBUG
		printf_P(PSTR("HTTPD %u VAR PORTD\n"), tcpIndex);
#endif
		utoa(PORTD, &buffer[0], 10);
		memcpy(posPtr, buffer, strlen(buffer));
		memcpy_P(posPtr + strlen(buffer), PSTR("        "), 8 - strlen(buffer));
	}
	// PortB
	posPtr = strcasestr_P(&ethBuffer[TCP_DATA], PSTR("{{PRTB}}"));
	if (posPtr) {
#ifdef DEBUG
		printf_P(PSTR("HTTPD %u VAR PORTB\n"), tcpIndex);
#endif
		utoa(PORTB, &buffer[0], 10);
		memcpy(posPtr, buffer, strlen(buffer));
		memcpy_P(posPtr + strlen(buffer), PSTR("        "), 8 - strlen(buffer));
	}
	// PinD
	posPtr = strcasestr_P(&ethBuffer[TCP_DATA], PSTR("{{PIND}}"));
	if (posPtr) {
#ifdef DEBUG
		printf_P(PSTR("HTTPD %u VAR PIND\n"), tcpIndex);
#endif
		DDRD &= ~_BV(PD4);
		DDRD &= ~_BV(PD5);
		PORTD |= _BV(PD4) | _BV(PD5);
		utoa(PIND, &buffer[0], 10);
		memcpy(posPtr, buffer, strlen(buffer));
		memcpy_P(posPtr + strlen(buffer), PSTR("        "), 8 - strlen(buffer));
	}
	// DdrD
	posPtr = strcasestr_P(&ethBuffer[TCP_DATA], PSTR("{{DDRD}}"));
	if (posPtr) {
#ifdef DEBUG
		printf_P(PSTR("HTTPD %u VAR DDRD\n"), tcpIndex);
#endif
		utoa(DDRD, &buffer[0], 10);
		memcpy(posPtr, buffer, strlen(buffer));
		memcpy_P(posPtr + strlen(buffer), PSTR("        "), 8 - strlen(buffer));
	}
}

/**
 * Main HTTP routine
 */
void httpd(uint8_t tcpIndex) {
	// Read HTTP entry form SRAM
	sramRead(HTTP_TABLE_START_POINTER + HTTP_TABLE_ITEM_LEN * tcpIndex,
			HTTP_TABLE_ITEM_LEN, (uint8_t*) &httpTableItem);
	// App reset
	if (tcpTableItem.app_status == 1) {
		httpTableItem.status = HTTP_EMPTY_STATE;
		httpTableItem.sent = 0;
		httpTableItem.size = 0;
		httpTableItem.auth = 0;
		httpTableItem.vars = 0;
		httpTableItem.post = 0;
	}
	// change directory to WWW
	if (cardPresence) {
		struct fat_dir_entry_struct subdir_entry;
		fat_close_dir(dd);
		fat_get_dir_entry_of_path(fs, "/WWW", &subdir_entry);
		dd = fat_open_dir(fs, &subdir_entry);
	}
#ifdef DEBUG
	printf_P(PSTR("HTTPD %u START\n"), tcpIndex);
#endif
	// Check state
	if (httpTableItem.status == HTTP_EMPTY_STATE) {
		// Check if something comming
		if (!tcpTableItem.dataLen) {
#ifdef DEBUG
			printf_P(PSTR("HTTPD %u EMPTY\n"), tcpIndex);
#endif
			goto httpdEnd;
		}
		// Check is POST OR GET
		uint8_t *cmdPtr = strcasestr_P(&ethBuffer[TCP_DATA], PSTR("POST"));
		if (cmdPtr) {
			// POST
#ifdef DEBUG
			printf_P(PSTR("HTTPD %u POST\n"), tcpIndex);
#endif
			// Get post vars & process
			httpTableItem.post = 1;
			cmdPtr += sizeof("POST ");
		} else {
			// GET
			cmdPtr = strcasestr_P(&ethBuffer[TCP_DATA], PSTR("GET"));
#ifdef DEBUG
			printf_P(PSTR("HTTPD %u GET\n"), tcpIndex);
#endif
			httpTableItem.post = 0;
			cmdPtr += sizeof("GET ");
		}
		if (!cmdPtr) {
#ifdef DEBUG
			printf_P(PSTR("HTTPD %u INVALID\n"), tcpIndex);
#endif
			goto httpdEnd;
		}
		// Get filename
		getFilename(cmdPtr);
		httpTableItem.auth = 0;
		checkAuth(tcpIndex);
		httpTableItem.status = HTTP_HEADER_STATE;
	} else if (httpTableItem.status == HTTP_HEADER_STATE) {
		// Check if something comming
		if (!tcpTableItem.dataLen) {
#ifdef DEBUG
			printf_P(PSTR("HTTPD %u EMPTY\n"), tcpIndex);
#endif
			goto httpdEnd;
		}
		checkAuth(tcpIndex);
		// Check is ready
		if ((!strcasestr_P(&ethBuffer[TCP_DATA], PSTR("\r\n\r\n")))
				&& httpTableItem.post != 2) {
#ifdef DEBUG
			printf_P(PSTR("HTTPD %u WAIT\n"), tcpIndex);
#endif
			goto httpdEnd;
		} else if ((httpTableItem.post == 1) && (strcmp_P(&ethBuffer[TCP_DATA
				+ tcpTableItem.dataLen - 4], PSTR("\r\n\r\n")) == 0)) {
#ifdef DEBUG
			printf_P(PSTR("HTTPD %u POST WAIT\n"), tcpIndex);
#endif
			httpTableItem.post = 2;
			goto httpdEnd;
		} else if ((httpTableItem.post == 1) && (strcmp_P(&ethBuffer[TCP_DATA
				+ tcpTableItem.dataLen - 4], PSTR("\r\n\r\n")) != 0)) {
			httpTableItem.post = 2;
		}
		if (httpTableItem.post == 2) {
			httpTableItem.post = 0;
			getPostVars(tcpIndex);
		}
		// Check authentication
		if (httpTableItem.auth) {
#ifdef DEBUG
			printf_P(PSTR("HTTPD %u OPEN %s\n"), tcpIndex,
					&httpTableItem.fileName[0]);
#endif
			// Open and get size
			if (cardPresence)
				fd = open_file_in_dir(fs, dd, &httpTableItem.fileName[0]);
			if (!fd) {
#ifdef DEBUG
				printf_P(PSTR("HTTPD %u NOT FOUND\n"), tcpIndex);
#endif
				// NOT FOUND
				notFoundHeaderWrite();
				TCPdataSend(tcpIndex);
			} else {
#ifdef DEBUG
				printf_P(PSTR("HTTPD %u FOUND\n"), tcpIndex);
#endif
				httpTableItem.size = fd->dir_entry.file_size;
				// Write header
				okHeaderWrite();
				// Write size
				uint8_t buffer[12];
				ultoa(httpTableItem.size, &buffer[0], 10);
				memcpy(&ethBuffer[TCP_DATA + tcpTableItem.dataLen], &buffer[0],
						strlen(buffer));
				tcpTableItem.dataLen += strlen(buffer);
				// Write mime
				memcpy_P(&ethBuffer[TCP_DATA + tcpTableItem.dataLen],
						headerContent, HEADER_CONTENT_LEN);
				tcpTableItem.dataLen += HEADER_CONTENT_LEN;
				uint8_t a = strcspn_P(&httpTableItem.fileName[0], PSTR("."));
				a++;
				// Check mime
				httpTableItem.vars = 0;
				if ((strcmp_P(&httpTableItem.fileName[a], PSTR("html")) == 0)
						|| (strcmp_P(&httpTableItem.fileName[a], PSTR("htm"))
								== 0)) {
					memcpy_P(&ethBuffer[TCP_DATA + tcpTableItem.dataLen],
							headerMimeHtml, HEADER_MIME_HTML_LEN);
					tcpTableItem.dataLen += HEADER_MIME_HTML_LEN;
					httpTableItem.vars = 1;
				} else if (strcmp_P(&httpTableItem.fileName[a], PSTR("css"))
						== 0) {
					memcpy_P(&ethBuffer[TCP_DATA + tcpTableItem.dataLen],
							headerMimeCss, HEADER_MIME_CSS_LEN);
					tcpTableItem.dataLen += HEADER_MIME_CSS_LEN;
				} else if ((strcmp_P(&httpTableItem.fileName[a], PSTR("jpg"))
						== 0) || (strcmp_P(
				&httpTableItem.fileName[a],
						PSTR("jpeg")) == 0)) {
					memcpy_P(&ethBuffer[TCP_DATA + tcpTableItem.dataLen],
							headerMimeJpg, HEADER_MIME_JPG_LEN);
					tcpTableItem.dataLen += HEADER_MIME_JPG_LEN;
				} else if (strcmp_P(&httpTableItem.fileName[a], PSTR("gif"))
						== 0) {
					memcpy_P(&ethBuffer[TCP_DATA + tcpTableItem.dataLen],
							headerMimeGif, HEADER_MIME_GIF_LEN);
					tcpTableItem.dataLen += HEADER_MIME_GIF_LEN;
				} else {
					memcpy_P(&ethBuffer[TCP_DATA + tcpTableItem.dataLen],
							headerMimePlain, HEADER_MIME_PLAIN_LEN);
					tcpTableItem.dataLen += HEADER_MIME_PLAIN_LEN;
				}
				TCPdataSend(tcpIndex);
				httpTableItem.status = HTTP_UPLOAD_STATE;
			}
		} else {
			// Auth needed
			needAuthHeaderWrite();
			TCPdataSend(tcpIndex);
		}
	} else if (httpTableItem.status == HTTP_UPLOAD_STATE) {
		// UPLOADING
		uint16_t s;
		if ((httpTableItem.size - httpTableItem.sent) > MAX_TCP_WINDOW) {
			s = MAX_TCP_WINDOW;
		} else {
			s = httpTableItem.size - httpTableItem.sent;
		}
		fd = open_file_in_dir(fs, dd, &httpTableItem.fileName[0]);
		if (!fd) {
			httpTableItem.sent = httpTableItem.size;
#ifdef DEBUG
			printf_P(PSTR("HTTPD %u ERROR SENDING\n"), tcpIndex);
#endif
		}
		if (httpTableItem.sent == httpTableItem.size) {
			httpTableItem.status = HTTP_EMPTY_STATE;
			httpTableItem.sent = 0;
			httpTableItem.size = 0;
			httpTableItem.auth = 0;
			goto httpdEnd;
		}
		fat_seek_file(fd, &httpTableItem.sent, FAT_SEEK_SET);
		// Write content
		uint8_t buffer[10];
		uint16_t read = 0;
		uint8_t a;
		do {
			a = fat_read_file(fd, buffer, sizeof(buffer));
			for (uint8_t i = 0; (read <= s) && (i < a); read++, i++) {
				ethBuffer[TCP_DATA + read] = buffer[i];
			}
		} while (a && (read <= s));
		httpTableItem.sent += read;
		// Parase VARS
		if (httpTableItem.vars)
			documentVars(tcpIndex);
		// Send data
		tcpTableItem.dataLen = read;
		TCPdataSend(tcpIndex);
	}
	httpdEnd:
	// Close filesystem heandles
	if (cardPresence) {
		fat_close_dir(dd);
		fat_close_file(fd);
	}
	// Write HTTP entry to SRAM
	sramWrite(HTTP_TABLE_START_POINTER + HTTP_TABLE_ITEM_LEN * tcpIndex,
			HTTP_TABLE_ITEM_LEN, (uint8_t*) &httpTableItem);
}
