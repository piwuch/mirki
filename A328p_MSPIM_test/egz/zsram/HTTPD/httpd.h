/*
 * httpd.h
 *
 *  Created on: 2010-03-03
 *      Author: damian
 */

#ifndef HTTPD_H_
#define HTTPD_H_

/**
 * HTTP STATUS
 */
#define HTTP_EMPTY_STATE	0x00
#define HTTP_UPLOAD_STATE	0x01
#define HTTP_HEADER_STATE	0x02

/**
 * HTTP SRAM table
 */
#define HTTP_TABLE_ITEM_LEN			sizeof(HTTP_TABLE)
#define HTTP_TABLE_START_POINTER 	ARP_TABLE_END_POINTER+1
#define HTTP_TABLE_END_POINTER		HTTP_TABLE_START_POINTER+(HTTP_TABLE_ITEM_LEN*(MAX_TCP_ENTRY+1))

typedef struct
{
	uint8_t fileName[31];
	uint32_t size;
	uint32_t sent;
	uint8_t status;
	uint8_t auth;
	uint8_t vars;
	uint8_t post;
} HTTP_TABLE;

/**
 * Initialize
 */
extern void httpdInit(const uint16_t port);

/**
 * Main HTTP routine
 */
extern void httpd(uint8_t tcpIndex);

#endif /* HTTPD_H_ */
