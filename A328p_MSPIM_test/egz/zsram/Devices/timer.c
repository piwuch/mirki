/*
 * timer.c
 *
 *  Created on: 2010-01-30
 *      Author: damian
 */

#include "timer.h"
#include <avr/io.h>

struct timerItem {
	void (*func)(void);
	uint16_t time : 15;
	uint8_t active : 1;
	uint16_t cnt;
};

struct timerItem timerTable[TIMER_TABLE_SIZE];

void timerInit(void)
{
	TCCR2 = _BV(CS22) | _BV(CS21);
	TIMSK |= _BV(TOIE2);
}

void timerAdd(void (*func)(void), uint16_t time)
{
	register uint8_t i asm("r3");
	for (i = 0; i < TIMER_TABLE_SIZE; i++) {
		if (timerTable[i].active == 0) {
			timerTable[i].active = 1;
			timerTable[i].time = time;
			timerTable[i].cnt = 0;
			timerTable[i].func = func;
			break;
		}
	}
}

ISR(TIMER2_OVF_vect)
{
	TCNT2 = 220;
	register uint8_t i asm("r3");
	for (i = 0; i < TIMER_TABLE_SIZE; i++) {
		if (timerTable[i].active == 1) {
			timerTable[i].cnt++;
			if (timerTable[i].cnt == timerTable[i].time) {
				timerTable[i].cnt = 0;
				timerTable[i].func();
			}
		} else {
			continue;
		}
	}
}
