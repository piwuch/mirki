/*
 * stdout.c
 *
 *  Created on: 2010-01-29
 *      Author: Damian Kmiecik <d0zoenator@gmail.com>
 */

#include "../Interfaces/rs232.h"
#include "stdout.h"

static int uart_putchar(char, FILE*);

static FILE mystdout = FDEV_SETUP_STREAM(uart_putchar, NULL, _FDEV_SETUP_WRITE);

void stdoutInit(void)
{
	uart_init();
	stdout = &mystdout;
}

static int uart_putchar(char c, FILE *stream)
{
//	if (c == '\n')
//		uart_putchar('\r', stream);
	uart_putc(c);
	return 0;
}
