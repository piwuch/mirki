/*
 * stdout.h
 *
 *  Created on: 2010-01-29
 *      Author: Damian Kmiecik <d0zoenator@gmail.com>
 */

#ifndef STDOUT_H_
#define STDOUT_H_

#include <stdio.h>
#include <avr/pgmspace.h>

extern void stdoutInit(void);

#endif /* STDOUT_H_ */
