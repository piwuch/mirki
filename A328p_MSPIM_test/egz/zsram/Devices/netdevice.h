/*
 * netdevice.h
 *
 *  Created on: 2010-01-29
 *      Author: Damian Kmiecik <d0zoenator@gmail.com>
 */

#ifndef NETDEVICE_H_
#define NETDEVICE_H_

#include <inttypes.h>
#include "../Drivers/enc28j60.h"

#define netdeviceInit enc28j60Init
#define netdeviceReceive enc28j60ReceivePacket
#define netdeviceSend enc28j60SendPacket
#define netdeviceReset enc28j60Reset

#endif /* NETDEVICE_H_ */
