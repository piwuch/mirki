/***************************************************************************
 *   Copyright (C) 2008 by Damian Kmiecik                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *                               DS FILE                                   *
 *                                                                         *
 ***************************************************************************/
#ifndef _DS18B20_H_
#define _DS18B20_H_
//Headers
#include <inttypes.h>			//INTTYPES
//Definitions
#define DS18B20_SKIP_ROM	0xCC
#define DS18B20_CONVERT		0x44
#define DS18B20_WRITE_SCR	0x4E
#define DS18B20_READ_SCR	0xBE
#define DS18B20_COPY_SCR	0x48
//Declarations
void ds18b20_init(void);
int8_t ds18b20_read_temp(void);
extern int8_t temperature;
 //
#endif
 
