/***************************************************************************
 *   Copyright (C) 2008 by Damian Kmiecik                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/
/***************************************************************************
 *                                                                         *
 *                               DS FILE                                   *
 *                                                                         *
 ***************************************************************************/
//Headers
#include <inttypes.h>			//INTTYPES
#include "../Interfaces/1wire.h"			//1WIRE
#include <util/delay.h>			//DELAY
#include "ds18b20.h"			//DS18B20
#include "../Lib/crc8.h"			//CRC8
//Definitions
int8_t temperature = 0;
//Init DS18B20
void ds18b20_init(void) {
	ow_init();
	//setting 9bits resolution
	ow_reset();
	ow_write(DS18B20_SKIP_ROM);
	ow_write(DS18B20_WRITE_SCR);
	ow_write(0);			//Th
	ow_write(0);			//Tl
	ow_write(0b00011111);		//Conf
	ow_reset();
}
//Read temperature
int8_t ds18b20_read_temp(void) {
	ow_reset();
	ow_write(DS18B20_SKIP_ROM);
	ow_write(DS18B20_CONVERT);
	_delay_ms(95);
	ow_reset();
	ow_write(DS18B20_SKIP_ROM);
	ow_write(DS18B20_READ_SCR);
	uint8_t data[9];
	data[0] = ow_read();		//LSB T
	data[1] = ow_read();		//MSB T
	data[2] = ow_read();		//Th
	data[3] = ow_read();		//Tl
	data[4] = ow_read();		//Conf
	data[5] = ow_read();		//Reserved byte
	data[6] = ow_read();		//Reserved byte
	data[7] = ow_read();		//Reserved byte
	data[8] = ow_read();		//CRC
	if(data[8] == crc8(data, 8)) {	//Check CRC
		data[1] <<= 4;			//8bit resolution
		data[0] >>= 4;			//8bit resolution
		data[0] = data[0] | data[1];	//
		data[0] &= 0b01111111;		//fix positive/negative bits
		return data[0];
	}
	return -1;
}
