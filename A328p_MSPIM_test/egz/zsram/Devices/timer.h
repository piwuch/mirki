/*
 * timer.h
 *
 *  Created on: 2010-01-30
 *      Author: damian
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <inttypes.h>
#include <avr/interrupt.h>

#define TIMER_TABLE_SIZE	4

extern void timerInit(void);
extern void timerAdd(void (*func)(void), uint16_t);

#endif /* TIMER_H_ */
