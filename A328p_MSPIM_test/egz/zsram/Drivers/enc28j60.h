/***************************************************************************
 *   Copyright (C) 2009 by Damian Kmiecik                                  *
 *   d0zoenator@gmail.com						   						   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

/**
 * Driver for SPI Ethernet card enc28j60 form Microchip
 *
 * @author 	Damian Kmiecik
 * @processor 	AVR
 * @package 	enc28j60
 * @compiler 	avr-gcc
 */

#ifndef _ENC28J60_H
#define	_ENC28J60_H

#ifdef	__cplusplus
extern "C"
{
#endif

#include <inttypes.h>
#include "enc28j60reg.h"

#define ENC28J60_DDR		DDRB
#define ENC28J60_PORT		PORTB
#define ENC28J60_PINx		PINB
#define ENC28J60_CS			PB4
#define ENC28J60_INT		PB2

#define NETDEVICE_INTERRUPT	bit_is_clear(ENC28J60_PINx, ENC28J60_INT)

// Max frame length which the conroller will accept:
#define ENC28J60_MAX_LEN	1500
// Transmit buffer start
#define ENC28J60_TXST		(0x1FFF-ENC28J60_MAX_LEN-1)
// Transmit buffer end
#define ENC28J60_TXND		0x1FFF
// Receive buffer start
#define ENC28J60_RXST		0x0000
// Receive buffer end
#define ENC28J60_RXND		(ENC28J60_TXST-1)

/**
 * Read Control Register Command
 * @param uint8_t address
 * @return uint8_t
 */
extern uint8_t enc28j60ReadCR(const uint8_t address);

/**
 * Write Control Register Command
 * @param uint8_t address
 * @param uint8_t data
 */
extern void enc28j60WriteCR(const uint8_t address, const uint8_t data);

/**
 * Read PHY Register Command
 * @param uint8_t address
 * @return uint16_t
 */
extern uint16_t enc28j60ReadPHY(const uint8_t address);

/**
 * Write PHY Register Command
 * @param uint8_t address
 * @param uint8_t data
 */
extern void enc28j60WritePHY(const uint8_t address, const uint16_t data);

/**
 * Set bits in register
 * @param uint8_t address
 * @param uint8_t mask
 */
extern void enc28j60SetBF(const uint8_t address, const uint8_t mask);

/**
 * Clear bits in register
 * @param uint8_t address
 * @param uint8_t mask
 */
extern void enc28j60ClearBF(const uint8_t address, const uint8_t mask);

/**
 * Bank Select
 * Check if destination address is in current bank
 * @param uint8_t address
 */
extern void enc28j60BankSel(const uint8_t address);

/**
 * ENC28J60 initialize
 * Must be run before any usage of ENC28J60 functions
 * @param uint8_t* macAddr
 */
extern void enc28j60Init(const uint8_t* macAddr);

/**
 * Read Buffer Memmory Command
 * @param uint16_t len
 * @param uint8_t* data
 */
extern void enc28j60ReadBM(uint16_t len, uint8_t* data);

/**
 * Write Buffer Memmory Command
 * @param uint16_t len
 * @param uint8_t* data
 */
extern void enc28j60WriteBM(uint16_t len, const uint8_t* data);

/**
 * Send Packet
 * @param uint16_t len
 * @param uint8_t* packet
 */
extern void enc28j60SendPacket(uint16_t len, const uint8_t* packet);

/**
 * Receive Packet
 * @param uint16_t maxlen
 * @param uint8_t* packet
 * @return uint16_t
 */
extern uint16_t enc28j60ReceivePacket(const uint16_t maxlen, uint8_t* packet);

extern void enc28j60Reset(void);

#ifdef	__cplusplus
}
#endif

#endif	/* _ENC28J60_H */
