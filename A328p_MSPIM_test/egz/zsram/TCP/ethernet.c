/***************************************************************************
 *   Copyright (C) 2009 by Damian Kmiecik                                  *
 *   d0zoenator@gmail.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include "../Devices/netdevice.h"
#include "../Devices/timer.h"
#include "../Devices/stdout.h"
#include "../Interfaces/sram.h"
#include "../Configuration/conf.h"
#include "ethernet.h"

uint8_t ethBuffer[MTU_SIZE + 1];
ARP_TABLE arpTable[MAX_ARP_ENTRY];

/**
 * Change byte direction
 */
uint32_t htons32(const uint32_t val)
{
	return HTONS32(val);
}

/**
 * Change byte direction
 */
uint16_t htons(const uint16_t val)
{
	return HTONS(val);
}

/**
 * Get data from ethernet card
 */
void ETHgetData(void)
{
	// If data present
	while (NETDEVICE_INTERRUPT)
	{
		uint16_t length = netdeviceReceive(MTU_SIZE, ethBuffer);
		if (length > 0) {
			cli();
			ETHpacketProcess();
			sei();
		}
	}
}

/**
 * Process ethernet packet
 */
void ETHpacketProcess(void)
{
    ETH_ETHERNET_HEADER* ethernet =
            (ETH_ETHERNET_HEADER*) & ethBuffer[ETHERNET_OFFSET];
    ETH_IP_HEADER* ip = (ETH_IP_HEADER*) & ethBuffer[IP_OFFSET];
    ETH_ICMP_HEADER* icmp = (ETH_ICMP_HEADER*) & ethBuffer[ICMP_OFFSET];
    // Check packet type
    if (ethernet->type == HTONS(ETH_TYPE_ARP))
    {
        ARPreply();
    }
    else
    {
        if (ethernet->type == HTONS(ETH_TYPE_IP))
        {
            if (ip->destIp == ipAddr)
            {
                ARPentryAdd();
                if (ip->protocol == IP_PROTOCOL_ICMP)
                {
                    switch (icmp->type)
                    {
                    case ICMP_REQUEST:
                        ICMPsend(ip->sourceIp, ICMP_REPLY, icmp->seqNum,
                                 icmp->id);
                        break;
                    case ICMP_REPLY:
                        if (ping.ip == ip->sourceIp)
                        {
                            ping.result = 1;
                        }
                        break;
                    }
                    return;
                }
                else
                {
                    if (ip->protocol == IP_PROTOCOL_TCP)
                    {
                        TCPpacketProcess();
                    }
                    if (ip->protocol == IP_PROTOCOL_UDP)
                    {
                        UDPpacketProcess();
                    }
                }
            }
            else
            {
                // If broadcast
                if (ip->destIp == (uint32_t) 0xFFFFFFFF)
                    if (ip->protocol == IP_PROTOCOL_UDP)
                    {
                        UDPpacketProcess();
                    }
            }
        }
    }
}

/**
 * Make ethernet header
 */
void ETHmakeHeader(uint32_t destIp)
{
    ETH_ETHERNET_HEADER* ethernet =
            (ETH_ETHERNET_HEADER*) & ethBuffer[ETHERNET_OFFSET];
    register uint8_t i asm("r3");
    // Search for MAC in ARP table
    ARP_TABLE arpTableItem;
    i = ARPentrySearch(destIp);
    if (i != MAX_ARP_ENTRY)
    {
        // Read ARP entry form SRAM
        sramRead(ARP_TABLE_START_POINTER + ARP_TABLE_ITEM_LEN * i,
                 ARP_TABLE_ITEM_LEN, (uint8_t*) & arpTableItem);
        for (i = 0; i < 6; i++)
        {
            ethernet->destMac[i] = arpTableItem.arp_t_mac[i];
            ethernet->sourceMac[i] = macAddr[i];
        }
        return;
    }
    for (i = 0; i < 6; i++)
    {
        ethernet->destMac[i] = 0xFF;
        ethernet->sourceMac[i] = macAddr[i];
    }
}

void ethernetInit(void)
{
	// Start timers
	timerAdd(ARPtimerDo, 2000);
	timerAdd(TCPtimerDo, 2000);
	//timerAdd(ARPprintTable, 10000);
	//timerAdd(TCPprintTable, 10000);
	// Write MAC to eth card
	netdeviceInit(macAddr);
}
