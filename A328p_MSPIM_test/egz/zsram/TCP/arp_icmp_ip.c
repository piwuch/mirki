/***************************************************************************
 *   Copyright (C) 2009 by Damian Kmiecik                                  *
 *   d0zoenator@gmail.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include "../Lib/checksum.h"
#include "../Devices/netdevice.h"
#include "../Devices/timer.h"
#include "../Devices/stdout.h"
#include "../Interfaces/sram.h"
#include "../Configuration/conf.h"
#include "ethernet.h"

PING_STRUCT ping;

#ifdef DEBUG
/**
 * Print ARP table
 */
void ARPprintTable(void)
{
	ARP_TABLE arpTableItem;
	register uint8_t i asm("r3");
	printf_P(PSTR("_ ARP TABLE _\n"));
	for (i = 0; i < MAX_ARP_ENTRY; i++)
	{
		// Read ARP entry form SRAM
		sramRead(ARP_TABLE_START_POINTER + ARP_TABLE_ITEM_LEN * i,
				ARP_TABLE_ITEM_LEN, (uint8_t*) &arpTableItem);
		printf_P(PSTR("%d.%d.%d.%d is %X:%X:%X:%X:%X:%X for %d\n"),
				((uint8_t*)&(arpTableItem.arp_t_ip))[0], ((uint8_t*)&(arpTableItem.arp_t_ip))[1],
				((uint8_t*)&(arpTableItem.arp_t_ip))[2], ((uint8_t*)&(arpTableItem.arp_t_ip))[3],
				arpTableItem.arp_t_mac[0], arpTableItem.arp_t_mac[1],
				arpTableItem.arp_t_mac[2], arpTableItem.arp_t_mac[3],
				arpTableItem.arp_t_mac[4], arpTableItem.arp_t_mac[5],
				arpTableItem.arp_t_time);
	}
	printf_P(PSTR("_ ARP TABLE END _\n"));
}
#endif

/**
 * Do ARP table refresh every 1 second
 */
void ARPtimerDo(void)
{
	// Refresh time
	ARP_TABLE arpTableItem;
	register uint8_t i asm("r3");
	for (i = 0; i < MAX_ARP_ENTRY; i++)
	{
		// Read ARP entry form SRAM
		sramRead(ARP_TABLE_START_POINTER + ARP_TABLE_ITEM_LEN * i,
				ARP_TABLE_ITEM_LEN, (uint8_t*) &arpTableItem);
		// Check time
		if (arpTableItem.arp_t_ip != 0 && arpTableItem.arp_t_time < 1)
			// Free
			arpTableItem.arp_t_ip = 0;
		else if (arpTableItem.arp_t_ip != 0)
			arpTableItem.arp_t_time--;
		// Write ARP entry to SRAM
		sramWrite(ARP_TABLE_START_POINTER + ARP_TABLE_ITEM_LEN * i,
				ARP_TABLE_ITEM_LEN, (uint8_t*) &arpTableItem);
	}
}

/**
 * ARP table process
 */
void ARPentryAdd(void)
{
	ETH_ETHERNET_HEADER* ethernet =
			(ETH_ETHERNET_HEADER*) &ethBuffer[ETHERNET_OFFSET];
	ETH_IP_HEADER* ip = (ETH_IP_HEADER*) &ethBuffer[IP_OFFSET];
	ETH_ARP_HEADER* arp = (ETH_ARP_HEADER*) &ethBuffer[ARP_OFFSET];
	// Check if MAC is in ARP table
	ARP_TABLE arpTableItem;
	register uint8_t i asm("r3");
	for (i = 0; i < MAX_ARP_ENTRY; i++)
	{
		// Read ARP entry form SRAM
		sramRead(ARP_TABLE_START_POINTER + ARP_TABLE_ITEM_LEN * i,
				ARP_TABLE_ITEM_LEN, (uint8_t*) &arpTableItem);
		if (ethernet->type == HTONS(ETH_TYPE_IP))
		{
			if (arpTableItem.arp_t_ip == ip->sourceIp)
			{
				arpTableItem.arp_t_time = MAX_ARP_ENTRY_TIME;
				sramWrite(ARP_TABLE_START_POINTER + ARP_TABLE_ITEM_LEN * i,
						ARP_TABLE_ITEM_LEN, (uint8_t*) &arpTableItem);
				return;
			}
		}
		if (ethernet->type == HTONS(ETH_TYPE_ARP))
		{
			if (arpTableItem.arp_t_ip == arp->senderIp)
			{
				arpTableItem.arp_t_time = MAX_ARP_ENTRY_TIME;
				sramWrite(ARP_TABLE_START_POINTER + ARP_TABLE_ITEM_LEN * i,
						ARP_TABLE_ITEM_LEN, (uint8_t*) &arpTableItem);
				return;
			}
		}
	}
	// Find free entry and save
	for (i = 0; i < MAX_ARP_ENTRY; i++)
	{
		// Read ARP entry form SRAM
		sramRead(ARP_TABLE_START_POINTER + ARP_TABLE_ITEM_LEN * i,
				ARP_TABLE_ITEM_LEN, (uint8_t*) &arpTableItem);
		if (arpTableItem.arp_t_ip == 0)
		{
			if (ethernet->type == HTONS(ETH_TYPE_IP))
			{
				register uint8_t a asm("r3");
				for (a = 0; a < 6; a++)
					arpTableItem.arp_t_mac[a] = ethernet->sourceMac[a];
				arpTableItem.arp_t_ip = ip->sourceIp;
				arpTableItem.arp_t_time = MAX_ARP_ENTRY_TIME;
				sramWrite(ARP_TABLE_START_POINTER + ARP_TABLE_ITEM_LEN * i,
						ARP_TABLE_ITEM_LEN, (uint8_t*) &arpTableItem);
				return;
			}
			if (ethernet->type == HTONS(ETH_TYPE_ARP))
			{
				register uint8_t a asm("r3");
				for (a = 0; a < 6; a++)
					arpTableItem.arp_t_mac[a] = arp->senderMac[a];
				arpTableItem.arp_t_ip = arp->senderIp;
				arpTableItem.arp_t_time = MAX_ARP_ENTRY_TIME;
				sramWrite(ARP_TABLE_START_POINTER + ARP_TABLE_ITEM_LEN * i,
						ARP_TABLE_ITEM_LEN, (uint8_t*) &arpTableItem);
				return;
			}
#ifdef DEBUG
			printf_P(PSTR("Bad ARP or IP packet\n"));
#endif
		}
	}
#ifdef DEBUG
	printf_P(PSTR("End of ARP table free space\n"));
#endif
}

/**
 * Search ip in ARP table and return item number
 */
uint8_t ARPentrySearch(uint32_t ip)
{
	// Check if MAC is in ARP table
	ARP_TABLE arpTableItem;
	register uint8_t i asm("r3");
	for (i = 0; i < MAX_ARP_ENTRY; i++)
	{
		// Read ARP entry form SRAM
		sramRead(ARP_TABLE_START_POINTER + ARP_TABLE_ITEM_LEN * i,
				ARP_TABLE_ITEM_LEN, (uint8_t*) &arpTableItem);
		if (arpTableItem.arp_t_ip == ip)
		{
			return (i);
		}
	}
	return (MAX_ARP_ENTRY);
}

/**
 * ARP reply
 */
void ARPreply(void)
{
#ifdef DEBUG
	printf_P(PSTR("ARP "));
#endif
	ETH_ETHERNET_HEADER* ethernet =
			(ETH_ETHERNET_HEADER*) &ethBuffer[ETHERNET_OFFSET];
	ETH_ARP_HEADER* arp = (ETH_ARP_HEADER*) &ethBuffer[ARP_OFFSET];
	// Check is ARP request OK
	if ((arp->hwType == HTONS(ETH_HW_ETHERNET)) && (arp->prType
			== HTONS(ETH_TYPE_IP))
			&& (arp->targetIp == ipAddr))
	{
		if (arp->opcode == ETH_ARP_REQUEST)
		{
#ifdef DEBUG
			printf_P(PSTR("request\n"));
#endif
			for (uint8_t i = 0; i < 6; i++)
			{
				// Set destination address
				ethernet->destMac[i] = ethernet->sourceMac[i];
				// Set source address
				ethernet->sourceMac[i] = macAddr[i];
				// Write out data to the ARP packet structure
				arp->targetMac[i] = ethernet->destMac[i];
				arp->senderMac[i] = macAddr[i];
			}
			// Set ARP opcode
			arp->opcode = ETH_ARP_REPLY;
			// Write out data to the ARP packet structure
			arp->targetIp = arp->senderIp;
			arp->senderIp = ipAddr;
			// Send packet
			netdeviceSend(ARP_REPLY_LEN, ethBuffer);
		}
		else if (arp->opcode == ETH_ARP_REPLY)
		{
#ifdef DEBUG
			printf_P(PSTR("reply\n"));
#endif
			ARPentryAdd();
		}
	}
}

/**
 * ARP request
 */
uint8_t ARPrequest(uint32_t destIp)
{
	ETH_ETHERNET_HEADER* ethernet =
			(ETH_ETHERNET_HEADER*) &ethBuffer[ETHERNET_OFFSET];
	ETH_ARP_HEADER* arp = (ETH_ARP_HEADER*) &ethBuffer[ARP_OFFSET];
	uint32_t destIpStore = destIp;
	// Check routing
	if ((destIp & netMaskAddr) != (ipAddr
			& netMaskAddr))
	{
		// use router
		destIp = netGateAddr;
	}
	// Make packet
	ethernet->type = HTONS(ETH_TYPE_ARP);
	ETHmakeHeader(destIp);
	arp->senderIp = ipAddr;
	arp->targetIp = destIp;
	register uint8_t i asm("r3");
	for (i = 0; i < 6; i++)
	{
		arp->senderMac[i] = macAddr[i];
		arp->targetMac[i] = 0x00;
	}
	arp->hwType = HTONS(ETH_HW_ETHERNET);
	arp->prType = HTONS(ETH_TYPE_IP);
	arp->hwLen = 6;
	arp->prLen = 4;
	arp->opcode = ETH_ARP_REQUEST;
	// Send
	netdeviceSend(ARP_REQUEST_LEN, ethBuffer);
	// Check reply
	ARP_TABLE arpTableItem;
	for (i = 0; i < 20; i++)
	{
		uint8_t index = ARPentrySearch(destIp);
		uint8_t index2 = ARPentrySearch(destIpStore);
		if (index < MAX_ARP_ENTRY || index2 < MAX_ARP_ENTRY)
		{
			if (index2 < MAX_ARP_ENTRY)
			{
				return (1);
			}
			sramRead(ARP_TABLE_START_POINTER + ARP_TABLE_ITEM_LEN * index,
					ARP_TABLE_ITEM_LEN, (uint8_t*) &arpTableItem);
			arpTableItem.arp_t_ip = destIpStore;
			sramWrite(ARP_TABLE_START_POINTER + ARP_TABLE_ITEM_LEN * index,
					ARP_TABLE_ITEM_LEN, (uint8_t*) &arpTableItem);
			return (1);
		}
		// Wait
		_delay_ms(10);
		// Refresh data
		ETHgetData();
	}
	return 0;
}

/**
 * Reply ICMP ping
 */
void ICMPsend(uint32_t destIp, uint8_t type, uint16_t seq, uint16_t id)
{
	ETH_IP_HEADER* ip = (ETH_IP_HEADER*) &ethBuffer[IP_OFFSET];
	ETH_ICMP_HEADER* icmp = (ETH_ICMP_HEADER*) &ethBuffer[ICMP_OFFSET];
	// Echo packet
	icmp->type = type;
	icmp->code = 0;
	icmp->id = id;
	icmp->seqNum = seq;
	icmp->checksum = 0;
	ip->totalLen = HTONS(0x0054);
	ip->protocol = IP_PROTOCOL_ICMP;
	IPmakeHeader(destIp);
	// Cheksum compute
	uint16_t result16 = ((ip->totalLen & 0xFF00) >> 8) | ((ip->totalLen
			& 0x00FF) << 8);
	result16 = result16 - ((ip->version & 0x0F) << 2);
	result16 = checksum(&icmp->type, result16, 0);
	icmp->checksum = ((result16 & 0xFF00) >> 8) | ((result16 & 0x00FF) << 8);
	// send
	netdeviceSend(ICMP_REPLY_LEN, ethBuffer);
#ifdef DEBUG
	printf_P(PSTR("PING send to %d.%d.%d.%d\n"),
				((uint8_t*)&(destIp))[0], ((uint8_t*)&(destIp))[1],
				((uint8_t*)&(destIp))[2], ((uint8_t*)&(destIp))[3]);
#endif
}

/**
 * Make ip header
 */
void IPmakeHeader(uint32_t destIp)
{
    ETH_ETHERNET_HEADER* ethernet =
            (ETH_ETHERNET_HEADER*) & ethBuffer[ETHERNET_OFFSET];
    ETH_IP_HEADER* ip = (ETH_IP_HEADER*) & ethBuffer[IP_OFFSET];
    // Ethernet header
    ETHmakeHeader(destIp);
    ethernet->type = HTONS(ETH_TYPE_IP);
    // IP
    ip->offset = 0x0040;
    ip->ttl = 128;
    //ip->id =
    ip->version = 0x45;
    ip->headerLen = 0;
    ip->destIp = destIp;
    ip->sourceIp = ipAddr;
    ip->checksum = 0;
    //Compute checksum
    uint16_t result16 = (ip->version & 0x0F) << 2;
    result16 = checksum(&ip->version, result16, 0);
    ip->checksum = ((result16 & 0xFF00) >> 8) | ((result16 & 0x00FF) << 8);
}
