/***************************************************************************
 *   Copyright (C) 2009 by Damian Kmiecik                                  *
 *   d0zoenator@gmail.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

#ifndef ETHERNET_H_
#define ETHERNET_H_

/**
 * Ethernet types
 */
#define ETH_TYPE_IP					0x0800
#define ETH_TYPE_ARP				0x0806

#define ETH_INTERRUPT_ENA			GICR |= _BV(INT0)
#define ETH_INTERRUPT_DIS			GICR &= ~_BV(INT0)

/**
 * ETHERNET type
 * based on IEEE 802.3 -->> http://standards.ieee.org/getieee802/802.3.html
 */
typedef struct {
	uint8_t destMac[6]; // Destination address [MAC]
	uint8_t sourceMac[6]; // Source address [MAC]
	uint16_t type; // Type/Length, if bigger then 1500, is a packet type
} ETH_ETHERNET_HEADER;

/**
 * ARP type
 * based on rfc826 -->> http://tools.ietf.org/html/rfc826
 */
typedef struct {
	uint16_t hwType; // Hardware type
	uint16_t prType; // Protocol type
	uint8_t hwLen; // Hardware size
	uint8_t prLen; // Protocol size
	uint16_t opcode; // Opcode
	uint8_t senderMac[6]; // Sender MAC address
	uint32_t senderIp; // Sender IP address
	uint8_t targetMac[6]; // Target MAC address
	uint32_t targetIp; // Target IP address
} ETH_ARP_HEADER;

/**
 * IP type
 * based on rfc791 -->> http://tools.ietf.org/html/rfc791
 */
typedef struct {
	uint8_t version; // Version
	uint8_t headerLen; // Header length
	uint16_t totalLen; // Total length
	uint16_t id; // Identification
	uint16_t offset; // Fragment offset
	uint8_t ttl; // Time to live
	uint8_t protocol; // Protocol
	uint16_t checksum; // Checksum
	uint32_t sourceIp; // Source IP
	uint32_t destIp; // Destination IP
} ETH_IP_HEADER;

/**
 * ICMP type
 * based on rfc792 -->> ftp://ftp.rfc-editor.org/in-notes/rfc792.txt
 */
typedef struct {
	uint8_t type; // ICMP type
	uint8_t code; // Code
	uint16_t checksum; // Checksum
	uint16_t id; // Identifier
	uint16_t seqNum; // Sequence number
} ETH_ICMP_HEADER;

/**
 * UDP type
 * based on rfc768 -->> ftp://ftp.rfc-editor.org/in-notes/rfc768.txt
 */
typedef struct {
	uint16_t sourcePort; // Source port
	uint16_t destPort; // Destination port
	uint16_t length; // Length
	uint16_t checksum; // Checksum
} ETH_UDP_HEADER;

/**
 * TCP type
 * based on rfc793 -->> http://tools.ietf.org/html/rfc793
 */
typedef struct {
	uint16_t sourcePort; // Source port
	uint16_t destPort; // Destination port
	uint32_t seqNum; // Sequence number
	uint32_t ackNum; // Acknowledgment number
	uint8_t offset; // Data offset
	uint8_t flags; // Flags
	uint16_t window; // Window
	uint16_t checksum; // Checksum
	uint16_t urgentPtr; // Urgent pointer
} ETH_TCP_HEADER;

/**
 * Header size
 */
#define ETH_TCP_HEADER_LEN			20
#define ETH_UDP_HEADER_LEN			8
#define ETH_ICMP_HEADER_LEN			8
#define ETH_IP_HEADER_LEN			20
#define ETH_ARP_HEADER_LEN			18
#define ETH_ETHERNET_HEADER_LEN			14

/**
 * TCP Flags
 */
#define  TCP_FIN_FLAG				0x01
#define  TCP_SYN_FLAG				0x02
#define  TCP_RST_FLAG				0x04
#define  TCP_PSH_FLAG				0x08
#define  TCP_ACK_FLAG				0x10

/**
 * TCP Status
 */
#define TCP_LISTEN_STATE			0x01
#define TCP_SYN_SENT_STATE			0x02
#define TCP_SYN_RECEIVED_STATE			0x03
#define TCP_ESTABLISHED_STATE			0x04
#define TCP_FIN_WAIT_1_STATE			0x05
#define TCP_FIN_WAIT_2_STATE			0x06
#define TCP_CLOSE_WAIT_STATE			0x07
#define TCP_CLOSING_STATE			0x08
#define TCP_LAST_ACK_STATE			0x09
#define TCP_TIME_WAIT_STATE			0x0A
#define TCP_CLOSED_STATE			0x0B

/**
 * Buffer offset
 */
#define ETHERNET_OFFSET				0x00
#define ARP_OFFSET				0x0E
#define IP_OFFSET				0x0E
#define ICMP_OFFSET				0x22
#define ICMP_DATA				0x2A
#define TCP_OFFSET				0x22
#define TCP_DATA				(ETH_ETHERNET_HEADER_LEN+ETH_TCP_HEADER_LEN+ETH_IP_HEADER_LEN)
#define UDP_OFFSET				0x22

/**
 * IP protocol
 */
#define	IP_PROTOCOL_ICMP			0x01
#define	IP_PROTOCOL_TCP				0x06
#define	IP_PROTOCOL_UDP				0x11

/**
 * TCP Send flags
 */
#define TCP_SEND_FLAG				0x01
#define TCP_TIME_OUT_SEND_FLAG		0x02
#define TCP_MORE_DATA_SEND_FLAG		0x04

/**
 * TCP port item
 */
typedef struct {
	uint16_t port; // Port
	void (*fp)(uint8_t); // Pointer to destination function
} TCP_PORT_ITEM;

/**
 * UDP port item
 */
typedef struct {
	uint16_t port; // Port
	void (*fp)(void); // Pointer to destination function
} UDP_PORT_ITEM;

/**
 * TCP & UDP PORT table
 */
#define MAX_APP_ENTRY				5
extern TCP_PORT_ITEM TCP_PORT_TABLE[MAX_APP_ENTRY];
extern UDP_PORT_ITEM UDP_PORT_TABLE[MAX_APP_ENTRY];

/**
 * Byte swap
 */
#define HTONS(n) (unsigned int)((((unsigned int) (n)) << 8) | (((unsigned int) (n)) >> 8))
#define HTONS32(x) ((x & 0xFF000000)>>24)+((x & 0x00FF0000)>>8)+((x & 0x0000FF00)<<8)+((x & 0x000000FF)<<24)

/**
 * Performance configuration
 * Ethernet Test Board by Damian Kmiecik has 32kB on-board SRAM,
 * 1kB on-chip SRAM and 128kB on-board EEPROM
 */
#if defined (__AVR_ATmega32__)
#define MAX_TCP_ENTRY		10
#define MAX_TCP_ENTRY_TIME	30
#define MAX_UDP_ENTRY		10
#define MAX_ARP_ENTRY		10
#define MAX_ARP_ENTRY_TIME	100
#if SD_RAW_WRITE_SUPPORT
#define MTU_SIZE 			400
#else
#define MTU_SIZE 			400
#endif
#endif

/**
 * Ethernet packet defines
 */
#define ICMP_REQUEST			0x08
#define ICMP_REPLY				0x00
#define ETH_ARP_REQUEST			0x0100
#define ETH_ARP_REPLY			0x0200
#define ETH_HW_ETHERNET			0x0001
#define ARP_REPLY_LEN			60
#define ICMP_REPLY_LEN			98
#define ARP_REQUEST_LEN			42
#define MAX_TCP_WINDOW			(MTU_SIZE-100)

/**
 * ARP SRAM table
 */
#define ARP_TABLE_ITEM_LEN			sizeof(ARP_TABLE)
#define ARP_TABLE_START_POINTER		TCP_TABLE_END_POINTER+1
#define ARP_TABLE_END_POINTER		ARP_TABLE_START_POINTER+(ARP_TABLE_ITEM_LEN*MAX_ARP_ENTRY)

/**
 * TCP SRAM table
 */
#define TCP_TABLE_ITEM_LEN			sizeof(TCP_TABLE)
#define TCP_TABLE_START_POINTER 	TCP_STACK_END_POINTER+1
#define TCP_TABLE_END_POINTER		TCP_TABLE_START_POINTER+(TCP_TABLE_ITEM_LEN*(MAX_TCP_ENTRY+1))

/**
 * SRAM TCP stack data
 */
#define TCP_STACK_ITEM_LEN			MAX_TCP_WINDOW
#define TCP_STACK_START_POINTER		0
#define TCP_STACK_END_POINTER		TCP_STACK_START_POINTER+(TCP_STACK_ITEM_LEN*MAX_TCP_ENTRY)

typedef struct {
	uint8_t arp_t_mac[6];
	uint32_t arp_t_ip;
	uint8_t arp_t_time;
} ARP_TABLE;

typedef struct {
	uint32_t ip;
	uint16_t src_port;
	uint16_t dest_port;
	uint32_t ack_counter;
	uint32_t seq_counter;
	uint8_t status;
	uint8_t flags;
	uint8_t time;
	// Amount of incoming/outgoing data
	uint16_t dataLen;
	uint8_t send_status;
	uint16_t app_status;
} TCP_TABLE;

typedef struct {
	volatile uint32_t ip;
	volatile uint8_t no;
	volatile uint8_t result;
} PING_STRUCT;

extern PING_STRUCT ping;
extern TCP_TABLE tcpTableItem;
extern uint8_t ethBuffer[MTU_SIZE + 1];

// Ethernet
extern void ethernetInit(void);
extern void ETHgetData(void);
extern void ETHpacketProcess(void);
extern void ETHmakeHeader(uint32_t destIp);
extern uint32_t htons32(const uint32_t val);
extern uint16_t htons(const uint16_t val);
//ARP
extern void ARPentryAdd(void);
extern uint8_t ARPentrySearch(uint32_t ip);
extern void ARPreply(void);
extern void ARPtimerDo(void);
extern void ARPprintTable(void);
extern uint8_t ARPrequest(uint32_t destIp);
// IP & ICMP
extern void ICMPsend(uint32_t destIp, uint8_t type, uint16_t seq, uint16_t id);
extern void IPmakeHeader(uint32_t destIp);
// UDP
extern void UDPpacketProcess(void);
// TCP
extern void TCPprintTable(void);
extern void TCPtimerDo(void);
extern void TCPmakeNewPacket(uint16_t len);
extern void TCPentryDelete(uint8_t tcpIndex);
extern void TCPportOpen(uint32_t destIp, uint16_t destPort, uint16_t srcPort);
extern void TCPpacketProcess(void);
extern void TCPappAdd(const uint16_t port, void(*fp1)(uint8_t));
extern void TCPdataSend(const uint8_t tcpIndex);

#endif /* STACK_H_ */
