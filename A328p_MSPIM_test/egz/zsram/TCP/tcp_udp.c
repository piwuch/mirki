/***************************************************************************
 *   Copyright (C) 2009 by Damian Kmiecik                                  *
 *   d0zoenator@gmail.com                                                  *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 *                                                                         *
 ***************************************************************************/

#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include "../Lib/checksum.h"
#include "../Devices/netdevice.h"
#include "../Devices/timer.h"
#include "../Devices/stdout.h"
#include "../Interfaces/sram.h"
#include "ethernet.h"

TCP_PORT_ITEM TCP_PORT_TABLE[MAX_APP_ENTRY] = { { 0, 0 } };
UDP_PORT_ITEM UDP_PORT_TABLE[MAX_APP_ENTRY] = { { 0, 0 } };
TCP_TABLE tcpTableItem;

#ifdef DEBUG
/**
 * Print TCP table
 */
void TCPprintTable(void) {
	TCP_TABLE tableItem;
	uint8_t i;
	printf_P(PSTR("_ TCP TABLE _\n"));
	for (i = 0; i < MAX_TCP_ENTRY; i++) {
		// Read ARP entry form SRAM
		sramRead(TCP_TABLE_START_POINTER + TCP_TABLE_ITEM_LEN * i,
				TCP_TABLE_ITEM_LEN, (uint8_t*) &tableItem);
		printf_P(PSTR("%d.%d.%d.%d dest %d src %d for %d\n"),
				((uint8_t*) &(tableItem.ip))[0],
				((uint8_t*) &(tableItem.ip))[1],
				((uint8_t*) &(tableItem.ip))[2],
				((uint8_t*) &(tableItem.ip))[3], tableItem.dest_port,
				tableItem.src_port, tableItem.time);
	}
	printf_P(PSTR("_ TCP TABLE END _\n"));
}
#endif

/**
 * TCP timer interrupt
 */
void TCPtimerDo(void) {
	uint8_t tcpIndex;
	for (tcpIndex = 0; tcpIndex < MAX_TCP_ENTRY; tcpIndex++) {
		// Read TCP entry form SRAM
		sramRead(TCP_TABLE_START_POINTER + TCP_TABLE_ITEM_LEN * tcpIndex,
				TCP_TABLE_ITEM_LEN, (uint8_t*) &tcpTableItem);
		if (tcpTableItem.status != TCP_CLOSED_STATE) {
			if ((tcpTableItem.time == 0)) {
				tcpTableItem.flags = TCP_RST_FLAG | TCP_ACK_FLAG;
				TCPmakeNewPacket(0);
				tcpTableItem.status = TCP_CLOSED_STATE;
			} else {
				tcpTableItem.time--;
			}
			if (tcpTableItem.time < MAX_TCP_ENTRY_TIME-4) {
				if (tcpTableItem.send_status & TCP_SEND_FLAG) {
					tcpTableItem.send_status |= TCP_TIME_OUT_SEND_FLAG;
					TCPdataSend(tcpIndex);
				}
			}
		}
		// Write TCP entry to SRAM
		sramWrite(TCP_TABLE_START_POINTER + TCP_TABLE_ITEM_LEN * tcpIndex,
				TCP_TABLE_ITEM_LEN, (uint8_t*) &tcpTableItem);
	}
}

/**
 * TCP packet process
 */
uint8_t TCPentryAdd(void) {
	ETH_IP_HEADER* ip = (ETH_IP_HEADER*) &ethBuffer[IP_OFFSET];
	ETH_TCP_HEADER* tcp = (ETH_TCP_HEADER*) &ethBuffer[TCP_OFFSET];
	uint8_t index;
	// Create new
	for (index = 0; index < MAX_TCP_ENTRY; index++) {
		// Read TCP entry form SRAM
		sramRead(TCP_TABLE_START_POINTER + TCP_TABLE_ITEM_LEN * index,
				TCP_TABLE_ITEM_LEN, (uint8_t*) &tcpTableItem);
		if (tcpTableItem.status == TCP_CLOSED_STATE) {
			tcpTableItem.ip = ip->sourceIp;
			tcpTableItem.src_port = tcp->sourcePort;
			tcpTableItem.dest_port = tcp->destPort;
			tcpTableItem.ack_counter = tcp->ackNum;
			tcpTableItem.seq_counter = tcp->seqNum;
			tcpTableItem.flags = tcp->flags;
			tcpTableItem.time = MAX_TCP_ENTRY_TIME;
			// SYN RECEIVED, send SYN-ACK and wait for ACK
			tcpTableItem.status = TCP_LISTEN_STATE;
			tcpTableItem.dataLen = 0;
			tcpTableItem.send_status = 0;
			tcpTableItem.app_status = 0;
			return index;
		}
	}
	// No free space
	return MAX_TCP_ENTRY;
}

/**
 * Search in TCP table
 */
uint8_t TCPentrySearch(const uint32_t srcIp, const uint16_t srcPort) {
	uint8_t index;
	// Search
	for (index = 0; index < MAX_TCP_ENTRY; index++) {
		// Read TCP entry form SRAM
		sramRead(TCP_TABLE_START_POINTER + TCP_TABLE_ITEM_LEN * index,
				TCP_TABLE_ITEM_LEN, (uint8_t*) &tcpTableItem);
		// Check is in TCP table
		if (tcpTableItem.status != TCP_CLOSED_STATE) {
			if ((tcpTableItem.ip == srcIp)
					&& (tcpTableItem.src_port == srcPort)) {
				return index;
			}
		}
	}
	return MAX_TCP_ENTRY;
}

/**
 * Create new TCP packet
 */
void TCPmakeNewPacket(uint16_t len) {
	ETH_IP_HEADER* ip = (ETH_IP_HEADER*) &ethBuffer[IP_OFFSET];
	ETH_TCP_HEADER* tcp = (ETH_TCP_HEADER*) &ethBuffer[TCP_OFFSET];
	uint16_t result16;
	uint16_t bufferLen;
	uint32_t result32;
	// Complain data
	tcp->sourcePort = tcpTableItem.dest_port;
	tcp->destPort = tcpTableItem.src_port;
	tcp->urgentPtr = 0;
	tcp->window = HTONS(MAX_TCP_WINDOW);
	tcp->offset = 0x50;
	tcp->flags = tcpTableItem.flags;
	result32 = htons32(tcpTableItem.seq_counter);
	if (tcpTableItem.flags & TCP_SYN_FLAG) {
		tcpTableItem.ack_counter = 0xe6acb148;
		result32++;
		ethBuffer[TCP_DATA] = 2;
		ethBuffer[TCP_DATA + 1] = 4;
		ethBuffer[TCP_DATA + 2] = (MAX_TCP_WINDOW >> 8) & 0xff;
		ethBuffer[TCP_DATA + 3] = MAX_TCP_WINDOW & 0xff;
		len = 0x04;
		tcp->offset = 0x60;
	}
	tcp->ackNum = htons32(result32);
	tcp->seqNum = tcpTableItem.ack_counter;
	bufferLen = ETH_IP_HEADER_LEN + ETH_TCP_HEADER_LEN + len;
	ip->totalLen = htons(bufferLen);
	bufferLen += ETH_ETHERNET_HEADER_LEN;
	ip->protocol = IP_PROTOCOL_TCP;
	IPmakeHeader(tcpTableItem.ip);
	// Checksum
	tcp->checksum = 0;
	result16 = htons(ip->totalLen) + 8;
	result16 = result16 - ((ip->version & 0x0F) << 2);
	result32 = result16 - 2;
	result16 = checksum((&ip->version + 12), result16, result32);
	tcp->checksum = htons(result16);
	// Send packet
	netdeviceSend(bufferLen, ethBuffer);
}

/**
 * TCP entry delete
 */
void TCPentryDelete(uint8_t tcpIndex) {
	tcpTableItem.status = TCP_CLOSED_STATE;
	// Write TCP entry to SRAM
	sramWrite(TCP_TABLE_START_POINTER + TCP_TABLE_ITEM_LEN * tcpIndex,
			TCP_TABLE_ITEM_LEN, (uint8_t*) &tcpTableItem);
}

void TCPportSearch(const uint16_t destPort) {
	for (portIndex = 0; portIndex < MAX_APP_ENTRY; portIndex++)
		if ((TCP_PORT_TABLE[portIndex].port) && (TCP_PORT_TABLE[portIndex].port == destPort))
			break;
	if (portIndex == MAX_APP_ENTRY) {
		DEBUG(PSTR("TCP: port %d is closed\n"), htons(tcp->destPort));
	return;
	}
}

/**
 * TCP packet process
 */
void TCPpacketProcess(void) {
	ETH_IP_HEADER* ip = (ETH_IP_HEADER*) &ethBuffer[IP_OFFSET];
	ETH_TCP_HEADER* tcp = (ETH_TCP_HEADER*) &ethBuffer[TCP_OFFSET];
	uint32_t result32;
	uint8_t portIndex;
	uint8_t tcpIndex;
	// Search port in table
	
	// Search for exists
	tcpIndex = TCPentrySearch(ip->sourceIp, tcp->sourcePort);
	// No one
	if (tcpIndex != MAX_TCP_ENTRY) {
		// Update entry
		tcpTableItem.ack_counter = tcp->ackNum;
		tcpTableItem.flags = tcp->flags;
		tcpTableItem.time = MAX_TCP_ENTRY_TIME;
		result32 = htons(ip->totalLen) - ETH_IP_HEADER_LEN - ((tcp->offset
				& 0xF0) >> 2);
		result32 = result32 + htons32(tcp->seqNum);
		tcpTableItem.seq_counter = htons32(result32);
	} else {
		// Add
		tcpIndex = TCPentryAdd();
	}
	// Stack full
	if (tcpIndex == MAX_TCP_ENTRY) {
#ifdef DEBUG
		printf_P(PSTR("TCP Stack full\n"));
#endif
		// TODO: Send RST
		goto theEndTCP;
	}
#ifdef DEBUG
	printf_P(PSTR("TCP %d : %d: "), tcpIndex, htons(tcp->destPort));
#endif
	// Check if new connection: SYN received
	if (tcpTableItem.status == TCP_LISTEN_STATE) {
		// Excepted SYN
		if (tcpTableItem.flags == TCP_SYN_FLAG) {
#ifdef DEBUG
			printf_P(PSTR("SYN-RECEIVED\n"));
#endif
			// Answer SYN+ACK
			tcpTableItem.flags = TCP_ACK_FLAG | TCP_SYN_FLAG;
			// Change state to half-opened connection, waiting for ACK
			tcpTableItem.status = TCP_SYN_RECEIVED_STATE;
			// Send
			TCPmakeNewPacket(0);
		} else {
#ifdef DEBUG
			printf_P(PSTR("STATUS ERROR SYN LISTEN\n"));
#endif
			// If no SYN then delete
			TCPentryDelete(tcpIndex);
		}
		goto theEndTCP;
	}
	// Check if SYN+ACK requested and received
	if (tcpTableItem.status == TCP_SYN_SENT_STATE) {
		// Excepted SYN+ACK
		if (tcpTableItem.flags == (TCP_ACK_FLAG | TCP_SYN_FLAG)) {
#ifdef DEBUG
			printf_P(PSTR("ESTABLISHED\n"));
#endif
			// Connection established
			tcpTableItem.status = TCP_ESTABLISHED_STATE;
			// Increment SEQ counter
			tcpTableItem.seq_counter = htons32(
					htons32(tcpTableItem.seq_counter) + 1);
			// Answer ACK
			tcpTableItem.flags = TCP_ACK_FLAG;
			// Send
			TCPmakeNewPacket(0);
		} else {
#ifdef DEBUG
			printf_P(PSTR("STATUS ERROR SYN SENT\n"));
#endif
			// If no SYN+ACK then delete
			TCPentryDelete(tcpIndex);
		}
		goto theEndTCP;
	}
	// Check if first ACK received
	if (tcpTableItem.status == TCP_SYN_RECEIVED_STATE) {
		// Excepted ACK
		if (tcpTableItem.flags == TCP_ACK_FLAG) {
#ifdef DEBUG
			printf_P(PSTR("ESTABLISHED\n"));
#endif
			// Connection established
			tcpTableItem.status = TCP_ESTABLISHED_STATE;
		} else {
#ifdef DEBUG
			printf_P(PSTR("STATUS ERROR SYN\n"));
#endif
			// If no ACK then delete
			TCPentryDelete(tcpIndex);
		}
		goto theEndTCP;
	}
	// Check if RST received
	if (tcpTableItem.flags & TCP_RST_FLAG) {
#ifdef DEBUG
		printf_P(PSTR("CLOSED\n"));
#endif
		// Closing connection
		tcpTableItem.status = TCP_CLOSED_STATE;
		goto theEndTCP;
	}
	// Check if Connection ESTABLISHED
	if (tcpTableItem.status == TCP_ESTABLISHED_STATE) {
		// Check if FIN received
		if ((tcpTableItem.flags & TCP_FIN_FLAG) && (tcpTableItem.flags
				& TCP_ACK_FLAG)) {
#ifdef DEBUG
			printf_P(PSTR("LAST-ACK\n"));
#endif
			// Last ACK, finished
			tcpTableItem.status = TCP_LAST_ACK_STATE;
			// Answer SYN+ACK
			tcpTableItem.flags = TCP_ACK_FLAG | TCP_FIN_FLAG;
			// Send
			TCPmakeNewPacket(0);
			goto theEndTCP;
		}
		// Check if ACK received
		else if (tcpTableItem.flags == TCP_ACK_FLAG) {
#ifdef DEBUG
			printf_P(PSTR("ACK RECEIVED\n"));
#endif
			uint16_t tcpDataLen = htons(ip->totalLen) - ETH_IP_HEADER_LEN
					- ETH_TCP_HEADER_LEN;
			// Check if ACK with data or ACK for sended data
			if (tcpDataLen) {
				tcpTableItem.dataLen = tcpDataLen;
				// ACK
				tcpTableItem.flags = TCP_ACK_FLAG;
				// Send ACK
				TCPmakeNewPacket(0);
				// Start application
				tcpTableItem.app_status++;
				TCP_PORT_TABLE[portIndex].fp(tcpIndex);
			} else {
				// Send process ACK
				TCPdataSend(tcpIndex);
			}
			goto theEndTCP;
		}
		// Check if ACK+PSH received
		else if (tcpTableItem.flags == (TCP_ACK_FLAG | TCP_PSH_FLAG)) {
#ifdef DEBUG
			printf_P(PSTR("ACK+PSH RECEIVED\n"));
#endif
			uint16_t tcpDataLen = htons(ip->totalLen) - ETH_IP_HEADER_LEN
					- ETH_TCP_HEADER_LEN;
			tcpTableItem.dataLen = tcpDataLen;
			// ACK
			tcpTableItem.flags = TCP_ACK_FLAG;
			// Send ACK
			TCPmakeNewPacket(0);
			// Start application
			tcpTableItem.app_status++;
			TCP_PORT_TABLE[portIndex].fp(tcpIndex);
		} else {
#ifdef DEBUG
			printf_P(PSTR("STATUS ERROR %X FLAGS\n"), tcpTableItem.flags);
#endif
			// If broken TCP
			TCPentryDelete(tcpIndex);
		}
		goto theEndTCP;
	}
	// Check if last ACK received
	if (tcpTableItem.status == TCP_LAST_ACK_STATE) {
		// Excepted ACK
		if (tcpTableItem.flags == TCP_ACK_FLAG) {
#ifdef DEBUG
			printf_P(PSTR("CLOSED\n"));
#endif
			// CLOSED
			tcpTableItem.status = TCP_CLOSED_STATE;
			// Answer ACK
			tcpTableItem.flags = TCP_ACK_FLAG;
			// Send
			TCPmakeNewPacket(0);
		} else {
#ifdef DEBUG
			printf_P(PSTR("STATUS ERROR: LAST ACK\n"));
#endif
			// If no ACK then delete
			TCPentryDelete(tcpIndex);
		}
		goto theEndTCP;
	}
#ifdef DEBUG
	printf_P(PSTR("WTF???\n"));
#endif
	theEndTCP:
	// Write TCP entry to SRAM
	sramWrite(TCP_TABLE_START_POINTER + TCP_TABLE_ITEM_LEN * tcpIndex,
			TCP_TABLE_ITEM_LEN, (uint8_t*) &tcpTableItem);
}

/**
 * Open TCP port
 */
void TCPportOpen(const uint32_t destIp, const uint16_t destPort,
		const uint16_t srcPort) {
	uint8_t tcpIndex;
	for (tcpIndex = 0; tcpIndex < MAX_TCP_ENTRY; tcpIndex++) {
		// Read TCP entry form SRAM
		sramRead(TCP_TABLE_START_POINTER + TCP_TABLE_ITEM_LEN * tcpIndex,
				TCP_TABLE_ITEM_LEN, (uint8_t*) &tcpTableItem);
		if (tcpTableItem.status == TCP_CLOSED_STATE) {
			tcpTableItem.ip = destIp;
			tcpTableItem.src_port = htons(destPort);
			tcpTableItem.dest_port = htons(srcPort);
			tcpTableItem.ack_counter = 0;
			tcpTableItem.seq_counter = 0;
			tcpTableItem.time = MAX_TCP_ENTRY_TIME;
			tcpTableItem.dataLen = 0;
			tcpTableItem.send_status = 0;
			break;
		}
	}
	if (tcpIndex == MAX_TCP_ENTRY) {
#ifdef DEBUG
		printf_P(PSTR("ERROR: Server busy"));
#endif
		return;
	}
#ifdef DEBUG
	printf_P(PSTR("TCP %d: Port %d open\n"), tcpIndex, srcPort);
#endif
	tcpTableItem.flags = TCP_SYN_FLAG;
	TCPmakeNewPacket(0);
	tcpTableItem.status = TCP_SYN_SENT_STATE;
	// Write TCP entry to SRAM
	sramWrite(TCP_TABLE_START_POINTER + TCP_TABLE_ITEM_LEN * tcpIndex,
			TCP_TABLE_ITEM_LEN, (uint8_t*) &tcpTableItem);
}

/**
 * Close connection
 * Send FIN
 */
void TCPportClose(const uint8_t tcpIndex) {
#ifdef DEBUG
	printf_P(PSTR("TCP %d: Port close\n"), tcpIndex);
#endif
	// Read TCP entry form SRAM
	sramRead(TCP_TABLE_START_POINTER + TCP_TABLE_ITEM_LEN * tcpIndex,
			TCP_TABLE_ITEM_LEN, (uint8_t*) &tcpTableItem);
	// TODO: must be implemented pasive close!!!!
	tcpTableItem.flags = TCP_RST_FLAG;
	TCPmakeNewPacket(0);
	tcpTableItem.status = TCP_CLOSED_STATE;
	// Write TCP entry to SRAM
	sramWrite(TCP_TABLE_START_POINTER + TCP_TABLE_ITEM_LEN * tcpIndex,
			TCP_TABLE_ITEM_LEN, (uint8_t*) &tcpTableItem);
}

/**
 * Add TCP app
 */
void TCPappAdd(const uint16_t port, void(*fp1)(uint8_t)) {
	uint8_t i;
	for (i = 0; i < MAX_APP_ENTRY; i++) {
		if (TCP_PORT_TABLE[i].port == 0) {
#ifdef DEBUG
			printf_P(PSTR("Note: TCP open port %d\n"), port);
#endif
			TCP_PORT_TABLE[i].port = htons(port);
			TCP_PORT_TABLE[i].fp = *fp1;
			return;
		}
	}
}

/**
 * Add UDP app
 */
void UDPappAdd(const uint16_t port, void(*fp1)(void)) {
	uint8_t i;
	for (i = 0; i < MAX_APP_ENTRY; i++) {
		if (UDP_PORT_TABLE[i].port == 0) {
#ifdef DEBUG
			printf_P(PSTR("Note: UDP open port %d\n"), port);
#endif
			UDP_PORT_TABLE[i].port = htons(port);
			UDP_PORT_TABLE[i].fp = *fp1;
			return;
		}
	}
}

/**
 * Prepare packet and send
 */
void UDPmakeNewPacket(const uint16_t len, const uint16_t srcPort,
		const uint16_t dstPort, const uint32_t ipAddr) {
	ETH_UDP_HEADER* udp = (ETH_UDP_HEADER*) &ethBuffer[UDP_OFFSET];
	ETH_IP_HEADER* ip = (ETH_IP_HEADER*) &ethBuffer[IP_OFFSET];
	uint16_t bufferLen;
	// Complain data
	udp->checksum = 0;
	udp->length = htons(len + ETH_UDP_HEADER_LEN);
	udp->sourcePort = htons(srcPort);
	udp->destPort = htons(dstPort);
	bufferLen = ETH_IP_HEADER_LEN + ETH_UDP_HEADER_LEN + len;
	ip->totalLen = htons(bufferLen);
	bufferLen += ETH_ETHERNET_HEADER_LEN;
	ip->protocol = IP_PROTOCOL_UDP;
	IPmakeHeader(ipAddr);
	// Send packet
	netdeviceSend(bufferLen, ethBuffer);
}

/**
 * UDP packet process
 */
void UDPpacketProcess(void) {
	ETH_UDP_HEADER* udp = (ETH_UDP_HEADER*) &ethBuffer[UDP_OFFSET];
	uint8_t i;
	// Search port in table
	for (i = 0; i < MAX_APP_ENTRY; i++) {
		if (UDP_PORT_TABLE[i].port == htons(udp->destPort)) {
			UDP_PORT_TABLE[i].fp();
			return;
		}
	}
#ifdef DEBUG
	printf_P(PSTR("blocked port\n"));
#endif
}

/**
 * Send data from SRAM buffer
 */
void TCPdataSend(const uint8_t tcpIndex) {
#ifdef DEBUG
	printf_P(PSTR("TCP %u: TCPdataSend "), tcpIndex);
#endif
	// Check for timeout
	if (tcpTableItem.send_status & TCP_TIME_OUT_SEND_FLAG) {
#ifdef DEBUG
		printf_P(PSTR("Retransmission "));
#endif
		// Retransmission
		tcpTableItem.ack_counter = htons32(htons32(tcpTableItem.ack_counter)
				- (tcpTableItem.dataLen));
	} else {
		// If no data to be send, Last ACK received
		if (tcpTableItem.send_status & TCP_SEND_FLAG) {
			tcpTableItem.send_status = 0;
#ifdef DEBUG
			printf_P(PSTR(" NEXT DATA\n"));
#endif
			uint8_t portIndex;
			// Search port in table
			for (portIndex = 0; portIndex < MAX_APP_ENTRY; portIndex++)
				if ((TCP_PORT_TABLE[portIndex].port)
						&& (TCP_PORT_TABLE[portIndex].port
								== tcpTableItem.dest_port))
					break;
			tcpTableItem.dataLen = 0;
			tcpTableItem.app_status++;
			TCP_PORT_TABLE[portIndex].fp(tcpIndex);
			return;
		}
	}
#ifdef DEBUG
	printf_P(PSTR("%u Bytes\n"), tcpTableItem.dataLen);
#endif
	tcpTableItem.send_status = TCP_SEND_FLAG;
	// Write packet data to SRAM (for retransmissions)
	sramWrite(TCP_STACK_START_POINTER + (TCP_STACK_ITEM_LEN * tcpIndex),
			tcpTableItem.dataLen, &ethBuffer[TCP_DATA]);
	// Set time out
	TCPmakeNewPacket(tcpTableItem.dataLen);
}
