/*
 * main.c
 *
 *  Created on: 2009-12-22
 *      Author: damian
 */

#include <avr/io.h>
#include <string.h>
#include <avr/wdt.h>
#include <inttypes.h>
#include <util/delay.h>
#include "Interfaces/sram.h"
#include "Devices/stdout.h"
#include "Devices/timer.h"
#include "TCP/ethernet.h"
#include "FAT/fat.h"
#include "Devices/partition.h"
#include "Drivers/sd_raw.h"
#include "Interfaces/rs232.h"
#include "Configuration/conf.h"
#include "HTTPD/httpd.h"
#include "Devices/ds18b20.h"

static void sdCardInit(void);

uint8_t print_disk_info(const struct fat_fs_struct* fs) {
	if (!fs)
		return 0;

	struct sd_raw_info disk_info;
	if (!sd_raw_get_info(&disk_info))
		return 0;

	printf_P(PSTR("manuf:  0x%X\n"), disk_info.manufacturer);
	printf_P(PSTR("oem:    "));
	printf((char*) disk_info.oem);
	printf("\n");
	printf_P(PSTR("prod:   "));
	printf((char*) disk_info.product);
	printf("\n");
	printf_P(PSTR("rev:    0x%X\n"), disk_info.revision);
	printf_P(PSTR("serial: 0x%X\n"), disk_info.serial);
	printf_P(PSTR("date:   %u/%u\n"), disk_info.manufacturing_month,
			disk_info.manufacturing_year);
	printf_P(PSTR("size:   %uMB\n"), disk_info.capacity / 1024 / 1024);
	printf_P(PSTR("copy:   %u\n"), disk_info.flag_copy);
	printf_P(PSTR("wr.pr.: %u/%u\n"), disk_info.flag_write_protect_temp,
			disk_info.flag_write_protect);
	printf_P(PSTR("format: %u\n"), disk_info.format);
	printf_P(PSTR("free:   %u/%u\n"), fat_get_fs_free(fs), fat_get_fs_size(fs));
	return 1;
}

void readTemperature(void) {
	temperature = ds18b20_read_temp();
}

int main(void) {
	sramInit();
	stdoutInit();
	printf_P(PSTR("Hello World!\n"));
	timerInit();
	ds18b20_init();
	_delay_ms(100);
	timerAdd(readTemperature, 1000);
	sdCardInit();
	if (!confRead()) {
#ifdef DEBUG
		printf_P(PSTR("Reading default configuration\n"));
#endif
		confDefault();
#if SD_RAW_WRITE_SUPPORT
		confSave();
#endif
	}
	confPrint();
	ethernetInit();
	//wdt_enable(WDTO_2S);
	PORTD |= _BV(PORTD3);
	MCUCR |= _BV(ISC10);
	GICR |= _BV(INT1);
	DDRD &= ~_BV(PD4);
	DDRD &= ~_BV(PD5);
	PORTD |= _BV(PD4) | _BV(PD5);
	sei();
#ifdef DEBUG
	for (uint16_t addr = 0; addr < 0x7FFF; addr++) {
		sramWriteByte(addr, 0xEE);
		sramReadByte(addr + 1);
		sramWriteByte(addr + 1, 0xFF);
		sramReadByte(addr + 2);
		if (sramReadByte(addr) != 0xEE) {
			printf_P(PSTR("SRAM ERROR AT %d\n"), addr);
		}
	}
	for (uint16_t addr = 0; addr < 0xFFFF; addr++) {
		sramWriteByte(addr, 0x00);
	}
	printf_P(PSTR("All 64kB SRAM was scaned and verified\n"));
#endif

	ARPrequest(0x0100000A);
	ICMPsend(0x0100000A, ICMP_REQUEST, 0, 0);

	httpdInit(80);

	while (1) {
		wdt_reset();
		ETHgetData();
	}
	return 0;
}

// SD CARD INTERRUPT
ISR(SIG_INTERRUPT1)
{
	sdCardInit();
}

void sdCardInit(void) {
	if (sd_raw_init()) {
#if SD_RAW_WRITE_SUPPORT
		partition = partition_open(sd_raw_read, sd_raw_read_interval,
				sd_raw_write, sd_raw_write_interval, 0);
#else
		partition = partition_open(sd_raw_read, sd_raw_read_interval, 0, 0, 0);
#endif
		if (!partition) {
			/* If the partition did not open, assume the storage device
			 * is a "superfloppy", i.e. has no MBR.
			 */
#if SD_RAW_WRITE_SUPPORT
			partition = partition_open(sd_raw_read, sd_raw_read_interval,
					sd_raw_write, sd_raw_write_interval, -1);
#else
			partition = partition_open(sd_raw_read, sd_raw_read_interval, 0, 0,
					-1);
#endif
			if (!partition) {
#ifdef DEBUG
				printf_P(PSTR("Opening partition failed\n"));
#endif
				goto endFuncSdCardInit;
			}
		}
		/* open file system */
		fs = fat_open(partition);
		if (!fs) {
#ifdef DEBUG
			printf_P(PSTR("Opening filesystem failed\n"));
#endif
			goto endFuncSdCardInit;
		}
		cardPresence = 1;
#ifdef DEBUG
		printf_P(PSTR("SD card connected!\n"));
#endif
		return;
	}
#ifdef DEBUG
	printf_P(PSTR("SD card disconnected!\n"));
#endif
	endFuncSdCardInit: fat_close_dir(dd);
	fat_close(fs);
	partition_close(partition);
	cardPresence = 0;
}
