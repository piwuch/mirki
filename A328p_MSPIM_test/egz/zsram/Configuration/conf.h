/*
 * conf.h
 *
 *  Created on: 2010-03-01
 *      Author: damian
 */

#ifndef CONF_H_
#define CONF_H_

extern uint32_t ipAddr;
extern uint32_t netMaskAddr;
extern uint32_t netGateAddr;
extern uint32_t dnsAddr;
extern uint8_t macAddr[6];

extern uint8_t confRead(void);
extern uint8_t confSave(void);
extern void confDefault(void);
extern void confPrint(void);

#endif /* CONF_H_ */
