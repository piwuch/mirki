/*
 * conf.c
 *
 *  Created on: 2010-03-01
 *      Author: damian
 */

#include <stdlib.h>
#include "../Devices/stdout.h"
#include "conf.h"
#include "../FAT/fat.h"
#include "../Devices/partition.h"
#include "../Drivers/sd_raw.h"
#include "../Interfaces/rs232.h"

uint32_t ipAddr = 0;
uint32_t netMaskAddr = 0;
uint32_t netGateAddr = 0;
uint32_t dnsAddr = 0;
uint8_t macAddr[6];

void confPrint(void) {
	printf_P(PSTR("IP: %u.%u.%u.%u\n"), ((uint8_t*) &(ipAddr))[0],
			((uint8_t*) &(ipAddr))[1], ((uint8_t*) &(ipAddr))[2],
			((uint8_t*) &(ipAddr))[3]);
	printf_P(PSTR("MASK: %u.%u.%u.%u\n"), ((uint8_t*) &(netMaskAddr))[0],
			((uint8_t*) &(netMaskAddr))[1], ((uint8_t*) &(netMaskAddr))[2],
			((uint8_t*) &(netMaskAddr))[3]);
	printf_P(PSTR("GATE: %u.%u.%u.%u\n"), ((uint8_t*) &(netGateAddr))[0],
			((uint8_t*) &(netGateAddr))[1], ((uint8_t*) &(netGateAddr))[2],
			((uint8_t*) &(netGateAddr))[3]);
	printf_P(PSTR("DNS: %u.%u.%u.%u\n"), ((uint8_t*) &(dnsAddr))[0],
			((uint8_t*) &(dnsAddr))[1], ((uint8_t*) &(dnsAddr))[2],
			((uint8_t*) &(dnsAddr))[3]);
	printf_P(PSTR("MAC: %X:%X:%X:%X:%X:%X\n"), ((uint8_t*) &(macAddr))[0],
			((uint8_t*) &(macAddr))[1], ((uint8_t*) &(macAddr))[2],
			((uint8_t*) &(macAddr))[3], ((uint8_t*) &(macAddr))[4],
			((uint8_t*) &(macAddr))[5]);
}

uint8_t confRead(void) {

	if (!cardPresence) {
#ifdef DEBUG
		printf_P(PSTR("Error reading configuration file!\n"));
#endif
		return 0;
	}

	/* open root directory */
	struct fat_dir_entry_struct directory;
	fat_get_dir_entry_of_path(fs, "/", &directory);
	dd = fat_open_dir(fs, &directory);
	if (!dd) {
#ifdef DEBUG
		printf_P(PSTR("Opening root directory failed\n"));
#endif
		return 0;
	}

	/* search file in current directory and open it */
	struct fat_file_struct* fd = open_file_in_dir(fs, dd, "conf.sys");
	if (!fd) {
#ifdef DEBUG
		printf_P(PSTR("Error opening configuration file!\n"));
#endif
		return 0;
	}

	/* read file contents */
	uint8_t buffer[4];
	// IP
	fat_read_file(fd, buffer, sizeof(buffer));
	ipAddr = *(uint32_t*) &buffer[0];
	// NETMASK
	fat_read_file(fd, buffer, sizeof(buffer));
	netMaskAddr = *(uint32_t*) &buffer[0];
	// NETGATE
	fat_read_file(fd, buffer, sizeof(buffer));
	netGateAddr = *(uint32_t*) &buffer[0];
	// DNS
	fat_read_file(fd, buffer, sizeof(buffer));
	dnsAddr = *(uint32_t*) &buffer[0];
	// MAC
	fat_read_file(fd, buffer, sizeof(buffer));
	*(uint32_t*)(&macAddr[0]) = *(uint32_t*)(&buffer[0]);
	fat_read_file(fd, buffer, sizeof(buffer));
	*(uint16_t*)(&macAddr[4]) = *(uint16_t*)(&buffer[0]);
	fat_close_file(fd);
	fat_close_dir(dd);

	return 1;
}

void confDefault(void) {
	// IP
	ipAddr = 0x0A00000A;
	// NETMASK
	netMaskAddr = 0x00FFFFFF;
	// NETGATE
	netGateAddr = 0x0100000A;
	// DNS
	dnsAddr = 0x0100000A;
	// MAC
	macAddr[0] = 'D';
	macAddr[1] = 'A';
	macAddr[2] = 'M';
	macAddr[3] = 'I';
	macAddr[4] = 'A';
	macAddr[5] = 'N';
}

#if SD_RAW_WRITE_SUPPORT
uint8_t confSave(void) {

	if (!cardPresence) {
#ifdef DEBUG
		printf_P(PSTR("Error writeing configuration file!\n"));
#endif
		return 0;
	}

	/* open root directory */
	struct fat_dir_entry_struct directory;
	fat_get_dir_entry_of_path(fs, "/", &directory);
	dd = fat_open_dir(fs, &directory);
	if (!dd) {
#ifdef DEBUG
		printf_P(PSTR("Opening root directory failed\n"));
#endif
		return 0;
	}

	struct fat_dir_entry_struct file_entry;
	if (find_file_in_dir(fs, dd, "conf.sys", &file_entry)) {
		fat_delete_file(fs, &file_entry);
	}

	if (!fat_create_file(dd, "conf.sys", &file_entry)) {
#ifdef DEBUG
		printf_P(PSTR("Error creating configuration file!\n"));
#endif
	}

	/* search file in current directory and open it */
	struct fat_file_struct* fd = open_file_in_dir(fs, dd, "conf.sys");
	if (!fd) {
#ifdef DEBUG
		printf_P(PSTR("Error opening configuration file!\n"));
#endif
		return 0;
	}

	fat_seek_file(fd, 0, FAT_SEEK_SET);

	/* write text to file */
	if (fat_write_file(fd, (uint8_t*) &ipAddr, 4) != 4) {
#ifdef DEBUG
		printf_P(PSTR("Error writing to configuration file\n"));
#endif
		return 0;
	}
	if (fat_write_file(fd, (uint8_t*) &netMaskAddr, 4) != 4) {
#ifdef DEBUG
		printf_P(PSTR("Error writing to configuration file\n"));
#endif
		return 0;
	}
	if (fat_write_file(fd, (uint8_t*) &netGateAddr, 4) != 4) {
#ifdef DEBUG
		printf_P(PSTR("Error writing to configuration file\n"));
#endif
		return 0;
	}
	if (fat_write_file(fd, (uint8_t*) &dnsAddr, 4) != 4) {
#ifdef DEBUG
		printf_P(PSTR("Error writing to configuration file\n"));
#endif
		return 0;
	}
	if (fat_write_file(fd, macAddr, 6) != 6) {
#ifdef DEBUG
		printf_P(PSTR("Error writing to configuration file\n"));
#endif
		return 0;
	}

	fat_close_file(fd);
	sd_raw_sync();
	fat_close_dir(dd);

#ifdef DEBUG
	printf_P(PSTR("Configuration succesfully wrote!\n"));
#endif

	return 1;
}
#endif
