
#ifndef SDCARD_H
#define SDCARD_H

/*------------------------------------
*	globale Variable
*/
extern struct fat16_fs_struct* g_fs;			// globaler Zeiger auf FAT16 Struktur der SD-Karte
extern const char cwdirectory[MAX_PATH+1];	// aktuelles Arbeitsverzeichnis


/*------------------------------------
*	typedefs
*/

typedef struct fat16_file_struct
{
    struct fat16_fs_struct* fs;
    struct fat16_dir_entry_struct dir_entry;
    uint32_t pos;
    uint16_t pos_cluster;
	char	mode;		// file open mode
} File;

/*------------------------------------
*	Prototypen
*/
bool 	f16_init(void);
File* 	f16_open(const char *filename, const char *mode);
void	f16_close(File *stream);
int 	f16_fseek(File *stream, int32_t offset, uint8_t whence);
int 	f16_fputc(char c, File *stream);
int 	f16_fputs(char *s, File *stream);
int 	f16_fputs_P(const char *s, File *stream);
int 	f16_getc(File *stream);
int 	f16_printf_P(File *fp, const char *format,...);
uint8_t f16_exist(const char *);
uint8_t f16_delete(const char *);
uint8_t f16_mkdir(const char *);
uint8_t find_file_in_dir(struct fat16_fs_struct*, struct fat16_dir_struct*, const char*, struct fat16_dir_entry_struct*);
uint8_t f16_mkdir(const char *);

#endif
