/*----------------------------------------------------------------------------
 Copyright:      W.Wallucks
 known Problems: ?
 Version:        01.04.2008
 Description:    SD-Kartenanbindung an AVR-Miniwebserver

 Dieses Programm ist freie Software. Sie k�nnen es unter den Bedingungen der 
 GNU General Public License, wie von der Free Software Foundation ver�ffentlicht, 
 weitergeben und/oder modifizieren, entweder gem�� Version 2 der Lizenz oder 
 (nach Ihrer Option) jeder sp�teren Version. 

 Die Ver�ffentlichung dieses Programms erfolgt in der Hoffnung, 
 da� es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, 
 sogar ohne die implizite Garantie der MARKTREIFE oder der VERWENDBARKEIT 
 F�R EINEN BESTIMMTEN ZWECK. Details finden Sie in der GNU General Public License. 

 Sie sollten eine Kopie der GNU General Public License zusammen mit diesem 
 Programm erhalten haben. 
 Falls nicht, schreiben Sie an die Free Software Foundation, 
 Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 
----------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <ctype.h>
#include <avr/pgmspace.h>

#include "../config.h"
#include "partition.h"
#include "fat16.h"
#include "fat16_config.h"
#include "sd_raw.h"
#include "sd-reader_config.h"
#include "sdcard.h"
#include "../timer.h"


//#include "../usart.h"
//#define SD_DEBUG	usart_write 
#define SD_DEBUG(...)	


struct fat16_fs_struct* g_fs;					// globaler Zeiger auf FAT16 Struktur der SD-Karte
const char cwdirectory[MAX_PATH+1] = "/";	// aktuelles Arbeitsverzeichnis

extern struct partition_struct partition_handles[1];

/*
*	get_datetime
*/
void get_datetime(uint16_t* year, uint8_t* month, uint8_t* day, uint8_t* hour, uint8_t* min, uint8_t* sec)
{
	*year 	= TM_YY + 2000;
	*month 	= TM_MM;
	*day 	= TM_DD;

	*hour	= TM_hh;
	*min	= TM_mm;
	*sec	= TM_ss;
}

/*
* SD-Karte initialisieren und FAT16 initialisieren
* gibt true zur�ck, falls Karte vorhanden und initialisiert
*/
bool f16_init()
{
	if (sd_raw_init()) //ist der R�ckgabewert ungleich 0 ist ein Fehler aufgetreten
		{
		SD_DEBUG("\r\n** Keine MMC/SD Karte gefunden!! %i **\n");	
		return false;
		}
	else
		{
		SD_DEBUG("\r\nSD-Karte gefunden!!");

		partition_close(partition_handles);
	    /* open first partition */
	    struct partition_struct* partition = partition_open(sd_raw_read,
	                                                        sd_raw_read_interval,
	                                                        sd_raw_write,
	                                                        sd_raw_write_interval,
	                                                        0
	                                                       );

	    if(!partition)
	    {
	        /* If the partition did not open, assume the storage device
	         * is a "superfloppy", i.e. has no MBR.
	         */
	        partition = partition_open(sd_raw_read,
	                                   sd_raw_read_interval,
	                                   sd_raw_write,
	                                   sd_raw_write_interval,
	                                   -1
	                                  );
	        if(!partition)
	        {
	            SD_DEBUG("\r\nFAT16: opening partition failed");
				return false;
	        }
	    }

    	/* open file system (tr�gt den Partitionszeiger partition in die Struktur fs ein) */
		fat16_close(g_fs);
    	g_fs = fat16_open(partition);
	    if(!g_fs)
	    {
			partition_close(partition);
	        SD_DEBUG("\r\nFAT16: opening filesystem failed");
			return false;
	    }

    	/* open root directory */
	    struct fat16_dir_entry_struct directory;
	    fat16_get_dir_entry_of_path(g_fs, "/", &directory);

	    struct fat16_dir_struct* dd = fat16_open_dir(g_fs, &directory);
	    if(!dd)
	    {
	        SD_DEBUG("\r\nopening root directory failed");
			return false;
	    }
	
		fat16_close_dir(dd);
		return true;
	}
}


File* f16_open(const char *filename, const char *mode)
{
	if (!g_fs)
		return 0;

    /* open root directory */
    struct fat16_dir_entry_struct directory;
    fat16_get_dir_entry_of_path(g_fs, cwdirectory, &directory);

    struct fat16_dir_struct* dd = fat16_open_dir(g_fs, &directory);

    struct fat16_dir_entry_struct file_entry;

    while(fat16_read_dir(dd, &file_entry))
    {
		SD_DEBUG("\r\nFAT16: comparing +%s+ -- +%s+",file_entry.long_name, filename);
        if(strcasecmp(file_entry.long_name, filename) == 0)
        {
            fat16_reset_dir(dd);

			File *newfile = fat16_open_file(g_fs, &file_entry);
			newfile->mode = *mode;
			if (*mode == 'a') {
				int32_t offset = 0;
				fat16_seek_file(newfile, &offset, FAT16_SEEK_END);
				SD_DEBUG("\r\nDatei %s geoeffnet %l Bytes",filename,offset);
			}
			fat16_close_dir(dd);
			return newfile;
        }
		SD_DEBUG(" << not equal");
    }

	// kein Dateieintrag gefunden -> bei mode == "a" oder "w" neuen erstellen
	if (*mode == 'a' || *mode == 'w') {
    	if(fat16_create_file(dd, filename, &file_entry)) {
			File *newfile = fat16_open_file(g_fs, &file_entry);
			newfile->mode = *mode;
			fat16_close_dir(dd);
			return newfile;
		}
	}

	fat16_close_dir(dd);
    return 0;
}

void f16_close(File *stream)
{
	sd_raw_sync();
	fat16_close_file(stream);
}

int f16_fseek(File *stream, int32_t offset, uint8_t whence)
{
	int32_t off = offset;
	return (int)fat16_seek_file(stream, &off, whence);
}

int f16_fputc(char c, File *stream)
{
	return fat16_write_file(stream, (uint8_t *)&c, 1);
}

int f16_fputs(char *s, File *stream)
{
	int i;

	for (i=0;;++i) {
		if (*s++ == 0)
			break;
	}

	return fat16_write_file(stream, (uint8_t *)s, i);
}

int f16_fputs_P(const char *s, File *stream)
{
	int i;
	char c;
	uint8_t *ptr = (uint8_t *)s;

	for (i=0;;++i) {		// Stringl�nge ermitteln
		c = pgm_read_byte(ptr++);
		if (c == 0)
			break;
	}

	ptr = malloc(i);
	memcpy_P(ptr,s,i);

	i = fat16_write_file(stream, ptr, i);

	free(ptr);
	return i;
}


int f16_getc(File *stream)
{
	unsigned char c;
	int i;

	i = fat16_read_file(stream, &c, 1);	// gibt Anzahl gelesener Zeichen zur�ck oder 0/-1 bei EOF/Fehler
	if (i <= 0) {
		return -1;		// EOF
	}
	else {
		return (int)c;
	}
}

int f16_printf_P(File *stream,const char *format,...)
{
	va_list ap;
	va_start (ap, format);	

	int format_flag;
	char str_buffer[10];
	char str_null_buffer[10];
	char move = 0;
	char Base = 0;
	int tmp = 0;
	unsigned char by;
	unsigned char *ptr;
	unsigned char outbuffer[64];
	uint8_t outpos = 0;
		
	//Ausgabe der Zeichen
    for(;;)
	{
		by = pgm_read_byte(format++);
		if(by==0) break; // end of format string
            
		if (by == '%')
		{
            by = pgm_read_byte(format++);
			if (isdigit(by)>0)
				{
                                 
 				str_null_buffer[0] = by;
				str_null_buffer[1] = '\0';
				move = atoi(str_null_buffer);
                by = pgm_read_byte(format++);
				}

			switch (by)
				{
                case 's':
                    ptr = va_arg(ap,unsigned char *);
                    while(*ptr) {
						if (outpos >= 64) {
							fat16_write_file(stream, outbuffer, outpos);
							outpos=0;
						}
						outbuffer[outpos++] = *ptr++;
					}
                    break;
				case 'b':
					Base = 2;
					goto ConversionLoop;
				case 'c':
					//Int to char
					format_flag = va_arg(ap,int);
					outbuffer[outpos++] = format_flag++;
					break;
				case 'l':
					ltoa(va_arg(ap,int32_t),str_buffer,10);
					goto CvLoop2;
				case 'u':
					utoa(va_arg(ap,uint16_t),str_buffer,10);
					goto CvLoop2;
				case 'i':
					Base = 10;
					goto ConversionLoop;
				case 'o':
					Base = 8;
					goto ConversionLoop;
				case 'x':
					Base = 16;
					//****************************
					ConversionLoop:
					//****************************
					itoa(va_arg(ap,int),str_buffer,Base);

					CvLoop2:;
					int b=0;
					while (str_buffer[b++] != 0){};
					b--;
					if (b<move)
						{
						move -=b;
						for (tmp = 0;tmp<move;tmp++)
							{
							str_null_buffer[tmp] = '0';
							}
						//tmp ++;
						str_null_buffer[tmp] = '\0';
						strcat(str_null_buffer,str_buffer);
						strcpy(str_buffer,str_null_buffer);
						}
					//usart_write_str (str_buffer);
					// Puffer leeren, falls wenig Platz
					if (outpos >= 54) {
						fat16_write_file(stream, outbuffer, outpos);
						outpos=0;
						}

					b=0;
					while (str_buffer[b] != 0) {
						outbuffer[outpos++] = str_buffer[b++];
					}
					move = 0;
					break;
				}
			
			}	
		else
		{
			outbuffer[outpos++] = by;	
		}

		// Puffer leeren
		if (outpos >= 64) {
			fat16_write_file(stream, outbuffer, 64);
			outpos=0;
		}
	}

	fat16_write_file(stream, outbuffer, outpos);	// letzten Rest rausschreiben
	va_end(ap);
	return 0;
}

uint8_t f16_exist(const char *filename)
{
	uint8_t iret = 0;

	if (!g_fs)
		return 0;

    /* open current directory */
    struct fat16_dir_entry_struct directory;
    fat16_get_dir_entry_of_path(g_fs, cwdirectory, &directory);

    struct fat16_dir_struct* dd = fat16_open_dir(g_fs, &directory);

    struct fat16_dir_entry_struct file_entry;

    if(find_file_in_dir(g_fs, dd, filename, &file_entry)) {
		iret = 1;
	}

	fat16_close_dir(dd);
	return iret;
}

uint8_t f16_delete(const char *filename)
{
	uint8_t iret = 0;
	if (!g_fs)
		return iret;

    /* open current directory */
    struct fat16_dir_entry_struct directory;
    fat16_get_dir_entry_of_path(g_fs, cwdirectory, &directory);

    struct fat16_dir_struct* dd = fat16_open_dir(g_fs, &directory);

    struct fat16_dir_entry_struct file_entry;

    if(find_file_in_dir(g_fs, dd, filename, &file_entry)) {
        if(fat16_delete_file(g_fs, &file_entry)) {
			iret = 1;
		}
    }
	fat16_close_dir(dd);
	return iret;
}

uint8_t find_file_in_dir(struct fat16_fs_struct* fs, struct fat16_dir_struct* dd, const char* name, struct fat16_dir_entry_struct* dir_entry)
{
    while(fat16_read_dir(dd, dir_entry)) {

        if(strcasecmp(dir_entry->long_name, name) == 0) {
            fat16_reset_dir(dd);
            return 1;
        }
    }

    return 0;
}

uint8_t f16_mkdir(const char *dirname)
{
	uint8_t iret = 0;

	if (!g_fs)
		return iret;

    /* open current directory */
    struct fat16_dir_entry_struct directory;
    fat16_get_dir_entry_of_path(g_fs, cwdirectory, &directory);

    struct fat16_dir_struct* dd = fat16_open_dir(g_fs, &directory);
	struct fat16_dir_entry_struct dir_entry;

	iret = fat16_create_dir(dd, dirname, &dir_entry);

	fat16_close_dir(dd);
	return iret;
}
