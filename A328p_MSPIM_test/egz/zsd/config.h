/*----------------------------------------------------------------------------
 Copyright:      Radig Ulrich  mailto: mail@ulrichradig.de
 Author:         Radig Ulrich
 Remarks:        
 known Problems: none
 Version:        03.11.2007
 Description:    Webserver Config-File

 Dieses Programm ist freie Software. Sie k�nnen es unter den Bedingungen der 
 GNU General Public License, wie von der Free Software Foundation ver�ffentlicht, 
 weitergeben und/oder modifizieren, entweder gem�� Version 2 der Lizenz oder 
 (nach Ihrer Option) jeder sp�teren Version. 

 Die Ver�ffentlichung dieses Programms erfolgt in der Hoffnung, 
 da� es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, 
 sogar ohne die implizite Garantie der MARKTREIFE oder der VERWENDBARKEIT 
 F�R EINEN BESTIMMTEN ZWECK. Details finden Sie in der GNU General Public License. 

 Sie sollten eine Kopie der GNU General Public License zusammen mit diesem 
 Programm erhalten haben. 
 Falls nicht, schreiben Sie an die Free Software Foundation, 
 Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 
------------------------------------------------------------------------------*/

#ifndef _CONFIG_H_
	#define _CONFIG_H_	
	
	//ETH_M32_EX (www.ulrichradig.de)
	#define USE_ENC28J60	1
	//Holger Buss (www.mikrocontroller.com) Mega32-Board
	#define USE_RTL8019		0
	
	//Konfiguration der PORTS (HEX)
	//1=OUTPUT / 0=INPUT
	#define OUTA 		0x07
	#define OUTC 		0x00
	#define OUTD 		0x00
	//Achtung!!!! an PORTB ist der ENC
	//nur �ndern wenn man wei� was man macht!


	//Umrechnung von IP zu unsigned long
	#define IP(a,b,c,d) ((unsigned long)(d)<<24)+((unsigned long)(c)<<16)+((unsigned long)(b)<<8)+a

	//IP des Webservers
	#define MYIP		IP(192,168,0,99)

	//Netzwerkmaske
	#define NETMASK		IP(255,255,255,0)
	
	//IP des Routers
	#define ROUTER_IP	IP(192,168,0,1)
	
	//IP des NTP-Servers z.B. Server 1.de.pool.ntp.org
	#define USE_NTP		1 //1 = NTP Client on
	#define NTP_IP		IP(77,37,6,59)
	
	//Broadcast-Adresse f�r WOL
	#define USE_WOL			0 //1 = WOL on
	#define WOL_BCAST_IP	IP(192,168,0,255)
	#define WOL_MAC 		{0x00,0x1A,0xA0,0x9C,0xC6,0x0A}
	
	//MAC Adresse des Webservers	
	#define MYMAC1	0x00
	#define MYMAC2	0x20
	#define MYMAC3	0x18
	#define MYMAC4	0xB1	
	#define MYMAC5	0x15
	#define MYMAC6	0x6F
	
	//Taktfrequenz
	//#define F_CPU 16000000UL	
	#define F_CPU 14745600UL
	//#define F_CPU 11059200UL
	
	//Timertakt intern oder extern
	#define EXTCLOCK 0 //0=Intern 1=Externer Uhrenquarz

/** USART *************/
	#define BAUDRATE 38400			// Baudrate der seriellen Schnittstelle
	#define USART_USE1	0			// 0 f�r USART0 und 1 f�r USART1 bei ATmega644P
									// Kamera ist dann an der anderen Schnittstelle
	
/** LCD ***************/
	#define USE_SER_LCD		0		//LCD Routinen mit einbinden
	#define USE_LCD_4Bit	0		//LCD im 4Bit Mode oder seriell

/** ADC ***************/
	#define USE_ADC			1		//AD-Wandler benutzen?

/** 1-Wire ************/
	#define USE_OW			1		// 1-Wire bus einbinden
	#define MAXSENSORS		8		// max. Anzahl der 1-wire sensoren (DS18B20)
	#define OW_ONE_BUS		1		// nur ein fest eingestellter 1-wire bus; keine Auswahl
									// die Ports werden in onewire.h definiert
	#define OW_ROMCODE_SIZE 8		// rom-code DS18B20 size including CRC

	// hier die ausgelesenen ROM-IDs der DS18B20 eintragen
	#define OW_ID_T01		{0x28,0xd7,0x00,0x32,0x01,0,0,0xbb}
	#define OW_ID_T02		{0x28,0xc9,0x1f,0x32,0x01,0,0,0xe0}

	#define OW_ID_T03		{0x28,0x11,0x22,0x33,0x44,0,0,0x77}
	#define OW_ID_T04		{0x28,0x22,0x33,0x44,0x55,0,0,0x77}
	#define OW_ID_Last		{0x00,0x00,0x00,0x00,0x00,0,0,0x00}

/** Kamera ************/
	//Kamera mit einbinden
	//Kamera arbeitet nur mit einem 14,7456Mhz Quarz!
	#define USE_CAM			0
	#define USE_SERVO		0
	//In cam.c k�nnen weitere Parameter eingestellt werde
	//z.B. Licht, Kompression usw.
	//Aufl�sungen
	//0 = 160x120 Pixel k�rzer (zum testen OK ;-)
	//1 = 320x240 Pixel ca. 10 Sek. bei einem Mega644
	//2 = 640x480 Pixel l�nger (dauert zu lang!)
	#define CAM_RESELUTION	0

/** SD-Karte **********/
	#define USE_MMC			1		// mit SD-Karte
	#define MAX_PATH		63		// maximale Pfadl�nge

/** TCP-Service *******/
	#define TCP_SERVICE		1		// mit TCP-Service (FTP, Telnet-Cmdline)
	#define MYTCP_PORT		61234	// Port# f�r Telnet-Cmd Interpreter	
	#define DOS_LIST		1		// DOS style Directory-Listing
	//#define UNIX_LIST		1
	#define FTP_ANONYMOUS	0		// anomymen Login (ohne User/Kennwort) erlauben
	#define FTP_USER		"chef"	// User, falls nicht anonym
	#define FTP_PASSWORD	"123"

	// don't touch!
	#if TCP_SERVICE
		#define _CMD_H_
	#else
		#define _TCPCMD_H_
	#endif
	#ifdef DOS_LIST
		#undef UNIX_LIST
	#endif

/** Passwort **********/
	#define HTTP_AUTH_DEFAULT	1	//Webserver mit Passwort? (0 == mit Passwort)
	
	//AUTH String "USERNAME:PASSWORT" max 14Zeichen
	//f�r Username:Passwort
	#define HTTP_AUTH_STRING "admin:uli1"
	
/** E-Mail ************/
    //Email vesand benutzen? Konfiguration des
    //Emailclient in der Sendmail.h
    #define USE_MAIL        0

/** Webserver abfragen **/
    //Empfang von Wetterdaten auf der Console (�ber HTTP_GET)
    #define GET_WEATHER     0

    
/** Scheduler *********/
    #define USE_SCHEDULER		1
	#define	TM_MAX_SCHALTZEITEN	32		// Anzahl unterschiedlicher Schaltzeiten
	#define TIME_TEMP			60		// jede Minute (60 sec) Temperaturen messen
	#define USE_LOGDATEI		1		// Logdatei schreiben


/*------------------------------------
*	typedefs
*/
#ifndef __STDINT_H_
	#include <stdint.h>
#endif

typedef enum { false, true } bool;

typedef struct
{
	volatile bool		relais1;			// Relais an/aus
	volatile bool		relais2;
	volatile bool		relais3;
	volatile uint8_t 	TReglerWert;
	volatile uint16_t 	Zaehler1;			// Z�hlerwerte
	volatile uint16_t 	Zaehler2;
	volatile uint16_t 	Zaehler3;
} ANLAGEN_STATUS;

typedef struct
{
	volatile uint8_t	Schalter1	: 1;	// Sollzustand an/aus
	volatile uint8_t	Schalter2	: 1;
	volatile uint8_t	Schalter3	: 1;
	volatile uint8_t 	TReglerWert;
} SOLL_STATUS;

typedef struct 
{
	volatile uint8_t		Uhrzeit;
	volatile uint8_t 		Wochentag;		// pro Tag ein bit
	volatile SOLL_STATUS	Zustand;		// Sollzustand der Schalter
} TM_Aktion;

/*
*	Prozessstatus und aktuelle Werte f�r die Endlosschleife in main()
*/
typedef struct
{
	volatile bool 			timeChanged;	// Zeitmessung ge�ndert
	volatile bool 			initialisieren;	// Maschine muss initialisiert werden
	volatile bool 			Tlesen;			// Temperaturen m�ssen gelesen werden
	volatile bool 			regeln;			// Werte einstellen
	volatile bool 			LogInit;		// SD-Karte mit Logdatei neu initialisieren
	volatile bool 			LogSchreiben;	// Werte m�ssen geloggt werden
	volatile bool			ADCgemessen;	// analoger Wert am ADC wurde gemessen
	volatile unsigned char	PINCchanged;	// Eingang wurde ge�ndert
	volatile unsigned char	PINAStatus;		// letzter Wert f�r PINA
	volatile unsigned char	PINCStatus;		// letzter Wert f�r PINC
	volatile unsigned char	PINDStatus;		// letzter Wert f�r PIND
} PROZESS_STATUS;

/*
*	Status der Logdatei
*/
typedef struct
{
	volatile bool		SDpresent;		// SD-Karte mit Datei vorhanden
	const char			log_datei[13];
	volatile uint8_t	logTag;			// Tag der Logdatei
	volatile uint8_t	logMonat;		// Monat der Logdatei
	volatile uint8_t	logJahr;		// Jahr der Logdatei
} LOG_STATUS;


/*------------------------------------
*	globale Variable
*/
	extern volatile ANLAGEN_STATUS	anlagenStatus;
	extern volatile PROZESS_STATUS 	machineStatus;
	extern 			LOG_STATUS		logStatus;
	extern volatile SOLL_STATUS 	anlagenSollStatus;
    
#endif //_CONFIG_H


