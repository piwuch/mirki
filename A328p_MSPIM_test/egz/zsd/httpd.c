/*----------------------------------------------------------------------------
 Copyright:      Radig Ulrich  mailto: mail@ulrichradig.de
 Author:         Radig Ulrich
 Remarks:        
 known Problems: none
 Version:        24.10.2007
 Description:    Webserver Applikation

 Dieses Programm ist freie Software. Sie k�nnen es unter den Bedingungen der 
 GNU General Public License, wie von der Free Software Foundation ver�ffentlicht, 
 weitergeben und/oder modifizieren, entweder gem�� Version 2 der Lizenz oder 
 (nach Ihrer Option) jeder sp�teren Version. 

 Die Ver�ffentlichung dieses Programms erfolgt in der Hoffnung, 
 da� es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, 
 sogar ohne die implizite Garantie der MARKTREIFE oder der VERWENDBARKEIT 
 F�R EINEN BESTIMMTEN ZWECK. Details finden Sie in der GNU General Public License. 

 Sie sollten eine Kopie der GNU General Public License zusammen mit diesem 
 Programm erhalten haben. 
 Falls nicht, schreiben Sie an die Free Software Foundation, 
 Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 
------------------------------------------------------------------------------*/
#include <stdio.h>
#include "config.h"
#include "httpd.h"
#include "webpage.h"
#include "sdkarte/fat16.h"
#include "sdkarte/sdcard.h"

#if USE_OW
#include "messung.h"
#endif

struct http_table http_entry[MAX_TCP_ENTRY];

//Hier wird das codierte Passwort aus config.h gespeichert.
unsigned char http_auth_passwort[20];

unsigned char post_in[5] = {'O','U','T','='};
unsigned char post_ready[5] = {'S','U','B','='};
unsigned char PORT_tmp = 0;

//----------------------------------------------------------------------------
//Variablenarry zum einf�gen in Webseite %VA@00 bis %VA@09
unsigned int var_array[MAX_VAR_ARRAY] = {10,50,30,0,0,0,0,0,0,0};
//----------------------------------------------------------------------------

PROGMEM char http_header1[]={	"HTTP/1.0 200 Document follows\r\n"
								"Server: AVR_Small_Webserver\r\n"
								"Content-Type: text/html\r\n\r\n"};

PROGMEM char http_header2[]={	"HTTP/1.0 200 Document follows\r\n"
								"Server: AVR_Small_Webserver\r\n"
								"Content-Type: image/jpg\r\n\r\n"};

PROGMEM char http_header3[]={	"HTTP/1.0 401 Unauthorized\r\n"
								"Server: AVR_Small_Webserver\r\n"
								"WWW-Authenticate: Basic realm=\"NeedPassword\""
								"\r\nContent-Type: text/html\r\n\r\n"};

PROGMEM char http_csv_header[]={	"HTTP/1.1 200 OK\r\n"
									"Server: AVR_Small_Webserver\r\n"
									"Content-Disposition: attachment;filename=Datalog.csv\r\n"
									"Connection: close\r\n"
									"Content-Type: file\r\n\r\n"};
									// oder application/octet-stream
#define HTTP_CSV_HEADER_LEN 141
//----------------------------------------------------------------------------
//Kein Zugriff Seite bei keinem Passwort
PROGMEM char Page0[] = {"401 Unauthorized%END"};

unsigned char rx_header_end[5] = {"\r\n\r\n\0"};

//----------------------------------------------------------------------------
//Initialisierung des Httpd Testservers
void httpd_init (void)
{
	//HTTP_AUTH_STRING 
	decode_base64((unsigned char*)HTTP_AUTH_STRING,http_auth_passwort);

	//Serverport und Anwendung eintragen
	add_tcp_app (HTTPD_PORT, (void(*)(unsigned char))httpd);
}
   
//----------------------------------------------------------------------------
//http Testserver
void httpd (unsigned char index)
{
	if (tcp_entry[index].status & FIN_FLAG) {	// Wird Verbindungsabbau signalisiert?
		HTTP_DEBUG("\r\nHTTP %i closed.",index);
		return;
	}

	//Allererste Aufruf des Ports f�r diese Anwendung
	//HTTPD_Anwendungsstack l�schen
	if(tcp_entry[index].app_status==1)
	{
		httpd_stack_clear(index);
	}
	
	//HTTP wurde bei dieser Verbindung zum ersten mal aufgerufen oder
	//HTML Header Retransmission!
	if (tcp_entry[index].app_status <= 2)
	{	
		httpd_header_check (index);
		return;
	}
	
	//Der Header wurde gesendet und mit ACK best�tigt (tcp_entry[index].app_status+1)
	//war das HTML Packet fertig, oder m�ssen weitere Daten gesendet werden, oder Retransmission?
	if (tcp_entry[index].app_status > 2 && tcp_entry[index].app_status < 0xFFFE)
	{
		httpd_data_send (index);
		return;
	}
	
	//Verbindung kann geschlossen werden! Alle HTML Daten wurden gesendet TCP Port kann
	//geschlossen werden (tcp_entry[index].app_status >= 0xFFFE)!!
	if (tcp_entry[index].app_status >= 0xFFFE)
	{
		tcp_entry[index].app_status = 0xFFFE;
		tcp_Port_close(index);
		return;
	}
	return;
}

//----------------------------------------------------------------------------
//HTTPD_STACK l�schen
void httpd_stack_clear (unsigned char index)
{
	http_entry[index].http_header_type =0;
	http_entry[index].first_switch = 0;
	http_entry[index].http_auth = HTTP_AUTH_DEFAULT;
	http_entry[index].new_page_pointer = 0;
	http_entry[index].old_page_pointer = 0;
	http_entry[index].post = 0;
	http_entry[index].auth_ptr = http_auth_passwort;
	http_entry[index].post_ptr = post_in;
	http_entry[index].post_ready_ptr = post_ready;
	http_entry[index].hdr_end_pointer = rx_header_end;
	#if USE_CAM
	http_entry[index].cam = 0;
	#endif //USE_CAM				
	#if USE_MMC
	http_entry[index].mmc = 0;
	http_entry[index].charcount = 0;
	http_entry[index].old_charcount = 0;
	#endif //USE_MMC			
	HTTP_DEBUG("\r\n**** NEUE HTTP ANFORDERUNG ****\r\n\r\n");	
	return;
}

//----------------------------------------------------------------------------
//Eintreffenden Header vom Client checken
void httpd_header_check (unsigned char index)
{
	unsigned int a = 0;
	
	if(strcasestr_P((char*)&eth_buffer[TCP_DATA_START_VAR],PSTR("POST"))!=0)
		{
		http_entry[index].post = 1;
		}
	
	//finden der Authorization und das Ende im Header auch �ber mehrere Packete hinweg!!	
	if(*http_entry[index].hdr_end_pointer != 0)
	{		
		for(a=TCP_DATA_START_VAR;a<(TCP_DATA_END_VAR);a++)
		{	
			HTTP_DEBUG("%c",eth_buffer[a]);
			
			if(!http_entry[index].http_auth) 
			{
				if (eth_buffer[a] != *http_entry[index].auth_ptr++)
				{
					http_entry[index].auth_ptr = http_auth_passwort;
				}
				if(*http_entry[index].auth_ptr == 0) 
				{
					http_entry[index].http_auth = 1;
					HTTP_DEBUG("  <---LOGIN OK!--->\r\n");
				}
			}
			
			if (eth_buffer[a] != *http_entry[index].hdr_end_pointer++)
			{
				http_entry[index].hdr_end_pointer = rx_header_end;
			}
			
			//Das Headerende wird mit (CR+LF+CR+LF) angezeigt!
			if(*http_entry[index].hdr_end_pointer == 0) 
			{
				HTTP_DEBUG("<---HEADER ENDE ERREICHT!--->\r\n");
				break;
			}
		}
	}
	
	//Einzelne Postpacket (z.B. bei firefox)
	if(http_entry[index].http_auth && http_entry[index].post == 1)
	{
		for(a = TCP_DATA_START_VAR;a<(TCP_DATA_END_VAR);a++)
		{	
			//Schaltanweisung finden!
			if (eth_buffer[a] != *http_entry[index].post_ptr++)
			{
				http_entry[index].post_ptr = post_in;
			}
			if(*http_entry[index].post_ptr == 0) 
			{
				switch (eth_buffer[a+1])
				  {
				    case ('A'):
					  PORT_tmp = PORT_tmp + 1;
				      break;
				
				    case ('B'):
					  PORT_tmp = PORT_tmp + 2;
				      break;
				
				    case ('C'):
					  PORT_tmp = PORT_tmp + 4;
				      break;

                    #if USE_WOL
                    case 'W':
                        wol_enable = 1;
						break;
                    #endif //USE_WOL

					/*
					** Hier k�nnen weitere Schaltanweisungen eingef�gt werden.
					**
					** Das sendende Formularfeld muss den Namen "OUT" haben und
					** ein einzelnes Zeichen als Wert, der gesetzt werden kann.
					** Zum zur�cksetzen muss ein weiteres Zeichen verwendet werden.
					**
					** Bereits verwendete Zeichen siehe oben bei den case-Statements.
					*/

				  }
				http_entry[index].post_ptr = post_in;
				//Schaltanweisung wurde gefunden
			}
		
			//Submit schlie�t die suche ab!
			if (eth_buffer[a] != *http_entry[index].post_ready_ptr++)
			{
				http_entry[index].post_ready_ptr = post_ready;
			}
			if(*http_entry[index].post_ready_ptr == 0) 
			{
				http_entry[index].post = 0;
				PORTA = PORT_tmp;
                PORT_tmp = 0;
				break;
				//Submit gefunden
			}
		}
	}	
	
	//Welche datei wird angefordert? Wird diese in der Flashspeichertabelle gefunden?
	unsigned char page_index = 0;
	
	if (!http_entry[index].new_page_pointer
	#if USE_MMC
		&& !http_entry[index].mmc	// falls Zeiger noch nicht in Dateipuffer zeigt
	#endif
		)
	{
		for(a = TCP_DATA_START_VAR+5;a<(TCP_DATA_END_VAR);a++)
		{
			if (eth_buffer[a] == '\r')
			{
				eth_buffer[a] = '\0';
				break;
			}
		}
	
		#if USE_MMC
		/*
		** falls SD-Karte vorhanden ist wird erst auf der Karte gesucht
		** und danach im Flash Speicher. So k�nnen einzelne Dateien
		** immer noch vom Flash benutzt werden (LEDON.GIF ...)
		*/

		File *sdfile;

		// Dateinamen ab 6. Stelle extrahieren (am Anfang steht "GET /")
		unsigned char *_ptmp = &eth_buffer[TCP_DATA_START_VAR + 5];
		unsigned char *_pfn = http_entry[index].fname;
		unsigned char *_dot = NULL;
		int8_t i = 12;		// max 12 Zeichen, da 8.3-Dateinamen

		while (*_ptmp && (*_ptmp != ' ') && i--) {
			if (*_ptmp == '.'){
				_dot = _pfn;
			}
			*_pfn++ = *_ptmp++;
		}
		*_pfn = 0;	// Dateinamen mit '\0' abschliessen

		if (_dot != NULL) {
			++_dot;			// zeigt auf Beginn der letzen Fileextension
		} else {
			_dot = _pfn;	// zeigt auf abschliessende \0
		}

		HTTP_DEBUG("\r\n%s (%s)... ",http_entry[index].fname,_dot);


		if (*(http_entry[index].fname) == '\0') {							// falls kein Dateiname angegeben wurde
			strcpy_P((char *)http_entry[index].fname, PSTR("INDEX.HTM"));	// Standard setzen
			_dot = http_entry[index].fname + 6;
		}
		
		sdfile = f16_open((char *)http_entry[index].fname,"r");
		if (sdfile) {
			HTTP_DEBUG("\r\nDatei %s auf SD-Karte gefunden.",http_entry[index].fname);

			http_entry[index].mmc = 1;									// Eintrag kommt von Speicherkarte

			if (strncasecmp_P((char *)_dot,PSTR("htm"),3)==0) {			// falls htm-Datei
				http_entry[index].http_header_type = 0;
			} else if (strncasecmp_P((char *)_dot,PSTR("csv"),3)==0) {	// falls csv-Datendatei
				http_entry[index].http_header_type = 2;
			} else {
				http_entry[index].http_header_type = 1;					// default ist auch 1
			}

			http_entry[index].filesize = sdfile->dir_entry.file_size;

			// ersten Datenblock setzen
			http_entry[index].charcount = 0;
			http_entry[index].old_charcount = 0;
			f16_close(sdfile);
		}
		else {
			#endif
			while((char *)pgm_read_word(&WEBPAGE_TABLE[page_index].filename))
			{
				if (strcasestr_P((char*)&eth_buffer[TCP_DATA_START_VAR],(char *)pgm_read_word(&WEBPAGE_TABLE[page_index].filename))!=0) 
					{
						http_entry[index].http_header_type = 1;
						if (strcasestr_P((char *)pgm_read_word(&WEBPAGE_TABLE[page_index].filename),PSTR(".jpg"))!=0)
						{
							#if USE_CAM
							if (strcasestr_P((char *)pgm_read_word(&WEBPAGE_TABLE[page_index].filename),PSTR("camera"))!=0)
							{	
								http_entry[index].cam = 1;
							}
							#endif //USE_CAM
							http_entry[index].http_header_type = 1;
						}
						if (strcasestr_P((char *)pgm_read_word(&WEBPAGE_TABLE[page_index].filename),PSTR(".gif"))!=0)
						{
							http_entry[index].http_header_type = 1;
						}	
						if (strcasestr_P((char *)pgm_read_word(&WEBPAGE_TABLE[page_index].filename),PSTR(".htm"))!=0)
						{
							http_entry[index].http_header_type = 0;	
						}	
						http_entry[index].new_page_pointer = (char *)pgm_read_word(&WEBPAGE_TABLE[page_index].page_pointer);
						break;
					}
				page_index++;
			}
		#if USE_MMC
		}
		#endif
	}

	//Wurde das Ende vom Header nicht erreicht
	//kommen noch weitere St�cke vom Header!
	if ((*http_entry[index].hdr_end_pointer != 0) || (http_entry[index].post == 1))
	{
		//Der Empfang wird Quitiert und es wird auf weiteres Headerst�ck gewartet
		tcp_entry[index].status =  ACK_FLAG;
		create_new_tcp_packet(0,index);
		//Warten auf weitere Headerpackete
		tcp_entry[index].app_status = 1;
		return;
	}	
	
	//Wurde das Passwort in den ganzen Headerpacketen gefunden?
	//Wenn nicht dann ausf�hren und Passwort anfordern!
	if((!http_entry[index].http_auth) && tcp_entry[index].status&PSH_FLAG)
	{	
		//HTTP_AUTH_Header senden!
		http_entry[index].new_page_pointer = Page0;
		memcpy_P((char*)&eth_buffer[TCP_DATA_START_VAR],http_header3,(sizeof(http_header3)-1));
		tcp_entry[index].status =  ACK_FLAG | PSH_FLAG;
		create_new_tcp_packet((sizeof(http_header3)-1),index);
		tcp_entry[index].app_status = 2;
		return;
	}
	
	//Standart INDEX.HTM Seite wenn keine andere gefunden wurde
	if (!http_entry[index].new_page_pointer
	#if USE_MMC
		&& !http_entry[index].mmc	// falls Zeiger noch nicht in Dateipuffer zeigt
	#endif
		)
	{
		//Besucher Counter
		var_array[MAX_VAR_ARRAY-1]++;
		
		http_entry[index].new_page_pointer = Page1;
		http_entry[index].http_header_type = 0;
	}	
	
	tcp_entry[index].app_status = 2;
	/*
	**	Seiten Header wird gesendet
	*/
	switch (http_entry[index].http_header_type) {

		case	0 :
			a = (sizeof(http_header1)-1);
			memcpy_P((char*)&eth_buffer[TCP_DATA_START_VAR],http_header1,a);
			HTTP_DEBUG("\r\nsende OK f�r text/html");
			break;
	
		case	1 :
			a = (sizeof(http_header2)-1);
			memcpy_P((char*)&eth_buffer[TCP_DATA_START_VAR],http_header2,a);
			HTTP_DEBUG("\r\nsende OK f�r image/jpg");
			break;

		case	2 :
			a = (sizeof(http_csv_header)-1);
			memcpy_P((char*)&eth_buffer[TCP_DATA_START_VAR],http_csv_header,a);
			HTTP_DEBUG("\r\nsende OK f�r csv-header");
			break;
	}

	HTTP_DEBUG("\r\nmit Header antworten. Index %i L:%i",index,a);
	tcp_entry[index].status =  ACK_FLAG | PSH_FLAG;
	create_new_tcp_packet(a,index);
	return;	
}

//----------------------------------------------------------------------------
//Daten Packete an Client schicken
void httpd_data_send (unsigned char index)
{	
	unsigned int a;
	unsigned char str_len;
	
	char var_conversion_buffer[CONVERSION_BUFFER_LEN];
	
	//Passwort wurde im Header nicht gefunden
	if(!http_entry[index].http_auth)
	{
		http_entry[index].new_page_pointer = Page0;
		#if USE_CAM
		http_entry[index].cam = 0;
		#endif //USE_CAM
	}
	
	#if USE_CAM //*****************************************************************
	unsigned long byte_counter = 0;
	
	if(http_entry[index].cam > 0)
	{
        //Neues Bild wird in den Speicher der Kamera geladen!
		if(http_entry[index].cam == 1){
			max_bytes = cam_picture_store(CAM_RESELUTION);
			http_entry[index].cam = 2;
		}
        
		for (a = 0;a < (MTU_SIZE-(TCP_DATA_START)-10);a++)
		{
			byte_counter = ((tcp_entry[index].app_status - 3)*(MTU_SIZE-(TCP_DATA_START)-10)) + a;
			
			eth_buffer[TCP_DATA_START + a] = cam_data_get(byte_counter);
			
			if(byte_counter > max_bytes)
			{
				tcp_entry[index].app_status = 0xFFFD;
				a++;
				break;
			}
		}
		//Erzeugte Packet kann nun gesendet werden!
		tcp_entry[index].status =  ACK_FLAG | PSH_FLAG;
		create_new_tcp_packet(a,index);
		return;	
	}
	#endif //USE_CAM***************************************************************

	//kein Paket empfangen Retransmission des alten Paketes
	if (tcp_entry[index].status == 0) 
	{
		http_entry[index].new_page_pointer = http_entry[index].old_page_pointer;
		#if USE_MMC
		if (http_entry[index].mmc) {
			 http_entry[index].charcount = http_entry[index].old_charcount;
		}
		#endif
	}

	/*
	** alte Pointer auf Datenanfang in Flash/Datei nachziehen
	*/
	http_entry[index].old_page_pointer = http_entry[index].new_page_pointer;
	#if USE_MMC
	http_entry[index].old_charcount = http_entry[index].charcount;

	File *htmfile = 0;

	if (http_entry[index].mmc) {
		htmfile = f16_open((char *)http_entry[index].fname,"r");
		if (htmfile) {
			f16_fseek(htmfile,http_entry[index].old_charcount,FAT16_SEEK_SET);
		} else {
			http_entry[index].mmc = false;
		}
	}
	#endif

	for (a = 0;a<(MTU_SIZE-(TCP_DATA_START)-150);a++)
	{
		unsigned char b;

		// hole n�chsten Character aus Flash oder Datei auf Speicherkarte
		#if USE_MMC
		if (http_entry[index].mmc) {
			int ch = f16_getc(htmfile);
			if ( ch == -1 ) {	// EOF
				tcp_entry[index].app_status = 0xFFFD;
				break;
			}

			b = (unsigned char) ch;
			http_entry[index].charcount++;

		} else
			#endif
			b = pgm_read_byte(http_entry[index].new_page_pointer++);

		eth_buffer[TCP_DATA_START + a] = b;
		
		//M�ssen Variablen ins Paket eingesetzt werden?
		if (b == '%')
		{
			#define TOKENLEN 10
			uint8_t nchars = 0;
			char token[TOKENLEN+1];
			char *dest = token;
			int8_t length = TOKENLEN;

			#if USE_MMC
			if (http_entry[index].mmc)
			{
				while (length--)
					{
					*dest++ = (unsigned char) f16_getc(htmfile);
					}
			}
			else
			#endif
				strlcpy_P(dest, http_entry[index].new_page_pointer, length);

			token[TOKENLEN] = 0;
			HTTP_DEBUG("\r\nTeste Token: %s",token);

			if (strncasecmp_P(token,PSTR("TIME"),4)==0) {
				uint16_t year;
				uint8_t month, day, hour, min, sec;
				HTTP_DEBUG(" - Zeit");
				get_datetime(&year, &month, &day, &hour, &min, &sec);
				sprintf_P(var_conversion_buffer,PSTR("%2.2d:%2.2d:%2.2d"),hour,min,sec);
				str_len = strnlen(var_conversion_buffer,CONVERSION_BUFFER_LEN);
				memmove(&eth_buffer[TCP_DATA_START+a],var_conversion_buffer,str_len);
				a += str_len-1;
				nchars = 4;
			}

			if (strncasecmp_P(token,PSTR("DATE"),4)==0) {
				uint16_t year;
				uint8_t month, day, hour, min, sec;
				HTTP_DEBUG(" - Datum");
				get_datetime(&year, &month, &day, &hour, &min, &sec);
				sprintf_P(var_conversion_buffer,PSTR("%2.2d.%2.2d.%4d"),day,month,year);
				str_len = strnlen(var_conversion_buffer,CONVERSION_BUFFER_LEN);
				memmove(&eth_buffer[TCP_DATA_START+a],var_conversion_buffer,str_len);
				a += str_len-1;
				nchars = 4;
			}

			if (strncasecmp_P(token,PSTR("VA@"),3)==0) {	
				HTTP_DEBUG(" - Analogwert");
				b = (token[3]-48)*10;
				b +=(token[4]-48);
				itoa (var_array[b],var_conversion_buffer,10);
				str_len = strnlen(var_conversion_buffer,CONVERSION_BUFFER_LEN);
				memmove(&eth_buffer[TCP_DATA_START+a],var_conversion_buffer,str_len);
				a += str_len-1;
				nchars = 5;
			}
			
#if USE_OW
			/*
			*	1-Wire Temperatursensoren
			*	-------------------------
			*	OW@nn	nn = 00 bis MAXSENSORS-1 gibt Werte in 1/10 �C aus
			*	OW@mm	mm = 20 bis MAXSENSORS-1+20 gibt Werte in �C mit einer Nachkommastelle aus
			*	d.h. OW@nn f�r Balkenbreite verwenden und OW@mm f�r Celsius-Anzeige
			*/
			if (strncasecmp_P(token,PSTR("OW@"),3)==0) {	
				HTTP_DEBUG(" - 1-wire");
				b = (token[3]-48)*10;
				b +=(token[4]-48);
				if (b >= 20) {	// Offset bei Sensor# abziehen und Wert als Dezimalzahl ausgeben
					b -= 20;
					int8_t i = (int8_t)(ow_array[b] / 10);
					itoa (i,var_conversion_buffer,10);
					str_len = strnlen(var_conversion_buffer,CONVERSION_BUFFER_LEN);
					var_conversion_buffer[str_len] = ',';
					str_len++;
					memmove(&eth_buffer[TCP_DATA_START+a],var_conversion_buffer,str_len);
					a += str_len;
					i = ow_array[b] % 10;
					itoa (i,var_conversion_buffer,10);
				} else {
					itoa (ow_array[b],var_conversion_buffer,10);
				}
				str_len = strnlen(var_conversion_buffer,CONVERSION_BUFFER_LEN);
				memmove(&eth_buffer[TCP_DATA_START+a],var_conversion_buffer,str_len);
				a += str_len-1;
				nchars = 5;
			}
#endif
			//Einsetzen des Port Status %PORTxy durch "checked" wenn Portx.Piny = 1
			//x: A..G  y: 0..7 
			if (strncasecmp_P(token,PSTR("PORT"),4)==0) {
				HTTP_DEBUG(" - Portstatus");
				unsigned char pin  = (token[5]-48);	
				b = 0;
				switch(token[4]) {
					case 'A':
						b = (PORTA & (1<<pin));
						break;
					case 'B':
						b = (PORTB & (1<<pin));
						break;
					case 'C':
						b = (PORTC & (1<<pin));
						break;
					case 'D':
						b = (PORTD & (1<<pin));
						break; 
				}
				
				if(b) {
					strcpy_P(var_conversion_buffer, PSTR("checked"));
				} else {
					strcpy_P(var_conversion_buffer, PSTR("\0"));
				}

				str_len = strnlen(var_conversion_buffer,CONVERSION_BUFFER_LEN);
				memmove(&eth_buffer[TCP_DATA_START+a],var_conversion_buffer,str_len);
				a += str_len-1;
				nchars = 6;
			}
			
			//Einsetzen des Pin Status %PI@xy bis %PI@xy durch "ledon" oder "ledoff"
			//x = 0 : PINA / x = 1 : PINB / x = 2 : PINC / x = 3 : PIND
			if (strncasecmp_P(token,PSTR("PIN"),3)==0) {
				HTTP_DEBUG(" - Eingangswert");
				unsigned char pin  = (token[4]-48);	
				b = 0;
				switch(token[3]) {
					case 'A':
						b = (PINA & (1<<pin));
						break;
					case 'B':
						b = (PINB & (1<<pin));
						break;
					case 'C':
						b = (PINC & (1<<pin));
						break;
					case 'D':
						b = (PIND & (1<<pin));
						break;    
				}
				
				if(b) {	// gesetztes bit bedeutet: nix dran, da Pullup Widerstand
					strcpy_P(var_conversion_buffer, PSTR("ledoff.gif"));
				} else {
					strcpy_P(var_conversion_buffer, PSTR("ledon.gif"));	// Schalter auf Masse geschlossen
				}

				str_len = strnlen(var_conversion_buffer,CONVERSION_BUFFER_LEN);
				memmove(&eth_buffer[TCP_DATA_START+a],var_conversion_buffer,str_len);
				a += str_len-1;
				nchars = 5;
			}

			//wurde das Ende des Paketes erreicht?
			//Verbindung TCP Port kann beim n�chsten ACK geschlossen werden
			//Schleife wird abgebrochen keine Daten mehr!!
			if (strncasecmp_P(token,PSTR("END"),3)==0) {	
				HTTP_DEBUG("\r\nFlash-Dateiende.");
				tcp_entry[index].app_status = 0xFFFD;
				nchars = 3;
				break;
			}

			#if USE_MMC
			if (http_entry[index].mmc) {
				http_entry[index].charcount += nchars;
				f16_fseek(htmfile,http_entry[index].charcount,FAT16_SEEK_SET);
			} else
				#endif
				http_entry[index].new_page_pointer += nchars;
		}
	}

	#if USE_MMC
	if (htmfile)
		f16_close(htmfile);
	#endif

	//Erzeugtes Paket kann nun gesendet werden!
	tcp_entry[index].status =  ACK_FLAG | PSH_FLAG;
	create_new_tcp_packet(a,index);
	return;
}


