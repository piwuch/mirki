/*----------------------------------------------------------------------------
 Copyright:      W.Wallucks
 Remarks:        
 known Problems: none
 Version:        04.05.2008
 Description:    messen und regeln mit dem Webmodul

 Dieses Programm ist freie Software. Sie k�nnen es unter den Bedingungen der 
 GNU General Public License, wie von der Free Software Foundation ver�ffentlicht, 
 weitergeben und/oder modifizieren, entweder gem�� Version 2 der Lizenz oder 
 (nach Ihrer Option) jeder sp�teren Version. 

 Die Ver�ffentlichung dieses Programms erfolgt in der Hoffnung, 
 da� es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, 
 sogar ohne die implizite Garantie der MARKTREIFE oder der VERWENDBARKEIT 
 F�R EINEN BESTIMMTEN ZWECK. Details finden Sie in der GNU General Public License. 

 Sie sollten eine Kopie der GNU General Public License zusammen mit diesem 
 Programm erhalten haben. 
 Falls nicht, schreiben Sie an die Free Software Foundation, 
 Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 
------------------------------------------------------------------------------*/

#ifndef _MESSUNG_H_
	#define _MESSUNG_H_

#define SDKARTE_CHECKPIN	PINA6

#define SENS_Read			PINC	//Porteingang an dem die An/Aus Sensoren angeschlossen sind
#define SENS_PULLUP			PORTC	//
#define SENS_DDR			DDRC	
#define SENS_INTENA			PCIE2	// == Port C
#define SENS_INTMASK		PCMSK2

#define PORT_SCHALTER1		PD4		// Portausgang mit Schalter
#define PORT_SCHALTER2		PD5
#define PORT_SCHALTER3		PD6
#define OUT_DDR				DDRD
#define OUT_PORT			PORTD

/*
* Schalter1-3 schalten
*/
#define S1An()	OUT_PORT |= (1<<PORT_SCHALTER1)
#define S1Aus()	OUT_PORT &= ~(1<<PORT_SCHALTER1)
#define S2An()	OUT_PORT |= (1<<PORT_SCHALTER2)
#define S2Aus()	OUT_PORT &= ~(1<<PORT_SCHALTER2)
#define S3An()	OUT_PORT |= (1<<PORT_SCHALTER2)
#define S3Aus()	OUT_PORT &= ~(1<<PORT_SCHALTER3)

#if USE_OW
extern volatile uint16_t ow_array[MAXSENSORS];	// Speicherplatz f�r 1-wire Sensorwerte
#endif

//----------------------------------------------------------------------------
// Prototypen
void messung_init(void);
void lese_Temperatur(void);
void set_SensorResolution(void);
int16_t initSchaltzeiten(char *);
void regelAnlage(SOLL_STATUS *);
void log_init(void);
void log_status(void);

#endif
