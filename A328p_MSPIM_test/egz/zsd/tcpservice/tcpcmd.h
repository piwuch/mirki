/*------------------------------------------------------------------------------
 Copyright:      Radig Ulrich + Wilfried Wallucks
 Author:         Radig Ulrich + Wilfried Wallucks
 Remarks:        
 known Problems: none
 Version:        14.05.2008
 Description:    Befehlsinterpreter
------------------------------------------------------------------------------*/

#ifndef _TCPCMD_H_
	#define _TCPCMD_H_	
	
	#define HELPTEXT	1		// Helptext anzeigen
	#define MAX_VAR		5
	
	//--------------------------------------------------------

	// Prototyp f�r Befehlsfunktion
	typedef int16_t(*cmd_fp)(char *);

	typedef struct
	{
		const char* cmd;		// Stringzeiger auf Befehlsnamen
		cmd_fp		fp;			// Zeiger auf auszuf�hrende Funktion
	} CMD_ITEM;	
	
	extern unsigned char extract_cmd	(char *);
	extern void write_eeprom_ip 		(unsigned int);
	
	//reset the unit
	extern int16_t command_reset		(char *);
	//Ausgabe der ARP Tabelle
	extern int16_t command_arp 			(char *);
	//Ausgabe der TCP Tabelle
	extern int16_t command_tcp 			(char *);
	
	//�ndern/ausgabe der IP, NETMASK, ROUTER_IP, NTP_IP
	extern int16_t command_ip 			(char *);
	extern int16_t command_net 			(char *);
	extern int16_t command_router		(char *);
	extern int16_t command_ntp 			(char *);

	extern int16_t command_mac			(char *);
	extern int16_t command_ver			(char *);
	extern int16_t command_setvar		(char *);
	extern int16_t command_time			(char *);
	extern int16_t command_ntp_refresh	(char *);
	extern int16_t command_setT			(char *);
	#if USE_WOL
	extern int16_t command_wol	 		(char *);
	#endif
    #if USE_MAIL
	extern int16_t command_mail			(char *);
	#endif //USE_MAIL
	extern int16_t command_ping 		(char *);
	extern int16_t command_help 		(char *);

	#if USE_MMC
	int16_t	cmd_MMCdir					(char *);
	#endif
	extern int16_t command_ADC			(char *);
	int16_t print_disk_info				(char *);
	#define RESET() {asm("ldi r30,0"); asm("ldi r31,0"); asm("ijmp");}
	
#endif //_TCPCMD_H_


