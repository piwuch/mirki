#ifndef _TCPSRVD_H
	#define _TCPSRVD_H
 
	//#define TCPSRV_DEBUG	usart_write		//mit Debugausgabe
	#define TCPSRV_DEBUG(...)

#define MAXCMDBUFFER	7
#define MAXARGVBUFFER	31

typedef struct {
		volatile uint8_t ctl_waitack	: 1;
		volatile uint8_t data_waitack	: 1;
		volatile uint8_t send_226		: 1;
		volatile uint8_t read_argv		: 1;	// wird cmd selbst oder Parameter gelesen
		volatile uint8_t data_state		: 2;	// 0 : idle; 1 : LIST; 2 : RETR; 3: STOR/Append
		volatile uint8_t transfermode	: 1;	// ASCII (0) oder BINARY (1)
		volatile uint8_t loginOK		: 1;	// Zustand, falls kein anonymer Zugriff erlaubt
		volatile uint8_t userOK			: 1;
		volatile uint8_t tcpindex;				// Zeiger auf aktuellen TCP Eintrag
		volatile uint8_t cmdindex;				// Zeiger auf aktuelles Zeichen im Puffer
		volatile uint8_t argvindex;				// Zeiger auf aktuelles Zeichen im Puffer
		volatile unsigned char cmdbuffer[MAXCMDBUFFER+1];	// Puffer f�r Kommando
		volatile unsigned char argvbuffer[MAXARGVBUFFER+1];	// Puffer f�r Parameter
		unsigned char 	fname[32];				// long filename
		uint32_t		filesize;
		uint32_t		charcount;				// Anzahl der bereits gesendeten Character
		uint32_t		old_charcount;			// Anzahl der vor dem letzten Paket gesendeten Character
		uint16_t		direntry_num;			// aktueller directory Eintrag
		uint16_t		old_direntry_num;		// alter directory Eintrag
} TCPSRV_STATUS;
	
	//Prototypen
	void 	 tcpsrvd_init (void);
	uint16_t doSrvCmd(uint8_t, unsigned char *);
	void 	 tcpsrvd (uint8_t);
	void 	 send_data_status(unsigned char);
 
	void respond_P(uint8_t, const char *, unsigned char);
	void respond(uint8_t, const char *, unsigned char);

#endif

