/*----------------------------------------------------------------------------
 Copyright:      Radig Ulrich  mailto: mail@ulrichradig.de
 Author:         Radig Ulrich
 Remarks:        
 known Problems: none
 Version:        24.10.2007
 Description:    Webserver uvm.

 Dieses Programm ist freie Software. Sie k�nnen es unter den Bedingungen der 
 GNU General Public License, wie von der Free Software Foundation ver�ffentlicht, 
 weitergeben und/oder modifizieren, entweder gem�� Version 2 der Lizenz oder 
 (nach Ihrer Option) jeder sp�teren Version. 

 Die Ver�ffentlichung dieses Programms erfolgt in der Hoffnung, 
 da� es Ihnen von Nutzen sein wird, aber OHNE IRGENDEINE GARANTIE, 
 sogar ohne die implizite Garantie der MARKTREIFE oder der VERWENDBARKEIT 
 F�R EINEN BESTIMMTEN ZWECK. Details finden Sie in der GNU General Public License. 

 Sie sollten eine Kopie der GNU General Public License zusammen mit diesem 
 Programm erhalten haben. 
 Falls nicht, schreiben Sie an die Free Software Foundation, 
 Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 
----------------------------------------------------------------------------*/

#include <avr/io.h>
#include "config.h"

#include "usart.h"
#include "networkcard/enc28j60.h"
#include "stack.h"
#include "timer.h"
#include "base64.h"
#include "httpd.h"
#include "telnetd.h"

#if USE_WOL
#include "wol.h"
#endif

#if USE_NTP
  #include "ntp.h"
#endif

#if USE_SER_LCD
  #include "lcd.h"
  #include "udp_lcd.h"
#endif

#if USE_ADC
  #include "analog.h"
#endif

#if USE_CAM
  #include "camera/cam.h"
  #include "camera/servo.h"
#endif

#if USE_MAIL
  #include "sendmail.h"
#endif

#if TCP_SERVICE
  #include "tcpservice/tcpcmd.h"
  #include "tcpservice/tcpsrv.h"
#else
  #include "cmd.h"
#endif

#if USE_MMC
  #include "sdkarte/fat16.h"
  #include "sdkarte/sdcard.h"
#endif

#include "messung.h"


/**
**	globale Variable
*/
volatile ANLAGEN_STATUS anlagenStatus;
volatile PROZESS_STATUS machineStatus;
		 LOG_STATUS		logStatus;
volatile SOLL_STATUS 	anlagenSollStatus;

//----------------------------------------------------------------------------
//
int main(void)
{
	//Konfiguration der Ausg�nge bzw. Eing�nge
	//definition erfolgt in der config.h
	DDRA = OUTA;
	DDRC = OUTC;
	DDRD = OUTD;
	
    unsigned long a;
	#if USE_SERVO
		servo_init ();
	#endif //USE_SERVO
	
    usart_init(BAUDRATE); // setup the UART
	
	#if USE_ADC
		ADC_Init();
	#endif
	
	usart_write("\n\rSystem Ready\n\r");
    usart_write("Compiliert am "__DATE__" um "__TIME__"\r\n");
    usart_write("Compiliert mit GCC Version "__VERSION__"\r\n");
	for(a=0;a<1000000;a++){asm("nop");};

	//Applikationen starten
	stack_init();
	httpd_init();
	telnetd_init();
	
	//Spielerrei mit einem LCD
	#if USE_SER_LCD
	udp_lcd_init();
	lcd_init();
	lcd_clear();
	back_light = 1;
	lcd_print(0,0,"System Ready");
	#endif
	//Ethernetcard Interrupt enable
	ETH_INT_ENABLE;
	
	//Globale Interrupts einschalten
	sei(); 
	
	#if USE_CAM
		#if USE_SER_LCD
		lcd_print(1,0,"CAMERA INIT");
		#endif //USE_SER_LCD
	for(a=0;a<2000000;a++){asm("nop");};
	cam_init();
	max_bytes = cam_picture_store(CAM_RESELUTION);
		#if USE_SER_LCD
		back_light = 0;
		lcd_print(1,0,"CAMERA READY");
		#endif //USE_SER_LCD
	#endif //USE_CAM
	
	#if USE_NTP
        ntp_init();
        ntp_request();
	#endif //USE_NTP
	
	#if USE_WOL
        wol_init();
	#endif //USE_WOL
    
    #if USE_MAIL
        mail_client_init();
	#endif //USE_MAIL
    
	#if TCP_SERVICE
		tcpsrvd_init();
	#endif

    #if USE_MMC
		f16_init();
	#endif

	//**************************************************
	#define logdata usart_write
	messung_init();
		
	while(1)
	{
		if (machineStatus.timeChanged) {
			// eine Sekunde hochz�hlen und
			// auf �nderungen zu vordefinierten Zeiten reagieren
			TM_AddOneSecond();
			machineStatus.timeChanged--;
		}

		#if USE_SCHEDULER

		#if USE_LOGDATEI
		if (machineStatus.LogInit ) {
			// neue Logdatei anlegen
			log_init();
			machineStatus.initialisieren = true;
			machineStatus.LogInit = false;
		}
		#endif

		if (machineStatus.initialisieren) {
			// Schaltzeiten neu einlesen
			initSchaltzeiten(0);
			machineStatus.initialisieren = 0;
		}

		if (machineStatus.regeln) {
			// Anlage entsprechend regeln
			// alle 10 Minuten wegen Schaltzust�nden
			SOLL_STATUS aktSoll;
			TM_SollzustandGetAktuell(&aktSoll);
			regelAnlage(&aktSoll);
			machineStatus.regeln = 0;
		}
		#endif

		if (machineStatus.Tlesen) {
			// Temperatursensoren lesen
			lese_Temperatur();
			machineStatus.LogSchreiben = true;		// danach gleich Logdatei schreiben
			machineStatus.Tlesen = 0;
		}

		#if USE_LOGDATEI
		if (machineStatus.LogSchreiben) {
			// schreiben
			log_status();
			machineStatus.LogSchreiben = false;
		}
		#endif

		if (machineStatus.PINCchanged != 0) {

			// Eingang hat sich ge�ndert

			if (machineStatus.PINCchanged & 0x80) {
				/* Zustand von PINC7 hat sich ge�ndert
				   TODO: hier sollte jetzt drauf reagiert werden

				   Beispiel:
				*/
				// z�hlen wie oft der PIN eingeschaltet wurde
				// der Z�hlerstand von PINCcounter kann dann
				// regelm�ssig ins Logfile geschrieben werden

				if ( !(machineStatus.PINCStatus & 0x80) ) {	// falls eingeschaltet
					anlagenStatus.Zaehler1++;
				}
			}

			if (machineStatus.PINCchanged & 0x40) {
				/* Zustand von PINC6 hat sich ge�ndert
				   TODO: hier sollte jetzt drauf reagiert werden

				   Beispiel:
				*/
				// Zeitspanne zwischen Ein- und Ausschalten messen
				// und in Logdatei festhalten
				if ( !(machineStatus.PINCStatus & 0x40) ) {	// falls eingeschaltet
					anlagenStatus.Zaehler2 = time;
				} 
				else {
					logdata("C6 Zeit: %i Sekunden",time - anlagenStatus.Zaehler2);
					anlagenStatus.Zaehler2=0;
				}
			}

			if (machineStatus.PINCchanged & 0x20) {
				/* Zustand von PINC5 hat sich ge�ndert
				   TODO: hier sollte jetzt drauf reagiert werden

				Beispiel:
				*/
				// in Abh�ngigkeit von C6 Pulse an C5 z�hlen
				if ( !(machineStatus.PINCStatus & 0x60) ) {
					// nur z�hlen, wenn C6 eingeschaltet
				   	anlagenStatus.Zaehler3++;
				} 
			}

			if (machineStatus.PINCchanged & 0x10) {
				/* Zustand von PINC4 hat sich ge�ndert
				   TODO: hier sollte jetzt drauf reagiert werden

				Beispiel: */
				// vordefinierte E-Mail schicken
				if ( !(machineStatus.PINCStatus & 0x10) ) {
					#if USE_MAIL
			    	mail_send();
					#else
					usart_write("\r\nEine E-Mail sollte gesendet werden.");
					#endif
				}
			}

			anlagenStatus.relais1 = !(machineStatus.PINCStatus & 0x01);
			anlagenStatus.relais2 = !(machineStatus.PINCStatus & 0x02);
			anlagenStatus.relais3 = !(machineStatus.PINCStatus & 0x04);
			machineStatus.PINCchanged = 0;
		}

		// free running ADC
		#if USE_ADC
		ANALOG_ON;
		#endif
	    eth_get_data();
		
        //Terminalcommandos auswerten
		if (usart_status.usart_ready){
            usart_write("\r\n");
			if(extract_cmd(&usart_rx_buffer[0]))
			{
			#if USE_MMC
				usart_write("\r\nSD:%s>",cwdirectory);
			#else
				usart_write("Ready\r\n\r\n");
			#endif
			}
			else
			{
				usart_write("ERROR\r\n\r\n");
			}
			usart_status.usart_ready =0;
		}
		
        //Wetterdaten empfangen (Testphase)
        #if GET_WEATHER
        http_request ();
        #endif

        //Empfang von Zeitinformationen
		#if USE_NTP
		if(!ntp_timer){
			ntp_timer = NTP_REFRESH;
			ntp_request();
		}
		#endif //USE_NTP
		
        //Versand von E-Mails
        #if USE_MAIL
        if (mail_enable == 1)
        {
            mail_enable = 0;
            mail_send();
        }
        #endif //USE_MAIL
        
        //Rechner im Netzwerk aufwecken
        #if USE_WOL
        if (wol_enable == 1)
        {
            wol_enable = 0;
            wol_request();
        }
        #endif //USE_WOL
           
		//USART Daten f�r Telnetanwendung?
		telnetd_send_data();
    }
		
return(0);
}

