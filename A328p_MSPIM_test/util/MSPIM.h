/*
 * TWI_.h
 *
 *  Created on: 21-07-2014
 *      Author: Ja
 */

#include <avr/io.h>
#define BAUD 9600UL
#include <util/setbaud.h>
void usart_MSPIM_Init(uint16_t baud);
uint8_t MSPIM_Transmit(uint8_t data);
