/*
 * defines.h
 *
 *  Created on: 23-12-2014
 *      Author: Ja
 */

#ifndef DEFINES_H_
#define DEFINES_H_

#define PORT(port) SPORT(port)
#define SPORT(port) (PORT##port)

#define DDR(port) SDDR(port)
#define SDDR(port) (DDR##port)

#define PIN(port) SPIN(port)
#define SPIN(port) (PIN##port)

#define P(port,pin) SECONDP(port,pin)
#define SECONDP(port, pin) (P##port##pin)

#endif /* DEFINES_H_ */
