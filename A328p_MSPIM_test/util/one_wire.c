/*
 * 1wire.c
 *
 *  Created on: 11-12-2014
 *      Author: Ja
 */

#include "one_wire.h"

void OW_init() {
	CLR(DDR, PIN);
	CLR(PORT, PIN);
}

void OW_ResetPulse() {
	SET(DDR, PIN);
	_delay_us(480);
	CLR(DDR, PIN);
}

uint8_t OW_WaitForPresencePulse() {
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		OW_ResetPulse();
		_delay_us(30);
		unsigned char counter = 0;
		while ((counter < 0xFF) && (PORT & PIN)) {
			_delay_us(1);
			counter++;
		}
		if (counter == 0xFF) {
//			Error = OW_NoPresencePulse;
			return 0;
		}
		counter = 0;
		while ((counter < 0xFF) && (PORT & PIN)==0)  {
			_delay_us(2);
			counter++;
		}
		if (counter == 0xFF) {
//			Error = OW_BusShorted;
			return 0;
		}
//		Error = OW_OK;
		return 1;
	}
}
