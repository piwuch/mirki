/*
 * MSPIM.c
 *
 *  Created on: 6-12-2014
 *      Author: Ja
 */

#include "MSPIM.h"



void MSPIM_Init(uint16_t baud) {
	UBRR0 = 0;
	DDRD |= _BV(PD4); //SCK jest wyj�ciem
	UCSR0C = _BV(UMSEL01) | _BV(UMSEL00) | _BV(UCPHA0) | _BV(UCPOL0); //Tryb SPI 0
	UCSR0B = _BV(RXEN0) | _BV(TXEN0); //W��cz nadajnik i odbiornik
	UBRR0 = F_CPU/16/baud-1;
}
uint8_t MSPIM_Transmit(uint8_t data)
{
while(!(UCSR0A & _BV(UDRE0))); //Czy bufor jest pusty?
UDR0=data;
while(!(UCSR0A & _BV(RXC0))); //Zaczekaj na odebranie bajtu
return UDR0;
}
