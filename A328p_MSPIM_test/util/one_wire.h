/*
 * 1wire.h
 *
 *  Created on: 11-12-2014
 *      Author: Ja
 */
#include <avr/io.h>
#include <util/delay.h>
#include <util/atomic.h>

#include "defines.h"

#define PIN PC0
#define PORT PORTC
#define DDR DDRC

void OW_init();
void OW_ResetPulse();
uint8_t OW_WaitForPresencePulse();
