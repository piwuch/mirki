################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../egz/zsram/Interfaces/1wire.c \
../egz/zsram/Interfaces/rs232.c \
../egz/zsram/Interfaces/spi.c \
../egz/zsram/Interfaces/sram.c 

S_UPPER_SRCS += \
../egz/zsram/Interfaces/i2cmaster.S 

OBJS += \
./egz/zsram/Interfaces/1wire.o \
./egz/zsram/Interfaces/i2cmaster.o \
./egz/zsram/Interfaces/rs232.o \
./egz/zsram/Interfaces/spi.o \
./egz/zsram/Interfaces/sram.o 

S_UPPER_DEPS += \
./egz/zsram/Interfaces/i2cmaster.d 

C_DEPS += \
./egz/zsram/Interfaces/1wire.d \
./egz/zsram/Interfaces/rs232.d \
./egz/zsram/Interfaces/spi.d \
./egz/zsram/Interfaces/sram.d 


# Each subdirectory must supply rules for building sources it contributes
egz/zsram/Interfaces/%.o: ../egz/zsram/Interfaces/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

egz/zsram/Interfaces/%.o: ../egz/zsram/Interfaces/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Assembler'
	avr-gcc -x assembler-with-cpp -mmcu=atmega328p -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


