################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../egz/zsram/Drivers/enc28j60.c \
../egz/zsram/Drivers/sd_raw.c 

OBJS += \
./egz/zsram/Drivers/enc28j60.o \
./egz/zsram/Drivers/sd_raw.o 

C_DEPS += \
./egz/zsram/Drivers/enc28j60.d \
./egz/zsram/Drivers/sd_raw.d 


# Each subdirectory must supply rules for building sources it contributes
egz/zsram/Drivers/%.o: ../egz/zsram/Drivers/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


