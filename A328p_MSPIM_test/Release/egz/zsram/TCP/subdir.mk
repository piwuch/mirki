################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../egz/zsram/TCP/arp_icmp_ip.c \
../egz/zsram/TCP/ethernet.c \
../egz/zsram/TCP/tcp_udp.c 

OBJS += \
./egz/zsram/TCP/arp_icmp_ip.o \
./egz/zsram/TCP/ethernet.o \
./egz/zsram/TCP/tcp_udp.o 

C_DEPS += \
./egz/zsram/TCP/arp_icmp_ip.d \
./egz/zsram/TCP/ethernet.d \
./egz/zsram/TCP/tcp_udp.d 


# Each subdirectory must supply rules for building sources it contributes
egz/zsram/TCP/%.o: ../egz/zsram/TCP/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


