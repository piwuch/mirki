################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../egz/zsram/Devices/clock.c \
../egz/zsram/Devices/ds18b20.c \
../egz/zsram/Devices/eeprom.c \
../egz/zsram/Devices/netdevice.c \
../egz/zsram/Devices/partition.c \
../egz/zsram/Devices/stdout.c \
../egz/zsram/Devices/timer.c 

OBJS += \
./egz/zsram/Devices/clock.o \
./egz/zsram/Devices/ds18b20.o \
./egz/zsram/Devices/eeprom.o \
./egz/zsram/Devices/netdevice.o \
./egz/zsram/Devices/partition.o \
./egz/zsram/Devices/stdout.o \
./egz/zsram/Devices/timer.o 

C_DEPS += \
./egz/zsram/Devices/clock.d \
./egz/zsram/Devices/ds18b20.d \
./egz/zsram/Devices/eeprom.d \
./egz/zsram/Devices/netdevice.d \
./egz/zsram/Devices/partition.d \
./egz/zsram/Devices/stdout.d \
./egz/zsram/Devices/timer.d 


# Each subdirectory must supply rules for building sources it contributes
egz/zsram/Devices/%.o: ../egz/zsram/Devices/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


