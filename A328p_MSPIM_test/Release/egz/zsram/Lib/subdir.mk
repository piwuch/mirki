################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../egz/zsram/Lib/base64.c \
../egz/zsram/Lib/byteordering.c \
../egz/zsram/Lib/checksum.c \
../egz/zsram/Lib/crc8.c 

OBJS += \
./egz/zsram/Lib/base64.o \
./egz/zsram/Lib/byteordering.o \
./egz/zsram/Lib/checksum.o \
./egz/zsram/Lib/crc8.o 

C_DEPS += \
./egz/zsram/Lib/base64.d \
./egz/zsram/Lib/byteordering.d \
./egz/zsram/Lib/checksum.d \
./egz/zsram/Lib/crc8.d 


# Each subdirectory must supply rules for building sources it contributes
egz/zsram/Lib/%.o: ../egz/zsram/Lib/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


