################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../egz/zsd/1-wire/crc8.c \
../egz/zsd/1-wire/delay.c \
../egz/zsd/1-wire/ds18x20.c \
../egz/zsd/1-wire/onewire.c 

OBJS += \
./egz/zsd/1-wire/crc8.o \
./egz/zsd/1-wire/delay.o \
./egz/zsd/1-wire/ds18x20.o \
./egz/zsd/1-wire/onewire.o 

C_DEPS += \
./egz/zsd/1-wire/crc8.d \
./egz/zsd/1-wire/delay.d \
./egz/zsd/1-wire/ds18x20.d \
./egz/zsd/1-wire/onewire.d 


# Each subdirectory must supply rules for building sources it contributes
egz/zsd/1-wire/%.o: ../egz/zsd/1-wire/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


