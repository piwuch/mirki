################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../egz/zsd/sdkarte/fat16.c \
../egz/zsd/sdkarte/partition.c \
../egz/zsd/sdkarte/sd_raw.c \
../egz/zsd/sdkarte/sdcard.c 

OBJS += \
./egz/zsd/sdkarte/fat16.o \
./egz/zsd/sdkarte/partition.o \
./egz/zsd/sdkarte/sd_raw.o \
./egz/zsd/sdkarte/sdcard.o 

C_DEPS += \
./egz/zsd/sdkarte/fat16.d \
./egz/zsd/sdkarte/partition.d \
./egz/zsd/sdkarte/sd_raw.d \
./egz/zsd/sdkarte/sdcard.d 


# Each subdirectory must supply rules for building sources it contributes
egz/zsd/sdkarte/%.o: ../egz/zsd/sdkarte/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


