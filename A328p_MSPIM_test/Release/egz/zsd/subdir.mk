################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../egz/zsd/analog.c \
../egz/zsd/base64.c \
../egz/zsd/cmd.c \
../egz/zsd/http_get.c \
../egz/zsd/httpd.c \
../egz/zsd/lcd.c \
../egz/zsd/main.c \
../egz/zsd/messung.c \
../egz/zsd/ntp.c \
../egz/zsd/sendmail.c \
../egz/zsd/stack.c \
../egz/zsd/telnetd.c \
../egz/zsd/timer.c \
../egz/zsd/udp_lcd.c \
../egz/zsd/usart.c \
../egz/zsd/wol.c 

OBJS += \
./egz/zsd/analog.o \
./egz/zsd/base64.o \
./egz/zsd/cmd.o \
./egz/zsd/http_get.o \
./egz/zsd/httpd.o \
./egz/zsd/lcd.o \
./egz/zsd/main.o \
./egz/zsd/messung.o \
./egz/zsd/ntp.o \
./egz/zsd/sendmail.o \
./egz/zsd/stack.o \
./egz/zsd/telnetd.o \
./egz/zsd/timer.o \
./egz/zsd/udp_lcd.o \
./egz/zsd/usart.o \
./egz/zsd/wol.o 

C_DEPS += \
./egz/zsd/analog.d \
./egz/zsd/base64.d \
./egz/zsd/cmd.d \
./egz/zsd/http_get.d \
./egz/zsd/httpd.d \
./egz/zsd/lcd.d \
./egz/zsd/main.d \
./egz/zsd/messung.d \
./egz/zsd/ntp.d \
./egz/zsd/sendmail.d \
./egz/zsd/stack.d \
./egz/zsd/telnetd.d \
./egz/zsd/timer.d \
./egz/zsd/udp_lcd.d \
./egz/zsd/usart.d \
./egz/zsd/wol.d 


# Each subdirectory must supply rules for building sources it contributes
egz/zsd/%.o: ../egz/zsd/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


