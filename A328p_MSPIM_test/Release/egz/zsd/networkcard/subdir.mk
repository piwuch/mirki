################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../egz/zsd/networkcard/enc28j60.c \
../egz/zsd/networkcard/rtl8019.c 

OBJS += \
./egz/zsd/networkcard/enc28j60.o \
./egz/zsd/networkcard/rtl8019.o 

C_DEPS += \
./egz/zsd/networkcard/enc28j60.d \
./egz/zsd/networkcard/rtl8019.d 


# Each subdirectory must supply rules for building sources it contributes
egz/zsd/networkcard/%.o: ../egz/zsd/networkcard/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


