################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../egz/zsd/tcpservice/tcpcmd.c \
../egz/zsd/tcpservice/tcpsrv.c 

OBJS += \
./egz/zsd/tcpservice/tcpcmd.o \
./egz/zsd/tcpservice/tcpsrv.o 

C_DEPS += \
./egz/zsd/tcpservice/tcpcmd.d \
./egz/zsd/tcpservice/tcpsrv.d 


# Each subdirectory must supply rules for building sources it contributes
egz/zsd/tcpservice/%.o: ../egz/zsd/tcpservice/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


