################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../egz/tuxlib/enc28j60.c \
../egz/tuxlib/ip_arp_udp_tcp.c \
../egz/tuxlib/websrv_help_functions.c 

OBJS += \
./egz/tuxlib/enc28j60.o \
./egz/tuxlib/ip_arp_udp_tcp.o \
./egz/tuxlib/websrv_help_functions.o 

C_DEPS += \
./egz/tuxlib/enc28j60.d \
./egz/tuxlib/ip_arp_udp_tcp.d \
./egz/tuxlib/websrv_help_functions.d 


# Each subdirectory must supply rules for building sources it contributes
egz/tuxlib/%.o: ../egz/tuxlib/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


