################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../egz/examples/UDP_Client_Send/main.c 

OBJS += \
./egz/examples/UDP_Client_Send/main.o 

C_DEPS += \
./egz/examples/UDP_Client_Send/main.d 


# Each subdirectory must supply rules for building sources it contributes
egz/examples/UDP_Client_Send/%.o: ../egz/examples/UDP_Client_Send/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


