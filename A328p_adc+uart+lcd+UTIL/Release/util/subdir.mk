################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../util/ADC.c \
../util/HD44780.c \
../util/MSPIM.c \
../util/UART.c \
../util/functions.c \
../util/twi.c 

OBJS += \
./util/ADC.o \
./util/HD44780.o \
./util/MSPIM.o \
./util/UART.o \
./util/functions.o \
./util/twi.o 

C_DEPS += \
./util/ADC.d \
./util/HD44780.d \
./util/MSPIM.d \
./util/UART.d \
./util/functions.d \
./util/twi.d 


# Each subdirectory must supply rules for building sources it contributes
util/%.o: ../util/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega328p -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


