/*
 * test_main.c
 *
 *  Created on: 14-11-2014
 *      Author: Ja
 */

#include <avr\io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "util/HD44780.h"
#include "util/UART.h"
#include "util/functions.h"
//#include "util/ADC.h"

#define LED _BV(PC5)
#define BLINK PORTC ^= LED
#define ADCIN PC5   //definicja ADCIN (wej�cie ADC)
volatile unsigned char a;       //odebrana liczba a
volatile uint8_t timer_flag = 0;
 ISR(USART_RX_vect){
	 UDRn = a;
	 a = UDRn;
	 timer_flag++;

 }

//ISR(TIMER1_COMPA_vect) {
//	timer_flag++;
//}

int main(void) {
	SREG |= _BV(7);

	TIMSK1 |= _BV(OCIE1A);
	TCCR1B |= _BV(CS12) | _BV(WGM12);
	OCR1A = 0x7a12; //31250;
	USART_Init();
	sei();
//	char str[15];
	LCD_Initalize();
	_delay_ms(5);
	for (;;) {

		if (timer_flag) {
			timer_flag = 0;
			LCD_Clear();
			LCD_WriteText("123");
//			USART_Transmit("a ");
		}
	}
}

