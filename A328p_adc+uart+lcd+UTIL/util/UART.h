/*
 * communicate by USART
 * File : UART.h
 * Microcontroller : Atmel AVR
 * Compilator : avr-gcc
 * Author : Karol Witkowski
 * Date 24.11.2014.
 */
#define BAUD 9600
#define MYUBRR F_CPU/16/BAUD-1

#include <avr/io.h>
#include <util/setbaud.h>

/*
 * Konfiguracja sygna��w
 * 0 - UART0 on ATMEGA 328P
 */
#define UART_NUMBER 0

#if UART_NUMBER == 0
#define UARTn 		UART0
#define UDRn 		UDR0
#define UBRRnH 		UBRR0H
#define UBRRnL 		UBRR0L

#define UCSRnA 		UCSR0A
#define RXCn 		RXC0
#define TXCn		TXC0
#define UDREn 		UDRE0
#define FEn 		FE0
#define DORn 		DOR0
#define UPEn		UPE0
#define U2Xn 		U2X0
#define MPCMn		MPCM0

#define UCSRnB 		UCSR0B
#define RXCIEn 		RXCIE0
#define TXCIEn 		TXCIE0
#define UDRIEn 		UDRIE0
#define RXENn		RXEN0
#define TXENn 		TXEN0
#define UCSZn2		UCSZ02
#define RXB8n		RXB80
#define TXB8n		TXB80

#define UCSRnC 		UCSR0C
#define UMSELn1		UMSEL01
#define UMSELn0 	UMSEL00
#define UPMn1		UPMN01
#define UPMn0		UPMN00
#define USBSn		USBS0
#define UCSZn1		UCSZ01
#define UCSZn0		UCSZ00
#define UCPOLn 		UCPOL0

#else
#define UARTn 		UART
#define UDRn 		UDR
#define UBRRnH 		UBRRH
#define UBRRnL 		UBRRL

#define UCSRnA 		UCSRA
#define RXCn 		RXC
#define TXCn		TXC
#define UDREn 		UDRE
#define FEn 		FE
#define DORn 		DOR
#define UPEn		UPE
#define U2Xn 		U2X
#define MPCMn		MPCM

#define UCSRnB 		UCSRB
#define RXCIEn 		RXCIE
#define TXCIEn 		TXCIE
#define UDRIEn 		UDRIE
#define RXENn		RXEN
#define TXENn 		TXEN
#define UCSZn2		UCSZ2
#define RXB8n		RXB8
#define TXB8n		TXB8

#define UCSRnC 		UCSRC
#define UMSELn1		UMSEL1
#define UMSELn0 	UMSEL0
#define UPMn1		UPMN1
#define UPMn0		UPMN0
#define USBSn		USBS
#define UCSZn1		UCSZ1
#define UCSZn0		UCSZ0
#define UCPOLn 		UCPOL
#endif

/*
 * Deklaracje funkcji
 */

void USART_Init();
void USART_Transmit(unsigned char data);
