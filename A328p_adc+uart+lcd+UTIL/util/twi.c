/*
 * TWI_.c
 *
 *  Created on: 21-07-2014
 *      Author: Ja
 */

#include "TWI.h"

// procedura transmisji sygna�u START
void TWI_start(void) {
	TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
	while (!(TWCR & (1 << TWINT)))
		;
}

// procedura transmisji sygna�u STOP
void TWI_stop(void) {
	TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);
	while ((TWCR & (1 << TWSTO)))
		;
}

// procedura transmisji bajtu danych
void TWI_write(uc data) {
	TWDR = data;
	TWCR = (1 << TWINT) | (1 << TWEN);
	while (!(TWCR & (1 << TWINT)))
		;
}

//procedura odczytu bajtu danych
uc TWI_read(uc ack) {
	TWCR = ack ?
			((1 << TWINT) | (1 << TWEN) | (1 << TWEA)) :
			((1 << TWINT) | (1 << TWEN));
	while (!(TWCR & (1 << TWINT)))
		;
	return TWDR ;
}

void TWI_write_buf(uc SLA, uc adr, uc len, uc *buf){
	TWI_start();
	TWI_write(SLA);
	TWI_write(adr);
	while (len--)
		TWI_write(*buf++);
	TWI_stop();
}

void TWI_read_buf(uc SLA, uc adr, uc len, uc *buf) {
	TWI_start();
	TWI_write(SLA);
	TWI_write(adr);
	TWI_start();
	TWI_write(SLA+1);
	while(len--)
		*buf++=TWI_read(len?ACK:NOACK);
	TWI_stop();
}

void TWI_SetBusSpeed(uint16_t speed) {
	speed = (F_CPU / speed / 100 - 16) / 2; //speed=TWBR�4^TWPS
	uint8_t prescaler = 0;
	while (speed > 255) //Oblicz warto�� preskalera
	{
		prescaler++;
		speed = speed / 4;
	};
	TWSR = (TWSR & (_BV(TWPS1) | _BV(TWPS0))) | prescaler;
	TWBR = speed;
	TWCR = _BV(TWEA) | _BV(TWEN); //i2c init
}
