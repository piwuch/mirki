/*
 * communicate by USART
 * File : UART.c
 * Microcontroller : Atmel AVR
 * Compilator : avr-gcc
 * Author : Karol Witkowski
 * Date 24.11.2014.
 */

#include "UART.h"

void USART_Init() {
	/* Set baud rate */
	UBRRnH = (unsigned char) (MYUBRR >> 8);
	UBRRnL = (unsigned char) MYUBRR;
//	ustaw odbior i nadawanie, oraz przerwanie przy odbiorze
	UCSRnB = (1 << RXENn) | (1 << RXCIEn) | (1 << TXENn);
//	format ramki: 8 data, 1 stop bit
	UCSRnC = (3 << UCSZn0);
}

void USART_Transmit(unsigned char data) {
//	oczekiwanie na pusty bufor
	while (!(UCSRnA & (1 << UDREn)))
		;
//	wyslij znak
	UDRn = data;
}
