/*
 * Control ADC
 * File : ADC.c
 * Microcontroller : Atmel AVR
 * Compilator : avr-gcc
 * Author : Karol Witkowski
 * Date 24.11.2014.
 */

#include "adc.h"

void ADC_Init(uint8_t prescaler) {
	switch (prescaler) {
	case 1:
		prescaler = 0;
		break;
	case 2:
		prescaler = 1;
		break;
	case 4:
		prescaler = 2;
		break;
	case 8:
		prescaler = 3;
		break;
	case 16:
		prescaler = 4;
		break;
	case 32:
		prescaler = 5;
		break;
	case 64:
		prescaler = 6;
		break;
	case 128:
		prescaler = 7;
		break;
	default:
		prescaler = 7;
	}
	ADCSRA |= _BV(ADEN) | (ADC_AUTO_TRIGGER_ENABLE << ADATE) | prescaler;
	ADCSRB |= (ADC_AUTO_TRIGGER_SOURCE << ADTS0);
	ADMUX |= (ADC_VOLTAGE_REFERENCE << REFS0);

}
void ADC_MUX_Select(uint8_t adc_channel) {
	ADMUX |= (adc_channel << MUX0);
}
uint16_t ADC_read(){
	ADCSRA |= (1 << ADSC);
			while (ADCSRA & (1 << ADSC))
				; //czeka na zakończenie konwersji
			return ADC;
}
uint16_t ADC_read_channel(uint8_t adc_channel){
	ADC_MUX_Select(adc_channel);
	return ADC_read();
}

