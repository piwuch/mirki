//-------------------------------------------------------------------------------------------------
// Wy�wietlacz alfanumeryczny ze sterownikiem HD44780
// Sterowanie w trybie 4-bitowym bez odczytu flagi zaj�to�ci
// z dowolnym przypisaniem sygna��w steruj�cych
// Plik : HD44780.h	
// Mikrokontroler : Atmel AVR
// Kompilator : avr-gcc
// Autor : Rados�aw Kwiecie�
// �r�d�o : http://radzio.dxp.pl/hd44780/
// Data : 24.03.2007
//-------------------------------------------------------------------------------------------------

#include <avr/io.h>
#include <util/delay.h>
#include "defines.h"
//-------------------------------------------------------------------------------------------------
//
// Konfiguracja sygna��w steruj�cych wy�wietlaczem.
// Mo�na zmieni� stosownie do potrzeb.
//
//-------------------------------------------------------------------------------------------------
#define RS_PORT 		B
#define RS_PIN 			0

#define RW_PORT 		D
#define RW_PIN 			7

#define E_PORT 			D
#define E_PIN 			6

#define DB4_PORT		B
#define DB4_PIN 		1
#define DB5_PORT		B
#define DB5_PIN 		2
#define DB6_PORT		B
#define DB6_PIN 		3
#define DB7_PORT		B
#define DB7_PIN 		4
//-------------------------------------------------------------------------------------------------
#define LCD_RS_DIR		DDR(RS_PORT)
#define LCD_RS_PORT 	PORT(RS_PORT)
#define LCD_RS_PIN		PIN(RS_PORT)
#define LCD_RS			P(RS_PORT,RS_PIN)

#define LCD_RW_DIR		DDR(RW_PORT)
#define LCD_RW_PORT		PORT(RW_PORT)
#define LCD_RW_PIN		PIN(RW_PORT)
#define LCD_RW			P(RW_PORT,RW_PIN)

#define LCD_E_DIR		DDR(E_PORT)
#define LCD_E_PORT		PORT(E_PORT)
#define LCD_E_PIN		PIN(E_PORT)
#define LCD_E			P(E_PORT,E_PIN)

#define LCD_DB4_DIR		DDR(DB4_PORT)
#define LCD_DB4_PORT	PORT(DB4_PORT)
#define LCD_DB4_PIN		PIN(DB4_PORT)
#define LCD_DB4			P(DB4_PORT,DB4_PIN)

#define LCD_DB5_DIR		DDR(DB5_PORT)
#define LCD_DB5_PORT	PORT(DB5_PORT)
#define LCD_DB5_PIN		PIN(DB5_PORT)
#define LCD_DB5			P(DB5_PORT,DB5_PIN)

#define LCD_DB6_DIR		DDR(DB6_PORT)
#define LCD_DB6_PORT	PORT(DB6_PORT)
#define LCD_DB6_PIN		PIN(DB6_PORT)
#define LCD_DB6			P(DB6_PORT,DB6_PIN)

#define LCD_DB7_DIR		DDR(DB7_PORT)
#define LCD_DB7_PORT	PORT(DB7_PORT)
#define LCD_DB7_PIN		PIN(DB7_PORT)
#define LCD_DB7			P(DB7_PORT,DB7_PIN)

//-------------------------------------------------------------------------------------------------
//
// Instrukcje kontrolera Hitachi HD44780
//
//-------------------------------------------------------------------------------------------------

#define HD44780_CLEAR					0x01

#define HD44780_HOME					0x02

#define HD44780_ENTRY_MODE				0x04
#define HD44780_EM_SHIFT_CURSOR		0
#define HD44780_EM_SHIFT_DISPLAY	1
#define HD44780_EM_DECREMENT		0
#define HD44780_EM_INCREMENT		2

#define HD44780_DISPLAY_ONOFF			0x08
#define HD44780_DISPLAY_OFF			0
#define HD44780_DISPLAY_ON			4
#define HD44780_CURSOR_OFF			0
#define HD44780_CURSOR_ON			2
#define HD44780_CURSOR_NOBLINK		0
#define HD44780_CURSOR_BLINK		1

#define HD44780_DISPLAY_CURSOR_SHIFT	0x10
#define HD44780_SHIFT_CURSOR		0
#define HD44780_SHIFT_DISPLAY		8
#define HD44780_SHIFT_LEFT			0
#define HD44780_SHIFT_RIGHT			4

#define HD44780_FUNCTION_SET			0x20
#define HD44780_FONT5x7				0
#define HD44780_FONT5x10			4
#define HD44780_ONE_LINE			0
#define HD44780_TWO_LINE			40//zmienione
#define HD44780_4_BIT				0
#define HD44780_8_BIT				16

#define HD44780_CGRAM_SET				0x40

#define HD44780_DDRAM_SET				0x80

//-------------------------------------------------------------------------------------------------
//
// Deklaracje funkcji
//
//-------------------------------------------------------------------------------------------------

void LCD_WriteCommand(unsigned char);
unsigned char LCD_ReadStatus(void);
void LCD_WriteData(unsigned char);
unsigned char LCD_ReadData(void);
void LCD_WriteText(char *);
void LCD_GoTo(unsigned char, unsigned char);
void LCD_Clear(void);
void LCD_Home(void);
void LCD_Initalize(void);

//-------------------------------------------------------------------------------------------------
//
// Koniec pliku HD44780.h
//
//-------------------------------------------------------------------------------------------------
