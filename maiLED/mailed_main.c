/*
 * main.c
 *	2 inty + led
 *  Created on: 23-07-2014
 *      Author: Ja
 */
#define FOSC 96000000
#include <avr\io.h>
#include <stdio.h>
#include <util/delay.h>
//#include <avr/interrupt.h>
#define LED _BV(PB3)
#define SENSOR_LETTER _BV(PB1)
#define SENSOR_OPEN _BV(PB2)
//ISR(INT0_vect) {
//	PORTB ^= _BV(PB3);
////	_delay_ms(100);
//}

int main(void) {
	DDRB |= LED;
	PORTB |= SENSOR_LETTER | SENSOR_OPEN;
//	SREG |= _BV(7);
	PORTB |= LED;
//	PCMSK |= _BV(PCINT0);
//	GIMSK |= _BV(INT0);
//	MCUCR |= (1 << ISC00);
//	GIFR |= (1 << INT0);
//	sei();
	PORTB ^= LED;
	_delay_ms(500);
	PORTB ^= LED;
	_delay_ms(500);
	PORTB ^= LED;
	_delay_ms(500);

	uint8_t is_letter = 1, key1_lock = 0, key2_lock = 0;
	while (1) {
		if (!key1_lock && (PINB & SENSOR_LETTER)) {
			key1_lock = 1;
			is_letter = 1;
		} else if (key1_lock && !(PINB & SENSOR_LETTER))
			key1_lock++;

		if (!key2_lock && (PINB & SENSOR_OPEN)) {
			key2_lock = 1;
			is_letter = 0;
		} else if (key2_lock && !(PINB & SENSOR_OPEN))
			key2_lock++;

		if (is_letter) {
			_delay_ms(500);
			PORTB ^= LED;
			_delay_ms(50);
			PORTB ^= LED;
		}
	}
}

