/*
 * lcd.c
 * lcd + uart na przerwaniach
 *  Created on: 16-07-2014
 *      Author: Ja
 */

#define BAUD 4800        //��dana pr�dko�� transmisji
#define FOSC 1000000     // Clock Speed
#define MYUBRR FOSC/16/BAUD-1 //12
#include <avr\io.h>
#include <stdio.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <util/setbaud.h>
#include "HD44780.h"

volatile unsigned char a;       //odebrana liczba a
volatile unsigned char odb_flaga = 0; //flaga informuj�ca main o odebraniu liczby

void USART_Init(unsigned int ubrr) {
	/* Set baud rate */
	UBRRH = (unsigned char) (ubrr >> 8);
	UBRRL = (unsigned char) ubrr;
	/* Enable receiver and 'transmitter' */UCSRB = (1 << RXEN)
			| (1 << RXCIE) /*| (1 << TXEN)*/;
	/* Set frame format: 8data, 2stop bit */
	UCSRC = (1 << URSEL) | (1 << USBS) | (3 << UCSZ0);
}

ISR(USART_RXC_vect) {
	//przerwanie generowane po odebraniu bajtu
	a = UDR;   //zapami�taj odebran� liczb�
	odb_flaga = 1; //ustaw flag� odbioru liczby dla main()
}
int main(void) {
	USART_Init( MYUBRR); //inicjalizacja USART
	sei();
	//przerwania on
	DDRB |= _BV(PB1) | _BV(PB2);
	LCD_Initalize();
	volatile char znak = 0;
	volatile char drugaKolejka = 0;
	PORTB |= _BV(PB2); // BIP
	_delay_ms(5);
	PORTB &= ~_BV(PB2);
	while (1) {
		if (odb_flaga) {
			odb_flaga = 0; //zga� flag�
//			if(a==8){
//				LCD_GoTo(znak-40<0?znak:znak-40,znak-40<0?0:1);
//				continue;
//			}
			LCD_WriteData(a);
			znak++;
			if (a == 'h') {
				PORTB |= _BV(PB1); //led on
			} else if (a == 'j') {
				PORTB &= ~_BV(PB1); // led off
			} else {
				PORTB |= _BV(PB2); // BIP
				_delay_ms(5);
				PORTB &= ~_BV(PB2);

			}
			if (znak == 12) {
				LCD_GoTo(0,1);
				znak = 40;
			} else if (znak == 52 ) {
				LCD_GoTo(12,0);
				znak = 12;
				drugaKolejka = 1;
			} else if (znak == 24) {
				LCD_GoTo(12,1);
				znak = 52;
			} else if (znak == 64) {
				LCD_Clear();
				znak = 0;
				drugaKolejka = 0;
			}
		}

	}
}

