/*
 * twi.c
 *
 *  Created on: 21-07-2014
 *      Author: Ja
 */

#include "TWI.h"

// procedura transmisji sygna�u START
void twistart(void) {
	TWCR = (1 << TWINT) | (1 << TWSTA) | (1 << TWEN);
	while (!(TWCR & (1 << TWINT)))
		;
}

// procedura transmisji sygna�u STOP
void twistop(void) {
	TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWSTO);
	while ((TWCR & (1 << TWSTO)))
		;
}

// procedura transmisji bajtu danych
void twiwrite(uc data) {
	TWDR = data;
	TWCR = (1 << TWINT) | (1 << TWEN);
	while (!(TWCR & (1 << TWINT)))
		;
}

//procedura odczytu bajtu danych
uc twiread(uc ack) {
	TWCR = ack ?
			((1 << TWINT) | (1 << TWEN) | (1 << TWEA)) :
			((1 << TWINT) | (1 << TWEN));
	while (!(TWCR & (1 << TWINT)))
		;
	return TWDR ;
}

void twiwrite_buf(uc SLA, uc adr, uc len, uc *buf){
	twistart();
	twiwrite(SLA);
	twiwrite(adr);
	while (len--)
		twiwrite(*buf++);
	twistop();
}

void twiread_buf(uc SLA, uc adr, uc len, uc *buf) {
	twistart();
	twiwrite(SLA);
	twiwrite(adr);
	twistart();
	twiwrite(SLA+1);
	while(len--)
		*buf++=twiread(len?ACK:NOACK);
	twistop();
}
