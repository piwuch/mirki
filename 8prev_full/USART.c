/*
 * USART.c
 *
 *  Created on: 21-07-2014
 *      Author: Ja
 */

#include "USART.h"

void USART_Init(unsigned int ubrr) {
	UBRRH = (unsigned char) (ubrr >> 8);
	UBRRL = (unsigned char) ubrr;
	UCSRB = (1 << RXEN) | (1 << TXEN) | (1 << RXCIE);
	UCSRC = (1 << URSEL) | (1 << USBS) | (3 << UCSZ0);
}

void USART_Transmit(char data) {
	while (!(UCSRA & (1 << UDRE)))
		;
	UDR = data;
}

void USART_Transmit_Text(char * text) {
	while (*text) {
		USART_Transmit(*text++);
	}
}

unsigned char USART_Receive(void) {
	/* Wait for data to be received */
	while (!(UCSRA & (1 << RXC)))
		;
	/* Get and return received data from buffer */
	return UDR ;
}

