/*
 * USART.h
 *
 *  Created on: 21-07-2014
 *      Author: Ja
 */

#include <avr/io.h>

#define ACK 1
#define NOACK 0

void USART_Init(unsigned int ubrr);
void USART_Transmit(char data);
void USART_Transmit_Text(char * text);
unsigned char USART_Receive(void);
