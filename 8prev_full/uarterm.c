/*
 * uarterm.c
 *	sterowanie USART + I2C termometr + LCD
 *  Created on: 23-07-2014
 *      Author: Ja
 */

/*
 * term.c
 * termometr + I2C + przerwanie
 *  Created on: 18-07-2014
 *      Author: Ja
 */

#define BAUD 4800        //��dana pr�dko�� transmisji
#define FOSC 1000000     // Clock Speed
#define MYUBRR FOSC/16/BAUD-1 //12
#define TA 0b10010000
//#include <stddef.h>
#include <avr\io.h>
#include <stdio.h>
#include <avr/interrupt.h>
//#include <util/delay.h>
#include <util/twi.h>
#include <util/setbaud.h>
#include <avr/wdt.h>

#include "HD44780.h"
#include "twi.h"
#include "USART.h"

volatile unsigned char usart_flag = 0;
volatile unsigned char timer_flag = 0;
volatile char UWbuf[10];
volatile char URbuf[10];
volatile unsigned char Rbuf_ind = 0;

void I2C_SetBusSpeed(uint16_t speed);
void temperature_to_text(unsigned char a1, unsigned char a2, char *text);

ISR(INT0_vect) {
	PORTB ^= _BV(PB2);
}

ISR(USART_RXC_vect) {
	URbuf[Rbuf_ind] = UDR;
	UDR = URbuf[Rbuf_ind++];
	usart_flag = 1; //ustaw flag� odbioru liczby dla main()
}

ISR(TIMER1_COMPA_vect) {
	timer_flag = 1;
}

int main(void) {
	USART_Init(MYUBRR);
	const char *wrong_command = "wrong command\n\r";
	USART_Transmit_Text("\n\r 1 - temperature read \n\r 2 - set treshold \n\r 3 - nie wciskac \n\r" );

	DDRB |= _BV(PB1) | _BV(PB2);
	MCUCR |= (1 << ISC00);
	GICR |= (1 << INT0);

	LCD_Initalize();

	TIMSK |= _BV(OCIE1A);
	TCCR1B |= _BV(CS10) | _BV(CS11) | _BV(WGM12);
	OCR1A = 0x3d09;

	WDTCR |= _BV(WDE) | _BV(WDP0) | _BV(WDP1) | _BV(WDP2);

	TWCR = _BV(TWEA) | _BV(TWEN); //i2c init
	I2C_SetBusSpeed(100);
	volatile unsigned char a[] = { 0, 0 };
	char temperature[10];
	uc tOS[] = { 34, 0 };
	uc tHYS[] = { 33, 0 };
	twiwrite_buf(TA, 2, 2, tHYS);
	twiwrite_buf(TA, 3, 2, tOS);
	uc menu_ind = 0;

	sei();
	while (1) {
		wdt_reset();
		if (timer_flag) {
			timer_flag = 0;
			PORTB ^= _BV(PB1);
			twiread_buf(TA, 0, 2, a);
			temperature_to_text(a[0], a[1], temperature);
			LCD_Clear();
			LCD_WriteText(temperature);
		}
		if (usart_flag) {
			usart_flag = 0;
			if (!menu_ind) {
				if (URbuf[Rbuf_ind - 1] == '1') { //show temp
					USART_Transmit_Text(temperature);
					USART_Transmit_Text("\n\r");
					LCD_Clear();
					LCD_WriteText(temperature);
				} else if (URbuf[Rbuf_ind - 1] == '2') {
					menu_ind = 1;
					USART_Transmit_Text("\n\rupper treshold: ");
					tOS[0] = 0;
					tHYS[0] = 0;
				} else if(URbuf[Rbuf_ind - 1] == '3'){
					USART_Transmit_Text("\n\rbedziesz smazyl sie w piekle");
					while(1);
				} else
					USART_Transmit_Text(wrong_command);
			} else if (menu_ind == 1) {
				if (!tOS[0]) {
					tOS[0] = (URbuf[Rbuf_ind - 1] - '0') * 10;
					USART_Transmit('x');
					USART_Transmit('0' + tOS[0] / 10);
					USART_Transmit('x');
				} else {
					tOS[0] += (URbuf[Rbuf_ind - 1] - '0');
					USART_Transmit('x');
					USART_Transmit('0' + tOS[0] % 10);
					USART_Transmit('x');
					menu_ind = 2;

					USART_Transmit_Text("\n\rlower treshold: ");
				}
			} else if (menu_ind == 2) {
				if (!tHYS[0])
					tHYS[0] = (URbuf[Rbuf_ind - 1] - '0') * 10;

				else {
					tHYS[0] += (URbuf[Rbuf_ind - 1] - '0');
					menu_ind = 0;
					USART_Transmit_Text("\n\rsetting hysteresis from: ");
					USART_Transmit('0' + tHYS[0] / 10);
					USART_Transmit('0' + tHYS[0] % 10);
					USART_Transmit_Text(" to ");
					USART_Transmit('0' + tOS[0] / 10);
					USART_Transmit('0' + tOS[0] % 10);
					twiwrite_buf(TA, 2, 2, tHYS);
					twiwrite_buf(TA, 3, 2, tOS);
					USART_Transmit_Text(
							"\n\r 1 - temperature read \n\r 2 - set treshold \n\r");
				}
			}
			if (Rbuf_ind > 7) {
				while (Rbuf_ind)
					URbuf[Rbuf_ind--] = 0;
				URbuf[0] = 0;
			}
		}

	}
} //main

void I2C_SetBusSpeed(uint16_t speed) {
	speed = (F_CPU / speed / 100 - 16) / 2; //speed=TWBR�4^TWPS
	uint8_t prescaler = 0;
	while (speed > 255) //Oblicz warto�� preskalera
	{
		prescaler++;
		speed = speed / 4;
	};
	TWSR = (TWSR & (_BV(TWPS1) | _BV(TWPS0))) | prescaler;
	TWBR = speed;
}

void temperature_to_text(unsigned char a1, unsigned char a2, char *text) {
	if (a1 > 127) {
		a1 -= 128;
		*text++ = '-';
	} else
		*text++ = ' ';
	*text++ = '0' + a1 / 10;
	*text++ = '0' + a1 % 10;
	*text++ = '.';
	*text++ = a2 ? '5' : '0';
	*text++ = ' ';
	*text++ = 239;
	*text++ = 'C';
	*text++ = '.';
	*text++ = 0;
}
